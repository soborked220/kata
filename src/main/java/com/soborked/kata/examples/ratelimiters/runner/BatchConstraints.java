package com.soborked.kata.examples.ratelimiters.runner;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class BatchConstraints {

  @Getter private int maxRequestsPerSecond;
  private double variance;

  public double perSecond() {
    return ((1 + variance) * maxRequestsPerSecond);
  }
}
