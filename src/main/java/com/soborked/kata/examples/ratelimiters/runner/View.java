package com.soborked.kata.examples.ratelimiters.runner;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class View {

  private String name;
  private Timeline timeline;
}
