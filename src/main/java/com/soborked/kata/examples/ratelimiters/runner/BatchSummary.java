package com.soborked.kata.examples.ratelimiters.runner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.soborked.kata.examples.ratelimiters.runner.Measure.*;
import static java.util.stream.Collectors.toList;

public class BatchSummary {

  public static final String BATCH_VIEW = "Batch";
  private static final long SECOND = 1000;

  private long started;
  private long stopped;
  private List<View> perspectives = new ArrayList<>();

  public BatchSummary(long startTime, long stopTime, Map<String, List<Attempt>> attemptsByRunner) {
    this.started = startTime;
    this.stopped = stopTime;
    attemptsByRunner.forEach(
        (runner, attempts) -> {
          Timeline line = new Timeline(started, stopped, SECOND, attempts);
          perspectives.add(new View(runner, line));
        });
    List<Attempt> all = attemptsByRunner.values().stream().flatMap(List::stream).collect(toList());
    Timeline batch = new Timeline(started, stopped, SECOND, all);
    perspectives.add(new View(BATCH_VIEW, batch));
  }

  public double attemptCount() {
    return lookup(BATCH_VIEW).getTimeline().totalAttempts();
  }

  public double successCount() {
    return lookup(BATCH_VIEW).getTimeline().successes();
  }

  public double failCount() {
    return lookup(BATCH_VIEW).getTimeline().failures();
  }

  public double averageAPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().totalAttempts() / lengthOfBatch();
  }

  public double averageSPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().successes() / lengthOfBatch();
  }

  public double averageFPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().failures() / lengthOfBatch();
  }

  public Frame lowestAPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().frameWithAttempt(MIN);
  }

  public Frame highestAPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().frameWithAttempt(MAX);
  }

  public Frame lowestSPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().frameWithSuccess(MIN);
  }

  public Frame highestSPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().frameWithSuccess(MAX);
  }

  public Frame lowestFPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().frameWithFailure(MIN);
  }

  public Frame highestFPSFrom(String perspective) {
    View view = lookup(perspective);
    return view.getTimeline().frameWithFailure(MAX);
  }

  public String report() {
    return String.format(
        "Report: \n"
            + "\tduration: %s start: %s stop: %s\n"
            + "\tavg/s: [attempts: %.2f successes: %.2f failures: %.2f]\n"
            + "\ttotal: [attempts: %s successes: %s failures: %s]\n"
            + "frames with: \n"
            + "\tmin attempts:  [%s] \n"
            + "\tmin successes: [%s] \n"
            + "\tmin failures:  [%s] \n"
            + "\tmax attempts:  [%s] \n"
            + "\tmax successes: [%s] \n"
            + "\tmax failures:  [%s] \n",
        (stopped - started),
        started,
        stopped,
        averageAPSFrom(BATCH_VIEW),
        averageSPSFrom(BATCH_VIEW),
        averageFPSFrom(BATCH_VIEW),
        attemptCount(),
        successCount(),
        failCount(),
        lowestAPSFrom(BATCH_VIEW).summary(),
        lowestSPSFrom(BATCH_VIEW).summary(),
        lowestFPSFrom(BATCH_VIEW).summary(),
        highestAPSFrom(BATCH_VIEW).summary(),
        highestSPSFrom(BATCH_VIEW).summary(),
        highestFPSFrom(BATCH_VIEW).summary());
  }

  private View lookup(String name) {
    return perspectives.stream()
        .filter(view -> view.getName().equalsIgnoreCase(name))
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("No view found with name: " + name));
  }

  private double lengthOfBatch() {
    long duration = stopped - started;
    return ((double) duration / SECOND);
  }
}
