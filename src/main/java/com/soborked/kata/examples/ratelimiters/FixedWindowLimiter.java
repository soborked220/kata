package com.soborked.kata.examples.ratelimiters;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class FixedWindowLimiter implements RateLimiter {

  private final ConcurrentHashMap<Long, AtomicInteger> windows = new ConcurrentHashMap<>();
  private int max;

  public FixedWindowLimiter(int maxRequestsPerSecond) {
    this.max = maxRequestsPerSecond;
  }

  @Override
  public boolean allow() {
    long key = System.currentTimeMillis() / 1000;
    windows.putIfAbsent(key, new AtomicInteger(0));
    return windows.get(key).incrementAndGet() <= max;
  }
}
