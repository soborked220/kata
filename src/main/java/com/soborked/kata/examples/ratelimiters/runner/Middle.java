package com.soborked.kata.examples.ratelimiters.runner;

import java.util.List;
import java.util.function.Supplier;

public class Middle extends Load {
  public Middle(int capacity, long duration) {
    super(capacity, duration);
  }

  @Override
  public List<Attempt> make(Supplier<Boolean> attempt, int remaining) {
    sleep(duration / 2);
    List<Attempt> attempts = makeAll(attempt, remaining);
    sleep(duration / 2);
    return attempts;
  }
}
