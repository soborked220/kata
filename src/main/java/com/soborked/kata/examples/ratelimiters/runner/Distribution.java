package com.soborked.kata.examples.ratelimiters.runner;

public enum Distribution {
  UNIFORM,
  START,
  MIDDLE,
  END;
}
