package com.soborked.kata.examples.ratelimiters;

/**
 * This really bogs down at scale and in bursty traffic. Might be useful if you incorporated a queue
 * system.
 */
public class LeakyBucketLimiter implements RateLimiter {

  public static final int ONE_SEC = 1000;
  private long nextAllowed;
  private int rps;

  public LeakyBucketLimiter(int rps) {
    this.rps = rps;
    this.nextAllowed = System.currentTimeMillis();
  }

  @Override
  public boolean allow() {
    long current = System.currentTimeMillis();
    if (current > nextAllowed) {
      nextAllowed = current + next();
      return true;
    }
    return false;
  }

  private long next() {
    return ONE_SEC / rps;
  }
}
