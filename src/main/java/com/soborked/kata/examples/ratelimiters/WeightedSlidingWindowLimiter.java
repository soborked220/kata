package com.soborked.kata.examples.ratelimiters;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

public class WeightedSlidingWindowLimiter implements RateLimiter {

  public static final int ONE_SECOND = 1000;
  public static final double ONE_SEC = 1000.0;
  private int max;
  private final ConcurrentMap<Long, AtomicInteger> windows = new ConcurrentHashMap<>();

  public WeightedSlidingWindowLimiter(int maxRequestsPerSecond) {
    this.max = maxRequestsPerSecond;
  }

  @Override
  public boolean allow() {
    long currentTime = System.currentTimeMillis();
    long startOfCurrentWindow = trimMillis(currentTime);
    windows.putIfAbsent(startOfCurrentWindow, new AtomicInteger(0));

    long startOfPreviousWindow = startOfCurrentWindow - ONE_SECOND;
    AtomicInteger previousCount = windows.get(startOfPreviousWindow);
    AtomicInteger currentCount = windows.get(startOfCurrentWindow);
    if (previousCount == null) {
      return currentCount.incrementAndGet() <= max;
    }

    double percentIntoCurrent = (currentTime - startOfCurrentWindow) / ONE_SEC;
    double percentRemainingInPrevious = 1 - percentIntoCurrent;
    long stillInUse = (long) (previousCount.get() * percentRemainingInPrevious);
    long weightedCount = stillInUse + currentCount.incrementAndGet();
    return weightedCount <= max;
  }

  private long trimMillis(long time) {
    return time / ONE_SECOND * ONE_SECOND;
  }
}
