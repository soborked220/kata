package com.soborked.kata.examples.ratelimiters.runner;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class Uniform extends Load {
  public Uniform(int capacity, long duration) {
    super(capacity, duration);
  }

  @Override
  public List<Attempt> make(Supplier<Boolean> attempt, int remaining) {
    List<Attempt> attempts = new ArrayList<>();
    for (int loaded = 0; loaded <= capacity; loaded++) {
      if (remaining != 0) {
        boolean success = attempt.get();
        if (success) {
          remaining--;
        }
        attempts.add(new Attempt(success));
        sleep(duration / capacity);
      }
    }
    return attempts;
  }
}
