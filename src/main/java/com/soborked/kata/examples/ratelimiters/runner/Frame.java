package com.soborked.kata.examples.ratelimiters.runner;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

import static java.lang.String.format;

@Getter
@ToString
@AllArgsConstructor
public class Frame {

  private long absoluteCenter;
  private long relativeCenter;
  private long duration;
  private List<Attempt> attempts;

  public int total() {
    return attempts.size();
  }

  public int successes() {
    return (int) attempts.stream().filter(Attempt::isSuccess).count();
  }

  public int failures() {
    return (int) attempts.stream().filter(attempt -> !attempt.isSuccess()).count();
  }

  public String summary() {
    return format(
        "spans: %s -> %s attempts: %s successes: %s failures: %s",
        relativeCenter - (duration / 2),
        relativeCenter + (duration / 2),
        total(),
        successes(),
        failures());
  }
}
