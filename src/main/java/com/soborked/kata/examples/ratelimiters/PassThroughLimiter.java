package com.soborked.kata.examples.ratelimiters;

public class PassThroughLimiter implements RateLimiter {
  @Override
  public boolean allow() {
    return true;
  }
}
