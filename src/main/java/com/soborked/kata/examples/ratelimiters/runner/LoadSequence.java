package com.soborked.kata.examples.ratelimiters.runner;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@Getter
public class LoadSequence {

  private String name;
  private int target;
  private List<Load> loadCycle;

  public LoadSequence(String name, int target, Load... loads) {
    this.name = name;
    this.target = target;
    this.loadCycle = Arrays.asList(loads);
  }

  public List<Attempt> runWithin(Supplier<Boolean> attempt) {
    List<Attempt> attempts = new ArrayList<>();
    int cycleSize = loadCycle.size();
    int cycle = 0;
    int remaining = target;
    while (remaining > 0) {
      Load load = loadCycle.get((cycle % cycleSize));
      List<Attempt> attempted = load.make(attempt, remaining);
      attempts.addAll(attempted);
      int successes = (int) attempted.stream().filter(Attempt::isSuccess).count();
      remaining -= successes;
      cycle++;
    }
    return attempts;
  }
}
