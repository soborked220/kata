package com.soborked.kata.examples.ratelimiters.runner;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@AllArgsConstructor
@Accessors(fluent = true)
public class Scenario {

  private String name;
  private int rps;
  private double allowedAvgRPS;
  private double allowedMaxRPS;
  private RequestLoader traffic;

  public String toString() {
    return name;
  }
}
