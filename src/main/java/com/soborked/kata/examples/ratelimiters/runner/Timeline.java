package com.soborked.kata.examples.ratelimiters.runner;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static com.soborked.kata.examples.ratelimiters.runner.Measure.MIN;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;

@Getter
public class Timeline {

  private static final IllegalStateException NO_FRAMES_EXIST =
      new IllegalStateException("No frames exist");
  private long started;
  private long stopped;
  private long frameSize;
  private List<Frame> frames;
  private List<Frame> offsetFrames;

  public Timeline(long started, long stopped, long frameSize, List<Attempt> attempts) {
    this.started = started;
    this.stopped = stopped;
    this.frameSize = frameSize;
    frames = build(started + (frameSize / 2), attempts);
    offsetFrames = build(started + (frameSize), attempts);
  }

  public double totalAttempts() {
    return frames.stream().map(Frame::getAttempts).mapToLong(List::size).sum();
  }

  public double successes() {
    return frames.stream()
        .map(Frame::getAttempts)
        .flatMap(List::stream)
        .filter(Attempt::isSuccess)
        .count();
  }

  public double failures() {
    return frames.stream()
        .map(Frame::getAttempts)
        .flatMap(List::stream)
        .filter(Attempt::isFailure)
        .count();
  }

  public Frame frameWithAttempt(Measure measure) {
    if (measure.equals(MIN)) {
      return Stream.concat(frames.stream(), offsetFrames.stream())
          .min(comparingInt(Frame::total))
          .orElseThrow(() -> NO_FRAMES_EXIST);
    } else {
      return Stream.concat(frames.stream(), offsetFrames.stream())
          .max(comparingInt(Frame::total))
          .orElseThrow(() -> NO_FRAMES_EXIST);
    }
  }

  public Frame frameWithSuccess(Measure measure) {
    if (measure.equals(MIN)) {
      return Stream.concat(frames.stream(), offsetFrames.stream())
          .min(comparingInt(Frame::successes))
          .orElseThrow(() -> NO_FRAMES_EXIST);
    } else {
      return Stream.concat(frames.stream(), offsetFrames.stream())
          .max(comparingInt(Frame::successes))
          .orElseThrow(() -> NO_FRAMES_EXIST);
    }
  }

  public Frame frameWithFailure(Measure measure) {
    if (measure.equals(MIN)) {
      return Stream.concat(frames.stream(), offsetFrames.stream())
          .min(comparingInt(Frame::failures))
          .orElseThrow(() -> NO_FRAMES_EXIST);
    } else {
      return Stream.concat(frames.stream(), offsetFrames.stream())
          .max(comparingInt(Frame::failures))
          .orElseThrow(() -> NO_FRAMES_EXIST);
    }
  }

  private List<Frame> build(long firstCenter, List<Attempt> attempts) {
    List<Frame> frames = new ArrayList<>();
    for (long time = firstCenter; time < stopped; time += frameSize) {
      long relativeCenter = time - started;
      List<Attempt> contained =
          attempts.stream()
              .filter(attempt -> withinFrame(attempt, relativeCenter))
              .collect(toList());
      frames.add(new Frame(time, relativeCenter, frameSize, contained));
    }
    return frames;
  }

  private boolean withinFrame(Attempt attempt, long relativeCenter) {
    long attemptedAt = attempt.getTime() - started;
    long frameStart = relativeCenter - (frameSize / 2);
    long frameEnd = relativeCenter + (frameSize / 2);
    return attemptedAt > frameStart && attemptedAt < frameEnd;
  }
}
