package com.soborked.kata.examples.ratelimiters.runner;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Attempt {
  private boolean success;
  private long time;

  public Attempt(boolean success) {
    this.success = success;
    this.time = System.currentTimeMillis();
  }

  /* package-private */ Attempt(boolean success, long time) {
    this.success = success;
    this.time = time;
  }

  public boolean isFailure() {
    return !success;
  }
}
