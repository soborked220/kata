package com.soborked.kata.examples.ratelimiters.runner;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@Getter
public abstract class Load {
  public int capacity;
  public long duration;

  public Load(int capacity, long duration) {
    this.capacity = capacity;
    this.duration = duration;
  }

  public abstract List<Attempt> make(Supplier<Boolean> attempt, int remaining);

  protected void sleep(long pause) {
    try {
      TimeUnit.MILLISECONDS.sleep(pause);
    } catch (InterruptedException e) {
      throw new IllegalStateException(e);
    }
  }

  protected List<Attempt> makeAll(Supplier<Boolean> attempt, int remaining) {
    List<Attempt> attempts = new ArrayList<>();
    for (int loaded = 0; loaded <= capacity; loaded++) {
      if (remaining != 0) {
        boolean success = attempt.get();
        if (success) {
          remaining--;
        }
        attempts.add(new Attempt(success));
      }
    }
    return attempts;
  }
}
