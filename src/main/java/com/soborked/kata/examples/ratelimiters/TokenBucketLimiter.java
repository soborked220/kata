package com.soborked.kata.examples.ratelimiters;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * Handles burst traffic fairly well, but has some one-second windows where max rps is exceeded by
 * 2, and the average rps often is exceeded by up to 10%.
 */
@Slf4j
public class TokenBucketLimiter implements RateLimiter {

  private int tokens;

  public TokenBucketLimiter(int maxRequestsPerSec) {
    if (maxRequestsPerSec == 0) {
      throw new IllegalArgumentException("Must let some traffic through");
    }
    this.tokens = maxRequestsPerSec;
    periodicallyRefill(maxRequestsPerSec);
  }

  private void periodicallyRefill(int maxRequestsPerSec) {
    new Thread(
            () -> {
              while (true) {
                waitaSecond();
                refill(maxRequestsPerSec);
              }
            })
        .start();
  }

  private void waitaSecond() {
    try {
      TimeUnit.MILLISECONDS.sleep(1000);
    } catch (InterruptedException ex) {
      log.error("RateLimiting thread interrupted.");
    }
  }

  private void refill(int max) {
    synchronized (this) {
      tokens = max;
      notifyAll();
    }
  }

  @Override
  public boolean allow() {
    synchronized (this) {
      if (tokens == 0) {
        return false;
      }
      tokens--;
      return true;
    }
  }
}
