package com.soborked.kata.examples.ratelimiters;

public interface RateLimiter {

  boolean allow();
}
