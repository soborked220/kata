package com.soborked.kata.examples.ratelimiters.runner;

import java.util.List;
import java.util.function.Supplier;

public class Back extends Load {
  public Back(int capacity, long duration) {
    super(capacity, duration);
  }

  @Override
  public List<Attempt> make(Supplier<Boolean> attempt, int remaining) {
    sleep(duration);
    return makeAll(attempt, remaining);
  }
}
