package com.soborked.kata.examples.ratelimiters;

import java.util.LinkedList;
import java.util.Queue;

public class LooseSlidingWindowLimiter implements RateLimiter {

  private static final long WINDOW_SIZE = 1000;
  private final Queue<Long> requests = new LinkedList<>();
  private int max;

  public LooseSlidingWindowLimiter(int maxRequestsPerSecond) {
    this.max = maxRequestsPerSecond;
  }

  @Override
  public boolean allow() {
    synchronized (requests) {
      long rightSideOfWindow = System.currentTimeMillis();
      long leftEdgeOfWindow = rightSideOfWindow - WINDOW_SIZE;
      purgeRecordsEarlierThan(leftEdgeOfWindow);
      if (requests.size() >= max) {
        return false;
      }
      requests.add(rightSideOfWindow);
      return true;
    }
  }

  private void purgeRecordsEarlierThan(long leftEdgeOfWindow) {
    while (!requests.isEmpty() && requests.element() <= leftEdgeOfWindow) {
      requests.poll();
    }
  }
}
