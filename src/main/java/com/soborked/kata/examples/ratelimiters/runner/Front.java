package com.soborked.kata.examples.ratelimiters.runner;

import java.util.List;
import java.util.function.Supplier;

public class Front extends Load {
  public Front(int capacity, long duration) {
    super(capacity, duration);
  }

  @Override
  public List<Attempt> make(Supplier<Boolean> attempt, int remaining) {
    List<Attempt> attempts = makeAll(attempt, remaining);
    sleep(duration);
    return attempts;
  }
}
