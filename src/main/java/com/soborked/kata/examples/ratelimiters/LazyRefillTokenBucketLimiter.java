package com.soborked.kata.examples.ratelimiters;

/**
 * Handles burst traffic fairly well, but has some one-second windows where max rps is exceeded by
 * 1, and the average rps often is exceeded by up to 7%.
 */
public class LazyRefillTokenBucketLimiter implements RateLimiter {

  private static final int MILLIS_IN_A_SEC = 1000;
  private final int max;
  private int tokens;
  private long lastRefilled;

  public LazyRefillTokenBucketLimiter(int maxRequestsPerSecond) {
    this.max = maxRequestsPerSecond;
    this.tokens = max;
    this.lastRefilled = System.currentTimeMillis();
  }

  @Override
  public boolean allow() {
    synchronized (this) {
      refill();
      if (tokens > 0) {
        tokens--;
        return true;
      }
      return false;
    }
  }

  private void refill() {
    long current = System.currentTimeMillis();
    int timeSince = elapsedSeconds(lastRefilled, current);
    if (timeSince > 0) {
      int refillRate = this.max;
      int newTokens = timeSince * refillRate;
      this.tokens = Math.min(tokens + newTokens, max);
      this.lastRefilled = current;
    }
  }

  private int elapsedSeconds(long start, long stop) {
    return (int) (stop - start) / MILLIS_IN_A_SEC;
  }
}
