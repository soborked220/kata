package com.soborked.kata.examples.ratelimiters.runner;

public enum Measure {
  AVERAGE,
  MIN,
  MAX;
}
