package com.soborked.kata.examples.ratelimiters.runner;

import com.soborked.kata.examples.ratelimiters.RateLimiter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class RequestLoader {

  private List<LoadSequence> runners;
  private Map<String, List<Attempt>> attemptsByRunner = new HashMap<>();

  public RequestLoader(LoadSequence... runners) {
    this.runners = Arrays.asList(runners);
  }

  public BatchSummary passesThrough(RateLimiter limiter) {
    long startTime = System.currentTimeMillis();
    CountDownLatch signal = new CountDownLatch(runners.size());
    runners.forEach(
        runner -> {
          new Thread(
                  () -> {
                    List<Attempt> attempted = runner.runWithin(limiter::allow);
                    attemptsByRunner.put(runner.getName(), attempted);
                    signal.countDown();
                  })
              .start();
        });
    waitUntil(signal);
    long stopTime = System.currentTimeMillis();
    return new BatchSummary(startTime, stopTime, attemptsByRunner);
  }

  private void waitUntil(CountDownLatch signal) {
    try {
      signal.await();
    } catch (InterruptedException e) {
      log.error("RequestBatch interrupted!");
    }
  }
}
