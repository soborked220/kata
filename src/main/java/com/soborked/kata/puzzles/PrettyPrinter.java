package com.soborked.kata.puzzles;

import java.util.Arrays;

public class PrettyPrinter<I, O> {

  private static final String MESSAGE =
      "\n%s\n\tBehavior: %s\n\tGiven: %s\n\tExpected: %s\n\tActual: %s";

  public String print(String name, String behavior, I given, O expected, O actual) {
    return String.format(MESSAGE, name, behavior, given, scrub(expected), scrub(actual));
  }

  private String scrub(Object value) {
    if (value == null) {
      return "NULL";
    }
    if (value.getClass().isArray()) {
      return prettyArray(value);
    }
    return value.toString();
  }

  private String prettyArray(Object value) {
    switch (value.getClass().getSimpleName()) {
      case "int[]":
        return Arrays.toString((int[]) value);
      case "short[]":
        return Arrays.toString((short[]) value);
      case "long[]":
        return Arrays.toString((long[]) value);
      case "double[]":
        return Arrays.toString((double[]) value);
      case "float[]":
        return Arrays.toString((float[]) value);
      case "char[]":
        return Arrays.toString((char[]) value);
      case "byte[]":
        return Arrays.toString((byte[]) value);
      case "boolean[]":
        return Arrays.toString((boolean[]) value);
      default:
        return Arrays.toString((Object[]) value);
    }
  }
}
