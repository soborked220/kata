package com.soborked.kata.puzzles;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Constraint<I, O> implements Verifiable<I, Exception>, Requirement {

  private final String name;
  private final String behavior;
  private final I input;
  private final Exception output;
  private final Criteria<Exception> criteria = (a, e) -> equals(a, e);
  private Exception actual;
  private final PrettyPrinter<I, Exception> printer = new PrettyPrinter<>();

  @Override
  public String name() {
    return name;
  }

  @Override
  public String behavior() {
    return behavior;
  }

  @Override
  public I given() {
    return input;
  }

  @Override
  public Exception expected() {
    return output;
  }

  @Override
  public Criteria<Exception> criteria() {
    return criteria;
  }

  @Override
  public boolean isSatisfiedBy(Solution solution) {
    try {
      solution.solve(given());
    } catch (Exception e) {
      actual = e;
      return criteria.isMet(e, expected());
    }
    return false;
  }

  @Override
  public String toString() {
    return printer.print(name(), behavior(), given(), expected(), actual);
  }

  private boolean equals(Exception actual, Exception expected) {
    return actual.getClass().equals(expected.getClass())
        && actual.getMessage().equals(expected.getMessage());
  }
}
