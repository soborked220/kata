package com.soborked.kata.puzzles.string.parse.intToRoman;

import com.soborked.kata.puzzles.Solution;

import java.util.List;

public class IntToRomanEnumSolution implements Solution<Integer, String> {

  @Override
  public String solve(Integer given) {
    StringBuilder s = new StringBuilder();
    while (given > 0) {
      List<Numeral> toCheck = Numeral.all();
      while (!toCheck.isEmpty()) {
        Numeral current = maxIn(toCheck);
        int bottomOfRange = current.lowerValue();
        while (given >= bottomOfRange) {
          if (given >= current.getValue()) {
            s.append(current.name());
            given -= current.getValue();
          } else {
            s.append(current.precededBy().map(Numeral::name).orElse(""));
            s.append(current.name());
            given -= bottomOfRange;
          }
        }
        shrink(toCheck);
      }
    }
    return s.toString();
  }

  private Numeral maxIn(List<Numeral> list) {
    return list.stream()
        .max(Numeral::compareTo)
        .orElseThrow(() -> new IllegalStateException("Should always find max"));
  }

  private void shrink(List<Numeral> list) {
    list.remove(list.size() - 1);
  }
}
