package com.soborked.kata.puzzles.string.parse.romanToInt;

import com.soborked.kata.puzzles.Solution;

import java.util.HashMap;
import java.util.Map;

public class RomanToIntMapSolution implements Solution<String, Integer> {

  private static final Map<String, Integer> NUMERALS = initialize();

  private static Map<String, Integer> initialize() {
    Map<String, Integer> numerals = new HashMap<>();
    numerals.put("I", 1);
    numerals.put("V", 5);
    numerals.put("X", 10);
    numerals.put("L", 50);
    numerals.put("C", 100);
    numerals.put("D", 500);
    numerals.put("M", 1000);
    return numerals;
  }

  @Override
  public Integer solve(String given) {
    int sum = 0;
    int current;
    int next;
    for (int i = 0; i < last(given); i++) {
      current = valueAt(given, i);
      next = valueAt(given, i + 1);
      if (current < next) {
        sum -= current;
      } else {
        sum += current;
      }
    }
    return sum + valueAt(given, last(given));
  }

  private int last(String given) {
    return given.length() - 1;
  }

  private int valueAt(String given, int i) {
    return NUMERALS.get(String.valueOf(given.charAt(i)));
  }
}
