package com.soborked.kata.puzzles.string.parse.intToRoman;

import com.soborked.kata.puzzles.Solution;

public class IntToRomanNaiveSolution implements Solution<Integer, String> {
  @Override
  public String solve(Integer given) {
    StringBuilder s = new StringBuilder();
    while (given > 0) {
      if (given >= 1000) {
        s.append("M");
        given -= 1000;
      } else if (given.equals(1000 - 100)) {
        s.append("C");
        s.append("M");
        given -= (1000 - 100);
      } else if (given >= 500) {
        s.append("D");
        given -= 500;
      } else if (given.equals(500 - 100)) {
        s.append("C");
        s.append("D");
        given -= (500 - 100);
      } else if (given >= 100) {
        s.append("C");
        given -= 100;
      } else if (given.equals(100 - 10)) {
        s.append("X");
        s.append("C");
        given -= (100 - 10);
      } else if (given >= 50) {
        s.append("L");
        given -= 50;
      } else if (given.equals(50 - 10)) {
        s.append("X");
        s.append("L");
        given -= (50 - 10);
      } else if (given >= 10) {
        s.append("X");
        given -= 10;
      } else if (given.equals(10 - 1)) {
        s.append("I");
        s.append("X");
        given -= (10 - 1);
      } else if (given >= 5) {
        s.append("V");
        given -= 5;
      } else if (given.equals(5 - 1)) {
        s.append("I");
        s.append("V");
        given -= (5 - 1);
      } else {
        s.append("I");
        given -= 1;
      }
    }
    return s.toString();
  }
}
