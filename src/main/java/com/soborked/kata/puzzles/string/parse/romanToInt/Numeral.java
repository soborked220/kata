package com.soborked.kata.puzzles.string.parse.romanToInt;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Numeral {
  I('I', 1),
  V('V', 5),
  X('X', 10),
  L('L', 50),
  C('C', 100),
  D('D', 500),
  M('M', 1000);

  private char name;
  @Getter private int value;

  public static Numeral of(char input) {
    return Numeral.valueOf(String.valueOf(input));
  }

  public boolean lessThan(Numeral next) {
    return this.value < next.value;
  }
}
