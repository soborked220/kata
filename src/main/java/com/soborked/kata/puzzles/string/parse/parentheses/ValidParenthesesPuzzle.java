package com.soborked.kata.puzzles.string.parse.parentheses;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/**
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the
 * input string is valid.
 *
 * <p>An input string is valid if:
 *
 * <p>Open brackets must be closed by the same type of brackets. Open brackets must be closed in the
 * correct order. Every close bracket has a corresponding open bracket of the same type.
 *
 * <p>Example 1:
 *
 * <p>Input: s = "()" Output: true
 *
 * <p>Example 2:
 *
 * <p>Input: s = "()[]{}" Output: true
 *
 * <p>Example 3:
 *
 * <p>Input: s = "(]" Output: false
 *
 * <p>Constraints:
 *
 * <p>1 <= s.length <= 104 s consists of parentheses only '()[]{}'.
 */
public class ValidParenthesesPuzzle implements Puzzle<String, Boolean> {

  private static final String MAX =
      "(((((((((((((((((((((((((((((((((((((((((((((((((({[]}))))))))))))))))))))))))))))))))))))))))))))))))))";

  // while right is less than length

  // l = 0 right = 1
  // lc = ( rc = [
  // lc and rc both open so
  //  shift l and r one to the right

  // l = 1 right = 2
  // lc = [ rc = ]
  // rc is closed so
  //  make sure they are same type
  //  move l left 1 and r right 1

  // l = 0 right = 3
  // lc = ( rc = )
  // rc is closed so
  //  make sure they are same type
  //  move l left 1 and r right 1

  @Override
  public Stream<UseCase<String, Boolean>> examples() {
    return Stream.of(
        new UseCase<>("(", "Single closures are not valid", "(", false, this::comparison),
        new UseCase<>("[", "Single closures are not valid", "[", false, this::comparison),
        new UseCase<>("{", "Single closures are not valid", "{", false, this::comparison),
        new UseCase<>("((", "Single closures are not valid", "((", false, this::comparison),
        new UseCase<>("([]", "Single closures are not valid", "([]", false, this::comparison),
        new UseCase<>("(){}}{", "Single closures are not valid", "(){}}{", false, this::comparison),
        new UseCase<>("([]){", "Single closures are not valid", "([]){", false, this::comparison),
        new UseCase<>(")(", "Out-of-order closures are not valid", ")(", false, this::comparison),
        new UseCase<>("][", "Out-of-order closures are not valid", "][", false, this::comparison),
        new UseCase<>(")(", "Out-of-order closures are not valid", "}{", false, this::comparison),
        new UseCase<>(
            "([)]", "Overlapping closures are not valid", "([)]", false, this::comparison),
        new UseCase<>(
            "()", "Closures with a left and right pair are valid", "()", true, this::comparison),
        new UseCase<>(
            "[]", "Closures with a left and right pair are valid", "[]", true, this::comparison),
        new UseCase<>(
            "{}", "Closures with a left and right pair are valid", "{}", true, this::comparison),
        new UseCase<>(
            "([])",
            "Closures are still valid if they contain other valid closures",
            "([])",
            true,
            this::comparison),
        new UseCase<>(
            "(([]){})",
            "Closures are still valid if they contain other valid closures",
            "(([]){})",
            true,
            this::comparison),
        new UseCase<>(
            "()[]{}",
            "Closures are valid if they are followed by other valid closures",
            "()[]{}",
            true,
            this::comparison),
        new UseCase<>(
            MAX, "Closures are valid up to the max size", "([])", true, this::comparison));
  }

  @Override
  public Stream<Constraint<String, Boolean>> constraints() {
    return null;
  }

  @Override
  public Stream<Performance<String, Boolean>> performance() {
    return null;
  }

  private boolean comparison(Boolean a, Boolean e) {
    return a.equals(e);
  }
}
