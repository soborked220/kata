package com.soborked.kata.puzzles.string.parse.romanToInt;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/**
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 *
 * <p>Symbol Value I 1 V 5 X 10 L 50 C 100 D 500 M 1000
 *
 * <p>For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written
 * as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
 *
 * <p>Roman numerals are usually written largest to smallest from left to right. However, the
 * numeral for four is not IIII. Instead, the number four is written as IV. Because the one is
 * before the five we subtract it making four. The same principle applies to the number nine, which
 * is written as IX. There are six instances where subtraction is used:
 *
 * <p>I can be placed before V (5) and X (10) to make 4 and 9. X can be placed before L (50) and C
 * (100) to make 40 and 90. C can be placed before D (500) and M (1000) to make 400 and 900.
 *
 * <p>Given a roman numeral, convert it to an integer.
 *
 * <p>Example 1:
 *
 * <p>Input: s = "III" Output: 3 Explanation: III = 3.
 *
 * <p>Example 2:
 *
 * <p>Input: s = "LVIII" Output: 58 Explanation: L = 50, V= 5, III = 3.
 *
 * <p>Example 3:
 *
 * <p>Input: s = "MCMXCIV" Output: 1994 Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
 *
 * <p>Constraints:
 *
 * <p>1 <= s.length <= 15 s contains only the characters ('I', 'V', 'X', 'L', 'C', 'D', 'M'). It is
 * guaranteed that s is a valid roman numeral in the range [1, 3999].
 */
public class RomanToIntPuzzle implements Puzzle<String, Integer> {
  @Override
  public Stream<UseCase<String, Integer>> examples() {
    return Stream.of(
        new UseCase<>("I", "I has a value of 1", "I", 1, this::comparison),
        new UseCase<>(
            "II", "I can add any lesser or equal values after: I", "II", 2, this::comparison),
        new UseCase<>(
            "III", "I can add any lesser or equal values after: II", "III", 3, this::comparison),
        new UseCase<>("IV", "Subtract if I before V", "IV", 4, this::comparison),
        new UseCase<>("V", "V has a value of 5", "V", 5, this::comparison),
        new UseCase<>("VI", "V can add any lesser values after: I", "VI", 6, this::comparison),
        new UseCase<>("IX", "Subtract if I before X", "IX", 9, this::comparison),
        new UseCase<>("X", "X has a value of 10", "X", 10, this::comparison),
        new UseCase<>(
            "XI", "X can add any lesser or equal values after: I", "XI", 11, this::comparison),
        new UseCase<>(
            "XV", "X can add any lesser or equal values after: V", "XV", 15, this::comparison),
        new UseCase<>(
            "XX", "X can add any lesser or equal values after: X", "XX", 20, this::comparison),
        new UseCase<>("XL", "Subtract if X before L", "XL", 40, this::comparison),
        new UseCase<>("L", "L has a value of 50", "L", 50, this::comparison),
        new UseCase<>("LI", "L can add any lesser values after: I", "LI", 51, this::comparison),
        new UseCase<>("LV", "L can add any lesser values after: V", "LV", 55, this::comparison),
        new UseCase<>("LX", "L can add any lesser values after: X", "LX", 60, this::comparison),
        new UseCase<>("XC", "Subtract if X before C", "XC", 90, this::comparison),
        new UseCase<>("C", "C has a value of 100", "C", 100, this::comparison),
        new UseCase<>(
            "CI", "C can add any lesser or equal values after: I", "CI", 101, this::comparison),
        new UseCase<>(
            "CV", "C can add any lesser or equal values after: V", "CV", 105, this::comparison),
        new UseCase<>(
            "CX", "C can add any lesser or equal values after: X", "CX", 110, this::comparison),
        new UseCase<>(
            "CL", "C can add any lesser or equal values after: L", "CL", 150, this::comparison),
        new UseCase<>(
            "CC", "C can add any lesser or equal values after: C", "CC", 200, this::comparison),
        new UseCase<>("CD", "Subtract if C before D", "CD", 400, this::comparison),
        new UseCase<>("D", "D has a value of 500", "D", 500, this::comparison),
        new UseCase<>("DI", "D can add any lesser values after: I", "DI", 501, this::comparison),
        new UseCase<>("DV", "D can add any lesser values after: V", "DV", 505, this::comparison),
        new UseCase<>("DX", "D can add any lesser values after: X", "DX", 510, this::comparison),
        new UseCase<>("DL", "D can add any lesser values after: L", "DL", 550, this::comparison),
        new UseCase<>("DC", "D can add any lesser values after: C", "DC", 600, this::comparison),
        new UseCase<>("CM", "Subtract if C before M", "CM", 900, this::comparison),
        new UseCase<>("M", "M has a value of 1000", "M", 1000, this::comparison),
        new UseCase<>(
            "MI", "M can add any lesser or equal values after: I", "MI", 1001, this::comparison),
        new UseCase<>(
            "MV", "M can add any lesser or equal values after: V", "MV", 1005, this::comparison),
        new UseCase<>(
            "MX", "M can add any lesser or equal values after: X", "MX", 1010, this::comparison),
        new UseCase<>(
            "ML", "M can add any lesser or equal values after: L", "ML", 1050, this::comparison),
        new UseCase<>(
            "MC", "M can add any lesser or equal values after: C", "MC", 1100, this::comparison),
        new UseCase<>(
            "MD", "M can add any lesser or equal values after: D", "MD", 1500, this::comparison),
        new UseCase<>(
            "MM", "M can add any lesser or equal values after: M", "MM", 2000, this::comparison),
        new UseCase<>(
            "Max", "Max roman numeral value is 3999", "MMMCMXCIX", 3999, this::comparison));
  }

  private boolean comparison(Integer a, Integer e) {
    return a.equals(e);
  }

  @Override
  public Stream<Constraint<String, Integer>> constraints() {
    return Stream.empty();
  }

  @Override
  public Stream<Performance<String, Integer>> performance() {
    return Stream.empty();
  }
}
