package com.soborked.kata.puzzles.string.substring.longest;

import com.soborked.kata.puzzles.Solution;

public class LongestSubstringNaiveSolution implements Solution<String, Integer> {

  @Override
  public Integer solve(String given) {
    String longest = "";
    StringBuilder current = new StringBuilder();
    char[] characters = given.toCharArray();
    for (char next : characters) {
      int lastOccurrence = current.toString().indexOf(String.valueOf(next));
      if (hitsRepeat(lastOccurrence)) {
        if (exceedsMax(current, longest)) {
          longest = current.toString();
        }
        rewind(current, lastOccurrence);
      }
      current.append(next);
    }
    if (exceedsMax(current, longest)) {
      longest = current.toString();
    }
    return longest.length();
  }

  private boolean hitsRepeat(int existing) {
    return existing >= 0;
  }

  private boolean exceedsMax(StringBuilder current, String longest) {
    return current.length() > longest.length();
  }

  private void rewind(StringBuilder current, int lastOccurrence) {
    if (lastOccurrence == 0) {
      current.deleteCharAt(0);
    } else {
      current.delete(0, lastOccurrence + 1);
    }
  }
}
