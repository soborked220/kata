package com.soborked.kata.puzzles.string.parse.intToRoman;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/**
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 *
 * <p>Symbol Value I 1 V 5 X 10 L 50 C 100 D 500 M 1000
 *
 * <p>For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written
 * as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
 *
 * <p>Roman numerals are usually written largest to smallest from left to right. However, the
 * numeral for four is not IIII. Instead, the number four is written as IV. Because the one is
 * before the five we subtract it making four. The same principle applies to the number nine, which
 * is written as IX. There are six instances where subtraction is used:
 *
 * <p>I can be placed before V (5) and X (10) to make 4 and 9. X can be placed before L (50) and C
 * (100) to make 40 and 90. C can be placed before D (500) and M (1000) to make 400 and 900.
 *
 * <p>Given an integer, convert it to roman numerals.
 *
 * <p>Example 1:
 *
 * <p>Input: 3 Output: s = "III" Explanation: 3 = III.
 *
 * <p>Example 2:
 *
 * <p>Input: 58 Output: s = "LVIII" Explanation: 50 = L, 5 = V, 3 = III.
 *
 * <p>Example 3:
 *
 * <p>Input: 1994 Output: s = "MCMXCIV" Explanation: 1000 = M, 900 = CM, 90 = XC and 4 = IV.
 *
 * <p>Constraints: 1 <= i <= 3999
 */
public class IntToRomanPuzzle implements Puzzle<Integer, String> {
  @Override
  public Stream<UseCase<Integer, String>> examples() {
    return Stream.of(
        new UseCase<>("1", "1 is written as I", 1, "I", this::comparison),
        new UseCase<>("2", "2 is the sum of two 1s", 2, "II", this::comparison),
        new UseCase<>("3", "3 is the sum of three 1s", 3, "III", this::comparison),
        new UseCase<>("5", "5 is written as V", 5, "V", this::comparison),
        new UseCase<>("6", "6 is the sum of one 5 and one 1", 6, "VI", this::comparison),
        new UseCase<>("7", "7 is the sum of one 5 and two 1s", 7, "VII", this::comparison),
        new UseCase<>("10", "10 is written as X", 10, "X", this::comparison),
        new UseCase<>("11", "11 is the sum of one 10 and one 1", 11, "XI", this::comparison),
        new UseCase<>("15", "15 is the sum of one 10 and one 5", 15, "XV", this::comparison),
        new UseCase<>(
            "16", "16 is the sum of one 10 and one 5 and one 1", 16, "XVI", this::comparison),
        new UseCase<>("50", "50 is written as L", 50, "L", this::comparison),
        new UseCase<>("100", "100 is written as C", 100, "C", this::comparison),
        new UseCase<>("500", "500 is written as D", 500, "D", this::comparison),
        new UseCase<>("1000", "1000 is written as M", 1000, "M", this::comparison),
        new UseCase<>("4", "4 is one 1 less than 5", 4, "IV", this::comparison),
        new UseCase<>("9", "9 is one 1 less than 10", 9, "IX", this::comparison),
        new UseCase<>("40", "40 is one 10 less than 50", 40, "XL", this::comparison),
        new UseCase<>("90", "90 is one 10 less than 100", 90, "XC", this::comparison),
        new UseCase<>("400", "400 is one 100 less than 500", 400, "CD", this::comparison),
        new UseCase<>("900", "900 is one 100 less than 1000", 900, "CM", this::comparison));
  }

  @Override
  public Stream<Constraint<Integer, String>> constraints() {
    return null;
  }

  @Override
  public Stream<Performance<Integer, String>> performance() {
    return null;
  }

  private boolean comparison(String a, String e) {
    return a.equals(e);
  }
}
