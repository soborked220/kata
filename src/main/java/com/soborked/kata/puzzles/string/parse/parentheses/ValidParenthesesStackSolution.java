package com.soborked.kata.puzzles.string.parse.parentheses;

import com.soborked.kata.puzzles.Solution;

import java.util.Stack;

public class ValidParenthesesStackSolution implements Solution<String, Boolean> {
  @Override
  public Boolean solve(String given) {
    Stack<Character> stack = new Stack<>();
    int n = given.length();

    for (int i = 0; i < n; i++) {
      char c = given.charAt(i);
      if (isOpen(c)) {
        stack.push(c);
      } else {
        if (stack.isEmpty()) {
          return false;
        }
        char top = stack.peek();

        if (match(c, top)) {
          stack.pop();
        } else {
          return false;
        }
      }
    }

    return stack.isEmpty();
  }

  private boolean match(char c, char top) {
    return (c == ')' && top == '(') || (c == ']' && top == '[') || (c == '}' && top == '{');
  }

  private boolean isOpen(char c) {
    return c == '(' || c == '[' || c == '{';
  }
}
