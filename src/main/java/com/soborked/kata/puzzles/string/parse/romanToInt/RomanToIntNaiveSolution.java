package com.soborked.kata.puzzles.string.parse.romanToInt;

import com.soborked.kata.puzzles.Solution;

public class RomanToIntNaiveSolution implements Solution<String, Integer> {

  @Override
  public Integer solve(String given) {
    int sum = 0;
    for (int i = 0; i < given.length(); i++) {
      char current = given.charAt(i);
      if (current == 'I') {
        sum += checkForSubtract(given, i, "VX", 1);
      } else if (current == 'V') {
        sum += checkForSubtract(given, i, "", 5);
      } else if (current == 'X') {
        sum += checkForSubtract(given, i, "LC", 10);
      } else if (current == 'L') {
        sum += checkForSubtract(given, i, "", 50);
      } else if (current == 'C') {
        sum += checkForSubtract(given, i, "DM", 100);
      } else if (current == 'D') {
        sum += checkForSubtract(given, i, "", 500);
      } else if (current == 'M') {
        sum += checkForSubtract(given, i, "", 1000);
      }
    }
    return sum;
  }

  private int checkForSubtract(String given, int i, String candidates, int value) {
    if (!isLast(given, i) && candidates.contains(String.valueOf(given.charAt(i + 1)))) {
      return -value;
    } else {
      return value;
    }
  }

  private boolean isLast(String given, int i) {
    return i == given.length() - 1;
  }
}
