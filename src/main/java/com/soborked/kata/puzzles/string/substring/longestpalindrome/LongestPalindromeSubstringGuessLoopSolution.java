package com.soborked.kata.puzzles.string.substring.longestpalindrome;

import com.soborked.kata.puzzles.Solution;

public class LongestPalindromeSubstringGuessLoopSolution implements Solution<String, String> {

  private static final int INCLUSIVE = 1;
  private int startIndexOfMax = 0;
  private int endIndexOfMax = 0;

  @Override
  public String solve(String given) {
    startIndexOfMax = 0;
    endIndexOfMax = -1;

    int inputLength = given.length();
    if (inputLength < 2) {
      return given;
    }
    for (int i = 0; i < inputLength - 1; i++) {
      findLongestOddLengthPalindromeStartingAt(given, i);
      findLongestEvenLengthPalindromeStartingAt(given, i);
    }
    return given.substring(startIndexOfMax, endIndexOfMax + INCLUSIVE);
  }

  private void findLongestOddLengthPalindromeStartingAt(String target, int center) {
    extendPalindrome(target, center, center);
  }

  private void findLongestEvenLengthPalindromeStartingAt(String target, int centerLeft) {
    extendPalindrome(target, centerLeft, centerLeft + 1);
  }

  private void extendPalindrome(String target, int start, int end) {
    boolean isPalindrome = false;
    while (inBounds(start, end, target) && isPalindrome(start, end, target)) {
      // expand
      start--;
      end++;
      isPalindrome = true;
    }
    if (isPalindrome) {
      // rewind since we always go one step too far
      start++;
      end--;
      if (length(startIndexOfMax, endIndexOfMax) < length(start, end)) {
        startIndexOfMax = start;
        endIndexOfMax = end;
      }
    }
  }

  private boolean inBounds(int start, int end, String target) {
    return start >= 0 && end < target.length();
  }

  private boolean isPalindrome(int start, int end, String target) {
    return target.charAt(start) == target.charAt(end);
  }

  private int length(int left, int right) {
    return right - left + 1; // add one since zero indexed
  }
}
