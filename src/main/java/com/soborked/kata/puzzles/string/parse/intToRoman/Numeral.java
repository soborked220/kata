package com.soborked.kata.puzzles.string.parse.intToRoman;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@AllArgsConstructor
public enum Numeral {
  I('I', 1, null),
  V('V', 5, Numeral.I),
  X('X', 10, Numeral.I),
  L('L', 50, Numeral.X),
  C('C', 100, Numeral.X),
  D('D', 500, Numeral.C),
  M('M', 1000, Numeral.C);

  private char name;
  @Getter private int value;

  private Numeral precededBy;

  public static Numeral of(char input) {
    return Numeral.valueOf(String.valueOf(input));
  }

  public static List<Numeral> all() {
    return Arrays.stream(Numeral.values()).collect(toList());
  }

  public Optional<Numeral> precededBy() {
    return Optional.ofNullable(precededBy);
  }

  public int lowerValue() {
    return this.getValue() - this.variance();
  }

  private int variance() {
    return this.precededBy().map(Numeral::getValue).orElse(0);
  }
}
