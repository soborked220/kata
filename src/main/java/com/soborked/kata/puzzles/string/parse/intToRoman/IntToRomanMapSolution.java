package com.soborked.kata.puzzles.string.parse.intToRoman;

import com.soborked.kata.puzzles.Solution;

import java.util.HashMap;
import java.util.Map;

public class IntToRomanMapSolution implements Solution<Integer, String> {

  private static final Map<Integer, String> NUMERALS = init();

  private static Map<Integer, String> init() {
    Map<Integer, String> map = new HashMap<>();
    map.put(1, "I");
    map.put(4, "IV");
    map.put(5, "V");
    map.put(9, "IX");
    map.put(10, "X");
    map.put(40, "XL");
    map.put(50, "L");
    map.put(90, "XC");
    map.put(100, "C");
    map.put(400, "CD");
    map.put(500, "D");
    map.put(900, "CM");
    map.put(1000, "M");
    return map;
  }

  @Override
  public String solve(Integer given) {
    StringBuilder s = new StringBuilder();
    int cur = startNumeral();
    while (given > 0) {
      while (given >= cur) {
        s.append(NUMERALS.get(cur));
        given -= cur;
      }
      cur = nextNumeral(cur);
    }
    return s.toString();
  }

  private int startNumeral() {
    return 1000;
  }

  private int nextNumeral(Integer current) {
    return NUMERALS.keySet().stream()
        .filter(numeral -> numeral < current)
        .max(Integer::compare)
        .orElse(0);
  }
}
