package com.soborked.kata.puzzles.string.substring.longest;

import com.soborked.kata.puzzles.Solution;

public class LongestSubstringOptimizedSolution implements Solution<String, Integer> {

  @Override
  public Integer solve(String given) {
    int longest = 0;
    int[] positionOfCharInWindow = new int[256]; // Relies on ASCII int assignments (0-256)

    for (int endOfWindow = 0, startOfWindow = 0; endOfWindow < given.length(); endOfWindow++) {
      char next = given.charAt(endOfWindow);
      int lastOccurrence = positionOfCharInWindow[next];
      if (hitRepeat(lastOccurrence)) {
        startOfWindow = lastOccurrence;
      }
      positionOfCharInWindow[next] = endOfWindow + 1;
      longest = Math.max(longest, currentLength(startOfWindow, endOfWindow));
    }
    return longest;
  }

  private boolean hitRepeat(int lastOccurrence) {
    return lastOccurrence > 0;
  }

  private int currentLength(int startOfWindow, int endOfWindow) {
    return endOfWindow - startOfWindow + 1;
  }
}
