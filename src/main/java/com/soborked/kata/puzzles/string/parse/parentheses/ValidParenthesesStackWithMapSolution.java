package com.soborked.kata.puzzles.string.parse.parentheses;

import com.soborked.kata.puzzles.Solution;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ValidParenthesesStackWithMapSolution implements Solution<String, Boolean> {

  private static final Map<Character, Character> OPENINGS =
      new HashMap<Character, Character>() {
        {
          put('(', ')');
          put('{', '}');
          put('[', ']');
        }
      };

  @Override
  public Boolean solve(String given) {
    Stack<Character> stack = new Stack<>();
    int n = given.length();

    for (int i = 0; i < n; i++) {
      char cur = given.charAt(i);
      if (isOpen(cur)) {
        stack.push(cur);
      } else {
        if (stack.isEmpty()) {
          return false;
        }
        char top = stack.peek();

        if (match(cur, top)) {
          stack.pop();
        } else {
          return false;
        }
      }
    }

    return stack.isEmpty();
  }

  private boolean match(char c, char top) {
    return OPENINGS.containsKey(top) && c == OPENINGS.get(top);
  }

  private boolean isOpen(char cur) {
    return OPENINGS.containsKey(cur);
  }
}
