package com.soborked.kata.puzzles.string.substring.longest;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/**
 * Given a string s, find the length of the longest substring without repeating characters.
 *
 * <p>Example 1:
 *
 * <p>Input: s = "abcabcbb" Output: 3 Explanation: The answer is "abc", with the length of 3.
 *
 * <p>Example 2:
 *
 * <p>Input: s = "bbbbb" Output: 1 Explanation: The answer is "b", with the length of 1.
 *
 * <p>Example 3:
 *
 * <p>Input: s = "pwwkew" Output: 3 Explanation: The answer is "wke", with the length of 3. Notice
 * that the answer must be a substring, "pwke" is a subsequence and not a substring.
 *
 * <p>Example 4:
 *
 * <p>Input: s = "" Output: 0
 *
 * <p>Constraints:
 *
 * <p>0 <= s.length <= 5 * 104 s consists of English letters, digits, symbols and spaces.
 */
public class LongestSubstringPuzzle implements Puzzle<String, Integer> {
  @Override
  public Stream<UseCase<String, Integer>> examples() {
    return Stream.of(
        new UseCase<>("Zero", "There is no substring if input is empty", "", 0, this::comparison),
        new UseCase<>(
            "One",
            "A single character does not count as a repeat",
            "a",
            1,
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "First of Repeat",
            "The first character in a repeating string still counts",
            "aaa",
            1,
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Repeat Trailing",
            "The repeat might trail a non-repeating substring",
            "abcc",
            3,
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Repeat Preceding",
            "The repeat might precede a non-repeating substring",
            "abccdefg",
            5,
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Non-Consecutive Repeat",
            "The repeated character might not be consecutive",
            "abcdbef",
            5,
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Non-Consecutive Repeat",
            "The repeated character might not be consecutive",
            "dvdf",
            3,
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Allows digits",
            "The substring can contain digits",
            "abc1234567890",
            13,
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Allows symbols",
            "The substring can contain symbols",
            "abc!@#$%^&*()",
            13,
            (a, e) -> comparison(a, e)));
  }

  @Override
  public Stream<Constraint<String, Integer>> constraints() {
    return Stream.of();
  }

  @Override
  public Stream<Performance<String, Integer>> performance() {
    return Stream.of();
  }

  private boolean comparison(Integer actual, Integer expected) {
    return actual != null && actual.equals(expected);
  }
  // TODO need to add comparison to interface to force it
  // TODO also bake in some null checks to avoid null pointers

}
