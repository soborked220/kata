package com.soborked.kata.puzzles.string.parse.romanToInt;

import com.soborked.kata.puzzles.Solution;

public class RomanToIntEnumSolution implements Solution<String, Integer> {

  @Override
  public Integer solve(String given) {
    int sum = 0;
    for (int i = 0; i < given.length() - 1; i++) {
      Numeral current = Numeral.of(given.charAt(i));
      Numeral next = Numeral.of(given.charAt(i + 1));
      if (current.lessThan(next)) {
        sum -= current.getValue();
      } else {
        sum += current.getValue();
      }
    }
    return sum + Numeral.of(given.charAt(last(given))).getValue();
  }

  private int last(String given) {
    return given.length() - 1;
  }
}
