package com.soborked.kata.puzzles.string.substring.longestpalindrome;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

public class LongestPalindromeSubstringPuzzle implements Puzzle<String, String> {

  @Override
  public Stream<UseCase<String, String>> examples() {
    return Stream.of(
        new UseCase<>(
            "One", "A single character counts as a palindrome", "a", "a", this::comparison),
        new UseCase<>(
            "First",
            "If multiple palindromes exist, return the first",
            "ac",
            "a",
            this::comparison),
        new UseCase<>(
            "First",
            "Multiple copies of the same palindrome can exist",
            "aacabdkacaa",
            "aca",
            this::comparison),
        new UseCase<>(
            "Start",
            "Palindrome can be in the start of the string",
            "aacd",
            "aa",
            this::comparison),
        new UseCase<>(
            "Middle",
            "Palindrome can be in the middle of the string",
            "caad",
            "aa",
            this::comparison),
        new UseCase<>(
            "End", "Palindrome can be in the end of the string", "cdaa", "aa", this::comparison),
        new UseCase<>(
            "Not Just Duplicates",
            "Palindrome can consist of multiple distinct characters",
            "babad",
            "bab",
            this::comparison),
        new UseCase<>(
            "Largest Found",
            "Return the largest Palindrome if multiple exist",
            "zcababadefe",
            "ababa",
            this::comparison));
  }

  @Override
  public Stream<Constraint<String, String>> constraints() {
    return Stream.of();
  }

  @Override
  public Stream<Performance<String, String>> performance() {
    return Stream.of();
  }

  private boolean comparison(String actual, String expected) {
    return actual != null && actual.equals(expected);
  }
}
