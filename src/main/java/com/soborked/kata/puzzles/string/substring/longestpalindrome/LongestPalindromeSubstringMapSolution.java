package com.soborked.kata.puzzles.string.substring.longestpalindrome;

import com.soborked.kata.puzzles.Solution;

import java.util.*;

public class LongestPalindromeSubstringMapSolution implements Solution<String, String> {

  @Override
  public String solve(String s) {
    if (s.length() == 1) {
      return s;
    }
    String longestPalindrome = "";
    Map<Character, List<Integer>> charOccurrences = new HashMap<>();
    for (int i = 0; i < s.length(); i++) {
      char first = s.charAt(i);
      charOccurrences.putIfAbsent(first, new ArrayList<>());
      charOccurrences.get(first).add(i);
    }

    for (Map.Entry<Character, List<Integer>> entry : charOccurrences.entrySet()) {
      List<Integer> indices = entry.getValue();
      if (indices.size() > 1) {
        for (int start = 0; start < indices.size() - 1; start++) {
          int startIndex = indices.get(start);
          for (int end = indices.size() - 1; end > start; end--) {
            int endIndex = indices.get(end);
            if (isPalindrome(s, startIndex, endIndex)) {
              if (shouldReplace(startIndex, endIndex, longestPalindrome, charOccurrences)) {
                longestPalindrome = s.substring(startIndex, endIndex + 1);
              }
              break;
            }
          }
        }
      }
    }

    return longestPalindrome == "" ? String.valueOf(s.charAt(0)) : longestPalindrome;
  }

  private boolean isPalindrome(String candidate, int forwardIndex, int backwardIndex) {
    while (forwardIndex < backwardIndex) {
      if (candidate.charAt(forwardIndex) == candidate.charAt(backwardIndex)) {
        forwardIndex++;
        backwardIndex--;
      } else {
        return false;
      }
    }
    return true;
  }

  private boolean shouldReplace(
      int start, int end, String longest, Map<Character, List<Integer>> charOccurrences) {
    int candidateLength = end - start + 1;
    return isLonger(candidateLength, longest)
        || isTied(candidateLength, longest) && isEarlier(start, longest, charOccurrences);
  }

  private boolean isLonger(int candidate, String longest) {
    return candidate > longest.length();
  }

  private boolean isTied(int candidate, String longest) {
    return candidate == longest.length();
  }

  private boolean isEarlier(
      int start, String longest, Map<Character, List<Integer>> charOccurrences) {
    return start
        < charOccurrences.get(longest.charAt(0)).stream()
            .findFirst()
            .orElseThrow(() -> new IllegalStateException("Occurrences must not be empty"));
  }
}
