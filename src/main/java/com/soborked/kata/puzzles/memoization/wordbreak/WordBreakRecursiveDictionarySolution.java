package com.soborked.kata.puzzles.memoization.wordbreak;

import com.soborked.kata.puzzles.Solution;

import java.util.*;

public class WordBreakRecursiveDictionarySolution implements Solution<WordBreakInput, Boolean> {

  private static final int SOLVED = 1;
  private static final int NOT_SOLVED = -SOLVED;

  @Override
  public Boolean solve(WordBreakInput given) {
    String input = given.getWord();
    Set<String> dictionary = new HashSet<>(given.getDictionary());
    byte[] solved = new byte[input.length()];
    Arrays.fill(solved, (byte) NOT_SOLVED);

    return wordBreak(input, 0, new ArrayList<>(dictionary), solved);
  }

  private boolean wordBreak(String input, int position, List<String> dictionary, byte[] solved) {
    if (hasReachedEnd(input, position)) {
      return true;
    }

    if (solved[position] != NOT_SOLVED) {
      return solved[position] == SOLVED;
    }

    byte match = 0;
    for (int i = 0; i < dictionary.size(); i++) {
      String word = dictionary.get(i);
      if (input.startsWith(word, position)
          && wordBreak(input, remaining(position, word), dictionary, solved)) {
        match = SOLVED;
        break;
      }
    }

    solved[position] = match;

    return match == SOLVED;
  }

  private int remaining(int position, String word) {
    return position + word.length();
  }

  private boolean hasReachedEnd(String input, int position) {
    return position >= input.length();
  }
}
