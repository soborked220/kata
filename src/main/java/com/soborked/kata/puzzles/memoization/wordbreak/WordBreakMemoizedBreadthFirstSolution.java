package com.soborked.kata.puzzles.memoization.wordbreak;

import com.soborked.kata.puzzles.Solution;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class WordBreakMemoizedBreadthFirstSolution implements Solution<WordBreakInput, Boolean> {
  @Override
  public Boolean solve(WordBreakInput given) {
    String input = given.getWord();
    int end = input.length();
    Set<String> dictionary = new HashSet<>(given.getDictionary());

    Stack<Integer> stack = new Stack<>();
    stack.add(0);
    Set<Integer> visited = new HashSet<>();
    while (!stack.isEmpty()) {
      int start = stack.peek();
      stack.pop();
      if (visited.contains(start)) {
        continue;
      }
      visited.add(start);
      String piece = "";
      for (int position = start; position < end; position++) {
        piece += input.charAt(position);
        if (dictionary.contains(piece)) {
          stack.push(position + 1);
          if (position + 1 == end) {
            return true;
          }
        }
      }
    }
    return false;
  }
}
