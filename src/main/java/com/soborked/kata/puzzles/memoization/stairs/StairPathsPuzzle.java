package com.soborked.kata.puzzles.memoization.stairs;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/**
 * You are climbing a staircase. It takes n steps to reach the top.
 *
 * <p>Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the
 * top?
 *
 * <p>Example 1:
 *
 * <p>Input: n = 2 Output: 2 Explanation: There are two ways to climb to the top. 1. 1 step + 1 step
 * 2. 2 steps
 *
 * <p>Example 2:
 *
 * <p>Input: n = 3 Output: 3 Explanation: There are three ways to climb to the top. 1. 1 step + 1
 * step + 1 step 2. 1 step + 2 steps 3. 2 steps + 1 step
 *
 * <p>Input: 4 Output: 5 1 1 1 1 1 1 2 1 2 1 2 1 1 2 2
 *
 * <p>Input: 5 Output: 8 1 1 1 1 1 1 1 1 2 1 1 2 1 1 2 1 1 2 1 1 1 1 2 2 2 1 2 2 2 1
 *
 * <p>Input: 6 Output: 12 1 1 1 1 1 1 1 1 1 1 2 1 1 1 2 1 1 1 2 1 1 1 2 1 1 1 2 1 1 1 1 1 1 2 2 1 2
 * 1 2 2 1 1 2 2 1 2 1 2 2 1 1 2 2 2
 *
 * <p>Input: 8 Output: 26 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1 2 1 1 1 1 1 2 1 1 1 1 1 2 1 1 1 1
 * 1 2 1 1 1 1 1 2 1 1 1 1 1 2 1 1 1 1 1 1 1 1 1 1 2 2 1 1 1 2 1 2 1 1 2 1 1 2 1 2 1 1 1 2 2 1 1 1 1
 * 2 2 1 1 1 2 1 2 1 1 2 1 1 2 1 2 1 1 1 2 2 1 1 1 1 1 1 2 2 2 1 2 1 2 2 2 1 1 2 2 2 1 2 1 2 2 2 1 1
 * 2 2 2 1 2 1 2 2 2 1 1 2 2 2 2
 *
 * <p>Constraints:
 *
 * <p>1 <= n <= 45
 */
public class StairPathsPuzzle implements Puzzle<Integer, Integer> {
  @Override
  public Stream<UseCase<Integer, Integer>> examples() {
    return Stream.of(
        new UseCase<>("1", "One stair has only one path", 1, 1, this::comparison),
        new UseCase<>(
            "2", "Stairs can be climbed one at a time or two at a time", 2, 2, this::comparison),
        new UseCase<>(
            "3", "Stairs can be climbed one at a time or two at a time", 3, 3, this::comparison),
        new UseCase<>(
            "4", "Stairs can be climbed one at a time or two at a time", 4, 5, this::comparison));
  }

  @Override
  public Stream<Constraint<Integer, Integer>> constraints() {
    return null;
  }

  @Override
  public Stream<Performance<Integer, Integer>> performance() {
    return null;
  }

  private boolean comparison(Integer a, Integer e) {
    return a.equals(e);
  }
}
