package com.soborked.kata.puzzles.memoization.stairs;

import com.soborked.kata.puzzles.Solution;

public class StairPathsClimbSolution implements Solution<Integer, Integer> {

  @Override
  public Integer solve(Integer given) {
    if (given <= 2) {
      return given;
    }
    int previous = 2;
    int twoPrevious = 1;
    int current = previous + twoPrevious;
    for (int step = 3; step <= given; step++) {
      current = previous + twoPrevious;
      twoPrevious = previous;
      previous = current;
    }
    return current;
  }
}
