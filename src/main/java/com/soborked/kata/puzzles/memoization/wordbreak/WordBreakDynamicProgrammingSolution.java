package com.soborked.kata.puzzles.memoization.wordbreak;

import com.soborked.kata.puzzles.Solution;

import java.util.HashSet;
import java.util.Set;

// Inefficient because it repeatedly checks letters
public class WordBreakDynamicProgrammingSolution implements Solution<WordBreakInput, Boolean> {

  @Override
  public Boolean solve(WordBreakInput given) {
    String input = given.getWord();
    int end = input.length();
    boolean[] solved = new boolean[end + 1];
    solved[end] = true;

    Set<String> dictionary = new HashSet<>(given.getDictionary());
    for (int left = end - 1; left >= 0; left--) {
      for (int right = left + 1; !solved[left] && right <= end; right++) {
        String piece = input.substring(left, right);
        solved[left] = (solved[right] && dictionary.contains(piece));
      }
    }

    return solved[0];
  }
}
