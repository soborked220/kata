package com.soborked.kata.puzzles.memoization.wordbreak;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class WordBreakInput {

  private String word;
  private List<String> dictionary;
}
