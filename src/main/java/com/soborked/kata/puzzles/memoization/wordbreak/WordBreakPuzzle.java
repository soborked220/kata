package com.soborked.kata.puzzles.memoization.wordbreak;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

import static java.util.Arrays.asList;

/**
 * Given a string s and a dictionary of strings wordDict, return true if s can be segmented into a
 * space-separated sequence of one or more dictionary words.
 *
 * <p>Note that the same word in the dictionary may be reused multiple times in the segmentation.
 *
 * <p>Example 1:
 *
 * <p>Input: s = "leetcode", wordDict = ["leet","code"] Output: true Explanation: Return true
 * because "leetcode" can be segmented as "leet code".
 *
 * <p>Example 2:
 *
 * <p>Input: s = "applepenapple", wordDict = ["apple","pen"] Output: true Explanation: Return true
 * because "applepenapple" can be segmented as "apple pen apple". Note that you are allowed to reuse
 * a dictionary word.
 *
 * <p>Example 3:
 *
 * <p>Input: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"] Output: false
 *
 * <p>Constraints:
 *
 * <p>1 <= s.length <= 300 1 <= wordDict.length <= 1000 1 <= wordDict[i].length <= 20 s and
 * wordDict[i] consist of only lowercase English letters. All the strings of wordDict are unique.
 */
public class WordBreakPuzzle implements Puzzle<WordBreakInput, Boolean> {

  @Override
  public Stream<UseCase<WordBreakInput, Boolean>> examples() {
    return Stream.of(
        new UseCase<>(
            "Split",
            "Input consists of two dictionary words",
            new WordBreakInput("leetcode", asList("leet", "code")),
            true,
            this::comparison),
        new UseCase<>(
            "Repeats",
            "Input repeats dictionary words",
            new WordBreakInput("applepenapple", asList("apple", "pen")),
            true,
            this::comparison),
        new UseCase<>(
            "Not all used",
            "Dictionary words may go unused",
            new WordBreakInput("cats", asList("cats", "dog")),
            true,
            this::comparison),
        new UseCase<>(
            "No reuse",
            "Input letters can only be used once",
            new WordBreakInput("catsandog", asList("cats", "dog", "sand", "and", "cat")),
            false,
            this::comparison),
        new UseCase<>(
            "All same",
            "Input letters can only be used once",
            new WordBreakInput("aaaaaaa", asList("aaaa", "aaa")),
            true,
            this::comparison));
  }

  @Override
  public Stream<Constraint<WordBreakInput, Boolean>> constraints() {
    return null;
  }

  @Override
  public Stream<Performance<WordBreakInput, Boolean>> performance() {
    return null;
  }

  private boolean comparison(Boolean a, Boolean e) {
    return a.equals(e);
  }
}
