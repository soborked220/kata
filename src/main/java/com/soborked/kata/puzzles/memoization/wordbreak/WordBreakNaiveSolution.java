package com.soborked.kata.puzzles.memoization.wordbreak;

import com.soborked.kata.puzzles.Solution;

import java.util.HashSet;
import java.util.Set;

public class WordBreakNaiveSolution implements Solution<WordBreakInput, Boolean> {
  @Override
  public Boolean solve(WordBreakInput given) {
    String input = given.getWord();
    Set<String> dictionary = new HashSet<>(given.getDictionary());
    return madeOfWords(input, dictionary);
  }

  private boolean madeOfWords(String[] pieces, Set<String> dictionary) {
    for (String piece : pieces) {
      if (piece.equals("")) {
        continue;
      }
      if (!madeOfWords(piece, dictionary)) {
        return false;
      }
    }
    return true;
  }

  private boolean madeOfWords(String piece, Set<String> dictionary) {
    if (dictionary.stream().noneMatch(piece::contains)) {
      return false;
    } else {
      return dictionary.stream()
          .anyMatch(
              word -> {
                if (piece.contains(word)) {
                  String[] chunks = piece.split(word);
                  return madeOfWords(chunks, dictionary);
                } else {
                  return false;
                }
              });
    }
  }
}
