package com.soborked.kata.puzzles.memoization.wordbreak;

import com.soborked.kata.puzzles.Solution;

import java.util.List;

public class WordBreakMemoizedDepthFirstSolution implements Solution<WordBreakInput, Boolean> {

  private Boolean[] solved;

  @Override
  public Boolean solve(WordBreakInput given) {
    String input = given.getWord();
    solved = new Boolean[input.length()];
    return isBreakableAtPosition(0, input, given.getDictionary());
  }

  private Boolean isBreakableAtPosition(int position, String input, List<String> dictionary) {
    int end = input.length();
    if (position == end) {
      return true;
    }
    if (solved[position] != null) {
      return solved[position];
    }
    for (int remaining = position + 1; remaining <= end; remaining++) {
      String frontPiece = input.substring(position, remaining);
      if (dictionary.contains(frontPiece) && isBreakableAtPosition(remaining, input, dictionary)) {
        solved[position] = true;
        return true;
      }
    }
    solved[position] = false;
    return false;
  }
}
