package com.soborked.kata.puzzles.memoization.wordbreak;

import com.soborked.kata.puzzles.Solution;

import java.util.HashSet;
import java.util.Set;

public class WordBreakSubstringDepthFirstSolution implements Solution<WordBreakInput, Boolean> {

  @Override
  public Boolean solve(WordBreakInput given) {
    String input = given.getWord();
    Set<String> dictionary = new HashSet<>(given.getDictionary());

    return isBreakableAtPosition(0, input, dictionary);
  }

  private Boolean isBreakableAtPosition(int position, String input, Set<String> dictionary) {
    int end = input.length();
    if (position == end) {
      return true;
    }
    for (int remaining = position + 1; remaining <= end; remaining++) {
      String frontPiece = input.substring(position, remaining);
      if (dictionary.contains(frontPiece) && isBreakableAtPosition(remaining, input, dictionary)) {
        return true;
      }
    }
    return false;
  }
}
