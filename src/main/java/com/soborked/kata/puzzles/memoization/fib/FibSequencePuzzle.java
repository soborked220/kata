package com.soborked.kata.puzzles.memoization.fib;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/**
 * The Fibonacci numbers, commonly denoted F(n) form a sequence, called the Fibonacci sequence, such
 * that each number is the sum of the two preceding ones, starting from 0 and 1. That is,
 *
 * <p>F(0) = 0, F(1) = 1 F(n) = F(n - 1) + F(n - 2), for n > 1.
 *
 * <p>Given n, calculate F(n).
 *
 * <p>Example 1:
 *
 * <p>Input: n = 2 Output: 1 Explanation: F(2) = F(1) + F(0) = 1 + 0 = 1.
 *
 * <p>Example 2:
 *
 * <p>Input: n = 3 Output: 2 Explanation: F(3) = F(2) + F(1) = 1 + 1 = 2.
 *
 * <p>Example 3:
 *
 * <p>Input: n = 4 Output: 3 Explanation: F(4) = F(3) + F(2) = 2 + 1 = 3.
 *
 * <p>Constraints:
 *
 * <p>0 <= n <= 30
 */
public class FibSequencePuzzle implements Puzzle<Integer, Integer> {

  @Override
  public Stream<UseCase<Integer, Integer>> examples() {
    return Stream.of(
        new UseCase<>("0", "Fibonacci starts at position 0", 0, 0, this::comparison),
        new UseCase<>("1", "Fibonacci of first position is 1", 1, 1, this::comparison),
        new UseCase<>("2", "Fibonacci is sum of the last two", 2, 1, this::comparison),
        new UseCase<>("3", "Fibonacci is sum of the last two", 3, 2, this::comparison),
        new UseCase<>("4", "Fibonacci is sum of the last two", 4, 3, this::comparison),
        new UseCase<>("5", "Fibonacci is sum of the last two", 5, 5, this::comparison),
        new UseCase<>("6", "Fibonacci is sum of the last two", 6, 8, this::comparison));
  }

  private boolean comparison(Integer a, Integer e) {
    return a.equals(e);
  }

  @Override
  public Stream<Constraint<Integer, Integer>> constraints() {
    return null;
  }

  @Override
  public Stream<Performance<Integer, Integer>> performance() {
    return null;
  }
}
