package com.soborked.kata.puzzles.memoization.stairs;

import com.soborked.kata.puzzles.Solution;

public class StairPathsMemoizedSolution implements Solution<Integer, Integer> {

  @Override
  public Integer solve(Integer given) {
    if (given <= 2) {
      return given;
    }
    int[] solved = new int[given + 1];
    return descend(given, solved);
  }

  private int descend(int current, int[] solved) {
    if (current <= 2) {
      return current;
    }
    if (solved[current] != 0) {
      return solved[current];
    }
    int solve = descend(current - 1, solved) + descend(current - 2, solved);
    solved[current] = solve;
    return solve;
  }
}
