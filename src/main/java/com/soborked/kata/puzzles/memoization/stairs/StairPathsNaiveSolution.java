package com.soborked.kata.puzzles.memoization.stairs;

import com.soborked.kata.puzzles.Solution;

public class StairPathsNaiveSolution implements Solution<Integer, Integer> {

  @Override
  public Integer solve(Integer given) {
    if (given <= 2) {
      return given;
    }
    return descend(given);
  }

  private int descend(int current) {
    if (current <= 2) {
      return current;
    }
    return descend(current - 1) + descend(current - 2);
  }
}
