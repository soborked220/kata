package com.soborked.kata.puzzles.memoization.fib;

import com.soborked.kata.puzzles.Solution;

public class FibSequenceNaiveSolution implements Solution<Integer, Integer> {
  @Override
  public Integer solve(Integer given) {
    if (given <= 1) {
      return given;
    }
    return fib(given);
  }

  private int fib(int current) {
    if (current <= 1) {
      return current;
    }
    return fib(current - 1) + fib(current - 2);
  }
}
