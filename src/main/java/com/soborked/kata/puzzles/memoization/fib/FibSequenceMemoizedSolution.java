package com.soborked.kata.puzzles.memoization.fib;

import com.soborked.kata.puzzles.Solution;

public class FibSequenceMemoizedSolution implements Solution<Integer, Integer> {
  @Override
  public Integer solve(Integer given) {
    int[] solved = new int[given + 1];
    if (given <= 1) {
      return given;
    }
    return fib(given, solved);
  }

  private int fib(int current, int[] solved) {
    if (current <= 1) {
      return current;
    }
    if (solved[current] != 0) {
      return solved[current];
    }
    return fib(current - 1, solved) + fib(current - 2, solved);
  }
}
