package com.soborked.kata.puzzles;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public interface Puzzle<I, O> {

  default List<Requirement> solveWith(Solution<I, O> solution) {
    List<Requirement> failures = new ArrayList<>();
    Stream.of(examples(), constraints(), performance())
        .flatMap(stream -> stream)
        .forEach(
            requirement -> {
              if (!requirement.isSatisfiedBy(solution)) {
                failures.add(requirement);
              }
            });
    return failures;
  }

  Stream<UseCase<I, O>> examples();

  Stream<Constraint<I, O>> constraints();

  Stream<Performance<I, O>> performance();
}
