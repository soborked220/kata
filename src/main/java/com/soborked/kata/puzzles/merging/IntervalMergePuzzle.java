package com.soborked.kata.puzzles.merging;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

/**
 * Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals,
 * and return an array of the non-overlapping intervals that cover all the intervals in the input.
 *
 * <p>Example 1:
 *
 * <p>Input: intervals = [[1,3],[2,6],[8,10],[15,18]] Output: [[1,6],[8,10],[15,18]] Explanation:
 * Since intervals [1,3] and [2,6] overlap, merge them into [1,6].
 *
 * <p>Example 2:
 *
 * <p>Input: intervals = [[1,4],[4,5]] Output: [[1,5]] Explanation: Intervals [1,4] and [4,5] are
 * considered overlapping.
 *
 * <p>Constraints:
 *
 * <p>1 <= intervals.length <= 10^4 intervals[i].length == 2 0 <= starti <= endi <= 10^4
 */
public class IntervalMergePuzzle implements Puzzle<List<Interval>, List<Interval>> {
  @Override
  public Stream<UseCase<List<Interval>, List<Interval>>> examples() {
    return Stream.of(
        new UseCase<>(
            "No overlap",
            "If two intervals do not overlap, do not merge them",
            asList(new Interval(0, 1), new Interval(3, 4)),
            asList(new Interval(0, 1), new Interval(3, 4)),
            this::comparison),
        new UseCase<>(
            "Subset",
            "If one interval is a subset of the other, collapse",
            asList(new Interval(0, 3), new Interval(1, 2)),
            asList(new Interval(0, 3)),
            this::comparison),
        new UseCase<>(
            "Edge",
            "If one interval's end is the start of the other, collapse",
            asList(new Interval(0, 1), new Interval(1, 2)),
            asList(new Interval(0, 2)),
            this::comparison),
        new UseCase<>(
            "Overlap",
            "If one interval's end is in the middle of the other, collapse",
            asList(new Interval(0, 2), new Interval(1, 3)),
            asList(new Interval(0, 3)),
            this::comparison),
        new UseCase<>(
            "All overlap",
            "If multiple intervals overlap, collapse",
            asList(new Interval(0, 2), new Interval(1, 4), new Interval(3, 6)),
            asList(new Interval(0, 6)),
            this::comparison),
        new UseCase<>(
            "Some overlap",
            "Only collapse those which do overlap",
            asList(new Interval(0, 2), new Interval(1, 4), new Interval(3, 6), new Interval(7, 8)),
            asList(new Interval(0, 6), new Interval(7, 8)),
            this::comparison),
        new UseCase<>(
            "Out of Order",
            "Order shouldn't matter",
            asList(new Interval(0, 2), new Interval(3, 6), new Interval(1, 4), new Interval(7, 8)),
            asList(new Interval(0, 6), new Interval(7, 8)),
            this::comparison));
  }

  private boolean comparison(List<Interval> a, List<Interval> e) {
    return a.equals(e);
  }

  @Override
  public Stream<Constraint<List<Interval>, List<Interval>>> constraints() {
    return null;
  }

  @Override
  public Stream<Performance<List<Interval>, List<Interval>>> performance() {
    return null;
  }
}
