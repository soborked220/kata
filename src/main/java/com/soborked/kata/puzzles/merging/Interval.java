package com.soborked.kata.puzzles.merging;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Arrays;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class Interval {

  private int min;
  private int max;

  @Override
  public String toString() {
    int[] range = new int[] {min, max};
    return Arrays.toString(range);
  }
}
