package com.soborked.kata.puzzles.merging;

import com.soborked.kata.puzzles.Solution;

import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparingInt;

public class IntervalMergeNaiveSolution implements Solution<List<Interval>, List<Interval>> {

  @Override
  public List<Interval> solve(List<Interval> given) {
    given.sort(comparingInt(Interval::getMin));
    List<Interval> merged = new ArrayList<>();
    Interval latestMerge = given.get(0);

    for (int i = 1; i < given.size(); i++) {
      Interval current = given.get(i);
      if (current.getMin() <= latestMerge.getMax()) {
        int newMax = Math.max(current.getMax(), latestMerge.getMax());
        latestMerge = new Interval(latestMerge.getMin(), newMax);
      } else {
        merged.add(latestMerge);
        latestMerge = current;
      }
    }

    merged.add(latestMerge);

    return merged;
  }
}
