package com.soborked.kata.puzzles;

public interface Criteria<V> {

  boolean isMet(V actual, V expected);
}
