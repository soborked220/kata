package com.soborked.kata.puzzles;

public interface Verifiable<G, E> {

  G given();

  E expected();

  Criteria<E> criteria();

  boolean isSatisfiedBy(Solution solution);
}
