package com.soborked.kata.puzzles.math.sums.ofTwo;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Given an array of integer candidates and an integer target, return indices of the two numbers
 * such that they add up to target. You may assume that each input would have exactly one solution,
 * and you may not use the same element twice. You can return the answer in any order.
 *
 * <p>Example 1: Input: candidates = [2,7,11,15], target = 9 Output: [0,1] Output: Because
 * candidates[0] + candidates[1] == 9, we return [0, 1].
 *
 * <p>Example 2: Input: candidates = [3,2,4], target = 6 Output: [1,2]
 *
 * <p>Example 3: Input: candidates = [3,3], target = 6 Output: [0,1]
 *
 * <p>Constraints: 2 <= candidates.length <= 104 -10^9 <= candidates[i] <= 10^9 -10^9 <= target <=
 * 10^9 Only one valid answer exists.
 */
public class SumOfTwoPuzzle implements Puzzle<SumOfTwoInput, int[]> {

  private static final int BILLION = 1000000000;
  private static final long NANO_3000 = 3000L;
  private static final long NANO_1500 = 1500L;

  @Override
  public Stream<UseCase<SumOfTwoInput, int[]>> examples() {
    return Stream.of(
        new UseCase<>(
            "Zeros",
            "Zero is a valid target sum",
            new SumOfTwoInput(new int[] {0, 0}, 0),
            new int[] {0, 1},
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Sum is First",
            "The pieces of the Sum can be at the start of the given",
            new SumOfTwoInput(new int[] {2, 7, 11, 15}, 9),
            new int[] {0, 1},
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Sum is Last",
            "The pieces of the Sum can be at the end of the given",
            new SumOfTwoInput(new int[] {11, 15, 2, 7}, 9),
            new int[] {2, 3},
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Sum is Copy",
            "The pieces of the Sum can be copies of each other",
            new SumOfTwoInput(new int[] {3, 3}, 6),
            new int[] {0, 1},
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Sum is not Adjacent",
            "The pieces of the Sum need not be adjacent",
            new SumOfTwoInput(new int[] {2, 3, 4}, 6),
            new int[] {0, 2},
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Target is Negative",
            "Negative numbers are a valid target sum",
            new SumOfTwoInput(new int[] {-1, 3, -3}, -4),
            new int[] {0, 2},
            (a, e) -> comparison(a, e)),
        new UseCase<>(
            "Sum is Balanced",
            "The Sum can mix positive and negative values",
            new SumOfTwoInput(new int[] {-5, 3, 5}, 0),
            new int[] {0, 2},
            (a, e) -> comparison(a, e)));
  }

  @Override
  public Stream<Constraint<SumOfTwoInput, int[]>> constraints() {
    return Stream.of(
        new Constraint<>(
            "Min Size of Given",
            "The solution should ensure the input must contain at least two integers",
            new SumOfTwoInput(new int[] {0}, 0),
            new IllegalArgumentException("Cannot apply solution: not enough input integers")),
        new Constraint<>(
            "Max Size of Given",
            "The solution should ensure the input must not contain more than 104 integers",
            new SumOfTwoInput(new int[105], 0),
            new IllegalArgumentException("Cannot apply solution: too many input integers")),
        new Constraint<>(
            "Min Value of Given",
            "The solution should ensure the input must not contain integers less than negative one billion",
            new SumOfTwoInput(new int[] {-BILLION - 1, BILLION}, -1),
            new IllegalArgumentException("Cannot apply solution: input value too low")),
        new Constraint<>(
            "Max Value of Given",
            "The solution should ensure the input must not contain integers greater than one billion",
            new SumOfTwoInput(new int[] {BILLION + 1, -BILLION}, 1),
            new IllegalArgumentException("Cannot apply solution: input value too high")),
        new Constraint<>(
            "Min Value of Target",
            "The solution should ensure the input target must not be less than negative one billion",
            new SumOfTwoInput(new int[] {-1, -BILLION}, -BILLION - 1),
            new IllegalArgumentException("Cannot apply solution: input value too low")),
        new Constraint<>(
            "Max Value of Target",
            "The solution should ensure the input target must not be greater than one billion",
            new SumOfTwoInput(new int[] {1, BILLION}, BILLION + 1),
            new IllegalArgumentException("Cannot apply solution: input value too high")));
  }

  @Override
  public Stream<Performance<SumOfTwoInput, int[]>> performance() {
    return Stream.of(
        new Performance<>(
            "Naive",
            "The solution should be solved in O(N^2) complexity within the specified time in nanoseconds",
            new SumOfTwoInput(maxGiven(), 2),
            NANO_3000),
        new Performance<>(
            "Optimized",
            "The solution should be solved in O(N) complexity within the specified time in nanoseconds",
            new SumOfTwoInput(maxGiven(), 2),
            NANO_1500));
  }

  private boolean comparison(int[] actual, int[] expected) {
    String a = Arrays.toString(actual);
    String e = Arrays.toString(expected);
    return a.equals(e);
  }

  private int[] maxGiven() {
    int[] given = new int[104];
    for (int i = 0; i < 104; i++) {
      int value = isFirstOrLast(i) ? 1 : 0;
      given[i] = value;
    }
    return given;
  }

  private boolean isFirstOrLast(int index) {
    return index == 0 || index == 103;
  }
}
