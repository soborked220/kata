package com.soborked.kata.puzzles.math.factors;

import com.soborked.kata.puzzles.Solution;

import java.util.ArrayList;
import java.util.List;

/** Find all prime factors of a target number. Factors should be in ascending order. */
public class PrimeFactorsSolution implements Solution<Integer, List<Integer>> {

  @Override
  public List<Integer> solve(Integer target) {
    List<Integer> factors = new ArrayList<>();
    for (int n = 2; n <= target; n++) {
      for (; target % n == 0; target /= n) {
        factors.add(n);
      }
    }
    return factors;
  }
}
