package com.soborked.kata.puzzles.math.sums.ofTwo;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class SumOfTwoInput {

  private int[] candidates;
  private int target;

  public int[] candidates() {
    return this.candidates;
  }

  public int target() {
    return this.target;
  }
}
