package com.soborked.kata.puzzles.math.reverse;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/**
 * Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the
 * value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0. Assume the
 * environment does not allow you to store 64-bit integers (signed or unsigned).
 *
 * <p>Example 1:
 *
 * <p>Input: x = 123 Output: 321
 *
 * <p>Example 2:
 *
 * <p>Input: x = -123 Output: -321
 *
 * <p>Example 3:
 *
 * <p>Input: x = 120 Output: 21
 *
 * <p>Constraints:
 *
 * <p>-2^31 <= x <= 2^31 - 1
 */
public class ReverseIntegerPuzzle implements Puzzle<Integer, Integer> {
  @Override
  public Stream<UseCase<Integer, Integer>> examples() {
    return Stream.of(
        new UseCase<>("1", "Reverse of single digit is just itself", 1, 1, this::comparison),
        new UseCase<>("12", "Reverse of two digits", 12, 21, this::comparison),
        new UseCase<>("123", "Reverse of three digits", 123, 321, this::comparison),
        new UseCase<>("1234", "Reverse of four digits", 1234, 4321, this::comparison),
        new UseCase<>("12345", "Reverse of five digits", 12345, 54321, this::comparison),
        new UseCase<>(
            "MAX",
            "Reverse of MAX exceeds bounds, so return zero",
            Integer.MAX_VALUE,
            0,
            this::comparison),
        new UseCase<>(
            "Just under MAX",
            "Reverse of just under MAX exceeds bounds, so return zero",
            Integer.MAX_VALUE - 1,
            0,
            this::comparison),
        new UseCase<>(
            "-123", "Reverse of a negative is still negative", -123, -321, this::comparison),
        new UseCase<>(
            "MIN",
            "Reverse of MIN exceeds bounds, so return zero",
            Integer.MIN_VALUE,
            0,
            this::comparison),
        new UseCase<>(
            "Just over MIN",
            "Reverse of just over MIN exceeds bounds, so return zero",
            Integer.MIN_VALUE + 1,
            0,
            this::comparison));
  }

  @Override
  public Stream<Constraint<Integer, Integer>> constraints() {
    return null;
  }

  @Override
  public Stream<Performance<Integer, Integer>> performance() {
    return null;
  }

  private boolean comparison(Integer a, Integer e) {
    return a.equals(e);
  }
}
