package com.soborked.kata.puzzles.math.factors;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class PrimeFactorsPuzzle implements Puzzle<Integer, List<Integer>> {
  @Override
  public Stream<UseCase<Integer, List<Integer>>> examples() {
    return Stream.of(
        new UseCase<>("One", "Return all prime factors", 1, asList(), this::comparison),
        new UseCase<>("Two", "Return all prime factors", 2, asList(2), this::comparison),
        new UseCase<>("Three", "Return all prime factors", 3, asList(3), this::comparison),
        new UseCase<>("Four", "Return all prime factors", 4, asList(2, 2), this::comparison),
        new UseCase<>("Five", "Return all prime factors", 5, asList(5), this::comparison),
        new UseCase<>("Six", "Return all prime factors", 6, asList(2, 3), this::comparison),
        new UseCase<>("Seven", "Return all prime factors", 7, asList(7), this::comparison),
        new UseCase<>("Eight", "Return all prime factors", 8, asList(2, 2, 2), this::comparison),
        new UseCase<>("Nine", "Return all prime factors", 9, asList(3, 3), this::comparison),
        new UseCase<>("Ten", "Return all prime factors", 10, asList(2, 5), this::comparison),
        new UseCase<>("Eleven", "Return all prime factors", 11, asList(11), this::comparison),
        new UseCase<>("Twenty", "Return all prime factors", 20, asList(2, 2, 5), this::comparison),
        new UseCase<>(
            "Twenty-two", "Return all prime factors", 22, asList(2, 11), this::comparison),
        new UseCase<>(
            "Twenty-five", "Return all prime factors", 25, asList(5, 5), this::comparison),
        new UseCase<>(
            "Twenty-seven", "Return all prime factors", 27, asList(3, 3, 3), this::comparison),
        new UseCase<>("Fifty", "Return all prime factors", 50, asList(2, 5, 5), this::comparison),
        new UseCase<>(
            "One Hundred", "Return all prime factors", 100, asList(2, 2, 5, 5), this::comparison),
        new UseCase<>(
            "One Hundred Twenty-five",
            "Return all prime factors",
            125,
            asList(5, 5, 5),
            this::comparison),
        new UseCase<>(
            "One Thousand",
            "Return all prime factors",
            1000,
            asList(2, 2, 2, 5, 5, 5),
            this::comparison));
  }

  @Override
  public Stream<Constraint<Integer, List<Integer>>> constraints() {
    return Stream.empty();
  }

  @Override
  public Stream<Performance<Integer, List<Integer>>> performance() {
    return Stream.empty();
  }

  private boolean comparison(List<Integer> a, List<Integer> e) {
    return a.equals(e);
  }
}
