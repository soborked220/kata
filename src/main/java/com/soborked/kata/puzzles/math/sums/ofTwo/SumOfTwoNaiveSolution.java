package com.soborked.kata.puzzles.math.sums.ofTwo;

import com.soborked.kata.puzzles.Solution;

public class SumOfTwoNaiveSolution implements Solution<SumOfTwoInput, int[]> {

  private static final int BILLION = 1000000000;

  @Override
  public int[] solve(SumOfTwoInput given) {
    int[] candidates = given.candidates();
    assertValidSize(candidates);
    int target = given.target();
    assertValidValue(target);
    int[] indices = new int[2];
    for (int f = 0; f < candidates.length; f++) {
      int first = candidates[f];
      assertValidValue(first);
      for (int s = f + 1; s < candidates.length; s++) {
        int second = candidates[s];
        assertValidValue(second);
        if (first + second == target) {
          indices[0] = f;
          indices[1] = s;
        }
      }
    }
    return indices;
  }

  private void assertValidSize(int[] candidates) {
    if (candidates.length < 2) {
      throw badArg("not enough input integers");
    }
    if (candidates.length > 104) {
      throw badArg("too many input integers");
    }
  }

  private void assertValidValue(int value) {
    if (value < -BILLION) {
      throw badArg("input value too low");
    }
    if (value > BILLION) {
      throw badArg("input value too high");
    }
  }

  private IllegalArgumentException badArg(String specificCause) {
    return new IllegalArgumentException("Cannot apply solution: " + specificCause);
  }
}
