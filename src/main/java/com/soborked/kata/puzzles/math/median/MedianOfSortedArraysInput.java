package com.soborked.kata.puzzles.math.median;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public class MedianOfSortedArraysInput {

  private final int[] left;
  private final int[] right;

  public String toString() {
    return Arrays.toString(left) + "," + Arrays.toString(right);
  }
}
