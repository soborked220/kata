package com.soborked.kata.puzzles.math.sums.byDigit;

import com.soborked.kata.puzzles.Solution;

public class SumByDigitLoopSolution implements Solution<SumByDigitInput, ListNode> {
  @Override
  public ListNode solve(SumByDigitInput given) {
    ListNode dummyHead = new ListNode(-1);
    ListNode current = dummyHead;

    ListNode chainA = given.getA();
    ListNode chainB = given.getB();
    int carriedOne = 0;
    while (chainA != null || chainB != null || carriedOne != 0) {
      int digitA = chainA == null ? 0 : chainA.val;
      int digitB = chainB == null ? 0 : chainB.val;

      int sum = digitA + digitB + carriedOne;
      int rightMostDigit = sum % 10;
      carriedOne = sum / 10;
      current.next = new ListNode(rightMostDigit);
      current = current.next;

      chainA = chainA == null ? null : chainA.next;
      chainB = chainB == null ? null : chainB.next;
    }

    return dummyHead.next;
  }
}
