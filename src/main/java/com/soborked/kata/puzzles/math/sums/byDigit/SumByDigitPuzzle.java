package com.soborked.kata.puzzles.math.sums.byDigit;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.*;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

/*
You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.



Example 1:

Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

Example 2:

Input: l1 = [0], l2 = [0]
Output: [0]

Example 3:

Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]



Constraints:

    The number of nodes in each linked list is in the range [1, 100].
    0 <= Node.val <= 9
    It is guaranteed that the list represents a number that does not have leading zeros.


 */
public class SumByDigitPuzzle implements Puzzle<SumByDigitInput, ListNode> {
  @Override
  public Stream<UseCase<SumByDigitInput, ListNode>> examples() {
    return Stream.of(
        new UseCase<>(
            "Zeroes",
            "Zero is a valid sum result",
            new SumByDigitInput(asList(0), asList(0)),
            new ListNode(new LinkedList<>(asList(0))),
            this::comparison),
        new UseCase<>(
            "Same Digit",
            "Input numbers can be the same",
            new SumByDigitInput(asList(1), asList(1)),
            new ListNode(new LinkedList<>(asList(2))),
            this::comparison),
        new UseCase<>(
            "Under Ten",
            "Sum should not carry the one if under ten",
            new SumByDigitInput(asList(1), asList(8)),
            new ListNode(new LinkedList<>(asList(9))),
            this::comparison),
        new UseCase<>(
            "Ten",
            "Sum should carry the one if ten",
            new SumByDigitInput(asList(1), asList(9)),
            new ListNode(new LinkedList<>(asList(0, 1))),
            this::comparison),
        new UseCase<>(
            "Twenty",
            "Sum should carry the one even past the first ten",
            new SumByDigitInput(asList(1, 1), asList(9)),
            new ListNode(new LinkedList<>(asList(0, 2))),
            this::comparison),
        new UseCase<>(
            "No Order",
            "The order of the two inputs doesn't change the result",
            new SumByDigitInput(asList(9), asList(1, 1)),
            new ListNode(new LinkedList<>(asList(0, 2))),
            this::comparison),
        new UseCase<>(
            "Every Ten",
            "Sum should carry the one every ten",
            new SumByDigitInput(asList(2, 2), asList(8, 7)),
            new ListNode(new LinkedList<>(asList(0, 0, 1))),
            this::comparison),
        new UseCase<>(
            "Max is 101 digits",
            "Two inputs at one hundred digits will result in a 101 digit sum",
            new SumByDigitInput(hundredDigitInput(), hundredDigitInput()),
            new ListNode(new LinkedList<>(hundredOneDigitOutput())),
            this::comparison));
  }

  /*
     [9, 9, ..., 9, 5]
     [9, 9, ..., 9, 5]
  */
  private List<Integer> hundredDigitInput() {
    List<Integer> values = new ArrayList<>();
    for (int i = 0; i < 100; i++) {
      if (i == 99) {
        values.add(5);
      } else {
        values.add(9);
      }
    }
    return values;
  }

  /*
     [8, 9, ..., 9, 1, 1]
  */
  private List<Integer> hundredOneDigitOutput() {
    List<Integer> values = new ArrayList<>();
    for (int i = 0; i < 101; i++) {
      if (i >= 99) {
        values.add(1);
      } else if (i == 0) {
        values.add(8);
      } else {
        values.add(9);
      }
    }
    return values;
  }

  @Override
  public Stream<Constraint<SumByDigitInput, ListNode>> constraints() {
    return Stream.empty();
  }

  @Override
  public Stream<Performance<SumByDigitInput, ListNode>> performance() {
    return Stream.empty();
  }

  private boolean comparison(ListNode actual, ListNode expected) {
    List<Integer> a = actual.unpack();
    List<Integer> e = expected.unpack();
    return a.equals(e);
  }
}
