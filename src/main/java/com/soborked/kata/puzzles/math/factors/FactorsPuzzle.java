package com.soborked.kata.puzzles.math.factors;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

/**
 * Find all factors of a target number. Factors should be in ascending order. Factors should be
 * distinct: no duplicates in the returned list.
 */
public class FactorsPuzzle implements Puzzle<Integer, List<Integer>> {

  @Override
  public Stream<UseCase<Integer, List<Integer>>> examples() {
    return Stream.of(
        new UseCase<>("One", "Return all factors", 1, asList(1), this::comparison),
        new UseCase<>("Two", "Return all factors", 2, asList(1, 2), this::comparison),
        new UseCase<>("Three", "Return all factors", 3, asList(1, 3), this::comparison),
        new UseCase<>("Four", "Return all factors", 4, asList(1, 2, 4), this::comparison),
        new UseCase<>("Five", "Return all factors", 5, asList(1, 5), this::comparison),
        new UseCase<>("Six", "Return all factors", 6, asList(1, 2, 3, 6), this::comparison),
        new UseCase<>("Seven", "Return all factors", 7, asList(1, 7), this::comparison),
        new UseCase<>("Eight", "Return all factors", 8, asList(1, 2, 4, 8), this::comparison),
        new UseCase<>("Nine", "Return all factors", 9, asList(1, 3, 9), this::comparison),
        new UseCase<>("Ten", "Return all factors", 10, asList(1, 2, 5, 10), this::comparison),
        new UseCase<>(
            "Twenty", "Return all factors", 20, asList(1, 2, 4, 5, 10, 20), this::comparison),
        new UseCase<>("Twenty-five", "Return all factors", 25, asList(1, 5, 25), this::comparison),
        new UseCase<>(
            "Twenty-seven", "Return all factors", 27, asList(1, 3, 9, 27), this::comparison),
        new UseCase<>(
            "Fifty", "Return all factors", 50, asList(1, 2, 5, 10, 25, 50), this::comparison),
        new UseCase<>(
            "One Hundred",
            "Return all factors",
            100,
            asList(1, 2, 4, 5, 10, 20, 25, 50, 100),
            this::comparison),
        new UseCase<>(
            "One Hundred Twenty-five",
            "Return all factors",
            125,
            asList(1, 5, 25, 125),
            this::comparison),
        new UseCase<>(
            "One Thousand",
            "Return all factors",
            1000,
            asList(1, 2, 4, 5, 8, 10, 20, 25, 40, 50, 100, 125, 200, 250, 500, 1000),
            this::comparison));
  }

  @Override
  public Stream<Constraint<Integer, List<Integer>>> constraints() {
    return Stream.empty();
  }

  @Override
  public Stream<Performance<Integer, List<Integer>>> performance() {
    return Stream.empty();
  }

  private boolean comparison(List<Integer> a, List<Integer> e) {
    return a.equals(e);
  }
}
