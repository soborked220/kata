package com.soborked.kata.puzzles.math.reverse;

import com.soborked.kata.puzzles.Solution;

import static java.lang.Integer.MAX_VALUE;

public class ReverseIntegerNaiveSolution implements Solution<Integer, Integer> {
  @Override
  public Integer solve(Integer given) {
    boolean negative = false;
    if (given < 0) {
      given = -1 * given;
      negative = true;
    }

    int result = 0;
    while (given != 0) {
      int digit = given % 10;
      if (result > prev(MAX_VALUE, digit)) {
        return 0;
      }
      result = next(result, digit);
      given /= 10;
    }
    return negative ? -result : result;
  }

  private int next(int cur, int digit) {
    return (cur * 10) + digit;
  }

  private int prev(int cur, int digit) {
    return (cur - digit) / 10;
  }
}
