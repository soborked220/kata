package com.soborked.kata.puzzles.math.sums.ofTwo;

import com.soborked.kata.puzzles.Solution;

import java.util.HashMap;
import java.util.Map;

public class SumOfTwoOptimizedSolution implements Solution<SumOfTwoInput, int[]> {

  private static final int BILLION = 1000000000;

  @Override
  public int[] solve(SumOfTwoInput given) {
    int[] candidates = given.candidates();
    assertValidSize(candidates);
    int target = given.target();
    assertValidValue(target);
    Map<Integer, Integer> visited = new HashMap<>(); // Value, Index
    for (int i = 0; i < candidates.length; i++) {
      int candidate = candidates[i];
      assertValidValue(candidate);
      int remaining = target - candidate;
      if (visited.containsKey(remaining)) {
        return new int[] {visited.get(remaining), i};
      }
      visited.put(candidate, i);
    }
    return new int[] {};
  }

  private void assertValidSize(int[] candidates) {
    if (candidates.length < 2) {
      throw badArg("not enough input integers");
    }
    if (candidates.length > 104) {
      throw badArg("too many input integers");
    }
  }

  private void assertValidValue(int value) {
    if (value < -BILLION) {
      throw badArg("input value too low");
    }
    if (value > BILLION) {
      throw badArg("input value too high");
    }
  }

  private IllegalArgumentException badArg(String specificCause) {
    return new IllegalArgumentException("Cannot apply solution: " + specificCause);
  }
}
