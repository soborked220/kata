package com.soborked.kata.puzzles.math.factors;

import com.soborked.kata.puzzles.Solution;

import java.util.ArrayList;
import java.util.List;

public class DistinctPrimeFactorsSolution implements Solution<Integer, List<Integer>> {
  @Override
  public List<Integer> solve(Integer target) {
    List<Integer> factors = new ArrayList<>();
    for (int n = 2; n <= target; n++) {
      for (int occurrence = 0; target % n == 0; target /= n, occurrence++) {
        if (occurrence == 0) {
          factors.add(n);
        }
      }
    }
    return factors;
  }
}
