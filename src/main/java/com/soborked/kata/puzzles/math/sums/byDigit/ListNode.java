package com.soborked.kata.puzzles.math.sums.byDigit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Used in the SumByDigitPuzzle */
public class ListNode {
  int val;
  ListNode next;

  ListNode() {}

  ListNode(int val) {
    this.val = val;
  }

  ListNode(int val, ListNode next) {
    this.val = val;
    this.next = next;
  }

  /**
   * Helper for test cases. Do not use while solving!
   *
   * @param input
   */
  public ListNode(List<Integer> input) {
    if (!input.isEmpty()) {
      this.val = input.get(0);
      input.remove(0);
      if (!input.isEmpty()) {
        this.next = new ListNode(input);
      }
    }
  }

  /**
   * Helper for test cases. Do not use while solving!
   *
   * @return
   */
  public List<Integer> unpack() {
    ListNode chain = this;
    List<Integer> list = new ArrayList<>();
    list.add(chain.val);
    while (chain.next != null) {
      chain = chain.next;
      list.add(chain.val);
    }
    Collections.reverse(list);
    return list;
  }

  public String toString() {
    return unpack().toString();
  }
}
