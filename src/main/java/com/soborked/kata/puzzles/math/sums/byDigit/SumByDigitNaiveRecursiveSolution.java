package com.soborked.kata.puzzles.math.sums.byDigit;

import com.soborked.kata.puzzles.Solution;

/** This one is very natural to arrive at if using a TDD approach */
public class SumByDigitNaiveRecursiveSolution implements Solution<SumByDigitInput, ListNode> {

  @Override
  public ListNode solve(SumByDigitInput given) {
    ListNode a = given.getA();
    ListNode b = given.getB();
    return next(a, b, false);
  }

  private ListNode next(ListNode a, ListNode b, boolean carryTheOne) {
    int sum = a.val + b.val;
    if (carryTheOne) {
      sum += 1;
    }
    if (sum > 9) {
      int remainder = sum % 10;
      ListNode na = a.next;
      ListNode nb = b.next;
      if (na != null && nb != null) {
        return new ListNode(remainder, next(na, nb, true));
      } else if (na != null) {
        return new ListNode(remainder, next(na, true));
      } else if (nb != null) {
        return new ListNode(remainder, next(nb, true));
      } else {
        return new ListNode(remainder, new ListNode(1));
      }
    } else {
      ListNode na = a.next;
      ListNode nb = b.next;
      if (na != null && nb != null) {
        return new ListNode(sum, next(na, nb, false));
      } else if (na != null) {
        return new ListNode(sum, next(na, false));
      } else if (nb != null) {
        return new ListNode(sum, next(nb, false));
      } else {
        return new ListNode(sum);
      }
    }
  }

  private ListNode next(ListNode node, boolean carryTheOne) {
    int sum = node.val;
    if (carryTheOne) {
      sum += 1;
    }
    if (sum > 9) {
      int remainder = sum % 10;
      ListNode nNode = node.next;
      if (nNode != null) {
        return new ListNode(remainder, next(nNode, true));
      } else {
        return new ListNode(remainder, new ListNode(1));
      }
    } else {
      ListNode nNode = node.next;
      if (nNode != null) {
        return new ListNode(sum, next(nNode, false));
      } else {
        return new ListNode(sum);
      }
    }
  }
}
