package com.soborked.kata.puzzles.math.sums.byDigit;

import com.soborked.kata.puzzles.Solution;

public class SumByDigitTidiedRecursiveSolution implements Solution<SumByDigitInput, ListNode> {
  @Override
  public ListNode solve(SumByDigitInput given) {
    ListNode chainA = given.getA();
    ListNode chainB = given.getB();
    return addTwoNumbers(chainA, chainB);
  }

  private ListNode addTwoNumbers(ListNode chainA, ListNode chainB) {
    if (chainA == null && chainB == null) {
      return null;
    }
    int digitA = chainA == null ? 0 : chainA.val;
    int digitB = chainB == null ? 0 : chainB.val;
    int sum = digitA + digitB;

    int rightMostDigit = sum % 10;
    ListNode current = new ListNode(rightMostDigit);

    chainA = chainA == null ? null : chainA.next;
    chainB = chainB == null ? null : chainB.next;
    current.next = addTwoNumbers(chainA, chainB);
    if (sum >= 10) {
      current.next = addTwoNumbers(current.next, new ListNode(1));
    }
    return current;
  }
}
