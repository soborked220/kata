package com.soborked.kata.puzzles.math.factors;

import com.soborked.kata.puzzles.Solution;

import java.util.ArrayList;
import java.util.List;

public class FactorsNaiveSolution implements Solution<Integer, List<Integer>> {

  @Override
  public List<Integer> solve(Integer target) {
    List<Integer> factors = new ArrayList<>();
    if (target == 1) {
      factors.add(1);
      return factors;
    }

    int increment = getIncrement(target);
    for (int n = 1; n <= target; n += increment) {
      if (isFactor(target, n)) {
        factors.add(n);
      }
    }
    return factors;
  }

  private boolean isFactor(int target, int current) {
    return target % current == 0;
  }

  private int getIncrement(int target) {
    boolean isEven = target % 2 == 0;
    return isEven ? 1 : 2;
  }
}
