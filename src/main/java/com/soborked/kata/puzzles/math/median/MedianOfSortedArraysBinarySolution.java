package com.soborked.kata.puzzles.math.median;

import com.soborked.kata.puzzles.Solution;

public class MedianOfSortedArraysBinarySolution
    implements Solution<MedianOfSortedArraysInput, Double> {

  private static final int OFFSET_INT_ROUNDING = 1;
  private static final int FIRST = 1;

  @Override
  public Double solve(MedianOfSortedArraysInput given) {
    int[] queueA = given.getLeft();
    int[] queueB = given.getRight();
    int aSize = queueA.length;
    int bSize = queueB.length;
    int totalSize = aSize + bSize;
    int middleGuess = isEven(totalSize) ? (totalSize / 2) : (offsetToRoundUp(totalSize) / 2);
    int adjacentGuess = isEven(totalSize) ? middleGuess + 1 : middleGuess;
    double middle = guessAndCheck(queueA, 0, queueB, 0, middleGuess);
    double adjacent = guessAndCheck(queueA, 0, queueB, 0, adjacentGuess);
    return (middle + adjacent) / 2.0;
  }

  public double guessAndCheck(int[] queueA, int indexA, int[] queueB, int indexB, int guess) {
    if (isExhausted(queueA, indexA)) {
      return queueB[indexB + asIndex(guess)];
    }
    if (isExhausted(queueB, indexB)) {
      return queueA[indexA + asIndex(guess)];
    }
    if (guess == FIRST) {
      return Math.min(queueA[indexA], queueB[indexB]);
    }
    int aMid = Integer.MAX_VALUE;
    int bMid = Integer.MAX_VALUE;
    int nextGuess = guess / 2;
    int nextIndex = asIndex(nextGuess);
    if (indexA + nextIndex < queueA.length) {
      aMid = queueA[indexA + nextIndex];
    }
    if (indexB + nextIndex < queueB.length) {
      bMid = queueB[indexB + nextIndex];
    }

    if (aMid < bMid) {
      return checkRightOfALeftOfB(queueA, indexA, queueB, indexB, guess, nextGuess);
    } else {
      return checkLeftOfARightOfB(queueA, indexA, queueB, indexB, guess, nextGuess);
    }
  }

  private double checkRightOfALeftOfB(
      int[] queueA, int indexA, int[] queueB, int indexB, int guess, int nextGuess) {
    return guessAndCheck(queueA, indexA + nextGuess, queueB, indexB, guess - nextGuess);
  }

  private double checkLeftOfARightOfB(
      int[] queueA, int indexA, int[] queueB, int indexB, int guess, int nextGuess) {
    return guessAndCheck(queueA, indexA, queueB, indexB + nextGuess, guess - nextGuess);
  }

  private boolean isEven(int size) {
    return size % 2 == 0;
  }

  private int offsetToRoundUp(int value) {
    return value + OFFSET_INT_ROUNDING;
  }

  private boolean isExhausted(int[] queue, int index) {
    return index > lastIndex(queue);
  }

  private int lastIndex(int[] queue) {
    return asIndex(queue.length);
  }

  private int asIndex(int value) {
    return value - 1;
  }
}
