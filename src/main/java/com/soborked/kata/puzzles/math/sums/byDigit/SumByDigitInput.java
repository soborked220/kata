package com.soborked.kata.puzzles.math.sums.byDigit;

import lombok.Getter;

import java.util.LinkedList;
import java.util.List;

@Getter
public class SumByDigitInput {

  private ListNode a;
  private ListNode b;

  /**
   * The two lists must be the digits of the input integer in reverse.
   *
   * <p>Examples: 127 => [7, 2, 1] 1 => [1] 10 => [0, 1]
   *
   * @param a list of digits of a number to be summed
   * @param b list of digits of a number to be summed
   */
  public SumByDigitInput(List<Integer> a, List<Integer> b) {
    this.a = new ListNode(new LinkedList<>(a));
    this.b = new ListNode(new LinkedList<>(b));
  }

  public String toString() {
    return a.unpack().toString() + "," + b.unpack().toString();
  }

  private ListNode chain(List<Integer> list) {
    ListNode chain = new ListNode();
    for (int i = 0; i < list.size() - 1; i++) {
      if (i == 0) {
        chain = new ListNode(list.get(i));
      }
      chain.next = new ListNode(list.get(i + 1));
      chain = chain.next;
    }
    return chain;
  }
}
