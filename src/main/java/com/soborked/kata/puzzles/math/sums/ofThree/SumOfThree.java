package com.soborked.kata.puzzles.math.sums.ofThree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SumOfThree {

  public int[][] triplets(int[] given) {
    List<int[]> answers = new ArrayList<>();
    Arrays.sort(given);
    int end = given.length - 1;
    for (int i = 0; i < given.length; i++) {
      if (i > 0 && given[i] == given[i - 1]) {
        continue;
      }
      int l = i + 1;
      int r = end;
      while (l < r) {
        int sum = given[i] + given[l] + given[r];
        if (sum < 0) {
          l++;
        } else if (sum > 0) {
          r--;
        } else { // sum == 0
          answers.add(new int[] {given[i], given[l], given[r]});
          l++;
          while (l < r && given[l] == given[l - 1]) {
            l++;
          }
        }
      }
    }
    return answers.toArray(int[][]::new);
  }
}

