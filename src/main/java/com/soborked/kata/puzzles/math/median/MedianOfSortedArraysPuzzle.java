package com.soborked.kata.puzzles.math.median;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

public class MedianOfSortedArraysPuzzle implements Puzzle<MedianOfSortedArraysInput, Double> {

  @Override
  public Stream<UseCase<MedianOfSortedArraysInput, Double>> examples() {
    return Stream.of(
        new UseCase<>(
            "Zeroes",
            "Median of zero is zero",
            new MedianOfSortedArraysInput(new int[] {0}, new int[] {0}),
            0.0,
            this::comparison),
        new UseCase<>(
            "Simple",
            "Median of two single node arrays",
            new MedianOfSortedArraysInput(new int[] {1}, new int[] {1}),
            1.0,
            this::comparison),
        new UseCase<>(
            "Lopsided Left",
            "Median can be determined for arrays of two different sizes",
            new MedianOfSortedArraysInput(new int[] {1, 2}, new int[] {1}),
            1.0,
            this::comparison),
        new UseCase<>(
            "Lopsided Right",
            "Median can be determined for arrays of two different sizes",
            new MedianOfSortedArraysInput(new int[] {1}, new int[] {1, 2}),
            1.0,
            this::comparison),
        new UseCase<>(
            "Even sized",
            "Median can be determined for arrays whose total size is even",
            new MedianOfSortedArraysInput(new int[] {1, 2}, new int[] {3, 4}),
            2.5,
            this::comparison),
        new UseCase<>(
            "Non-consecutive",
            "Median can be determined for arrays whose values are non-consecutive",
            new MedianOfSortedArraysInput(new int[] {1, 3}, new int[] {2, 7}),
            2.5,
            this::comparison),
        new UseCase<>(
            "Large",
            "Median can be determined for large array",
            new MedianOfSortedArraysInput(new int[] {1, 3, 5, 7, 9}, new int[] {2, 4, 6, 8, 10}),
            5.5,
            this::comparison));
  }

  private boolean comparison(Double a, Double e) {
    return a.equals(e);
  }

  @Override
  public Stream<Constraint<MedianOfSortedArraysInput, Double>> constraints() {
    return Stream.empty();
  }

  @Override
  public Stream<Performance<MedianOfSortedArraysInput, Double>> performance() {
    return Stream.empty();
  }
}
