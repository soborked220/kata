package com.soborked.kata.puzzles.math.factors;

import com.soborked.kata.puzzles.Solution;

import java.util.ArrayList;
import java.util.List;

public class FactorsSolution implements Solution<Integer, List<Integer>> {

  @Override
  public List<Integer> solve(Integer target) {
    List<Integer> bottom = new ArrayList<>();
    List<Integer> top = new ArrayList<>();
    if (target == 1) {
      bottom.add(1);
      return bottom;
    }

    int limit = (int) Math.sqrt(target);
    int increment = getIncrement(target);
    for (int n = 1; n <= limit; n += increment) {
      if (isFactor(target, n)) {
        bottom.add(n);
        int converse = converse(target, n);
        if (n != converse) {
          top.add(converse(target, n));
        }
      }
    }
    for (int i = top.size() - 1; i >= 0; i--) {
      bottom.add(top.get(i));
    }
    return bottom;
  }

  private int converse(int target, int n) {
    return target / n;
  }

  private boolean isFactor(int target, int current) {
    return target % current == 0;
  }

  private int getIncrement(int target) {
    boolean isEven = target % 2 == 0;
    return isEven ? 1 : 2;
  }
}
