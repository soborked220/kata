package com.soborked.kata.puzzles.math.sums.ofTwo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ZeroSumOfTwo {

    public int[][] zerosum(int[] given) {
        List<int[]> answers = new ArrayList<>();
        Arrays.sort(given);
        int l=0;
        int r=given.length-1;
        while(l<r) {
            int sum = given[l] + given[r];
            if(sum < 0) {
                l = next(given, l);
            }else if (sum >0) {
                r--;
            } else {
                answers.add(new int[]{given[l], given[r]});
                l = next(given, l);
            }
        }
        return answers.toArray(int[][]::new);
    }

    private int next(int[] given, int current) {
        current++;
        while(current > 0 && given[current] == given[current-1]) {
            current++;
        }
        return current;
    }
}
