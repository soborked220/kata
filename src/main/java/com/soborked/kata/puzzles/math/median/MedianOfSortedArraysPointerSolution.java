package com.soborked.kata.puzzles.math.median;

import com.soborked.kata.puzzles.Solution;

public class MedianOfSortedArraysPointerSolution
    implements Solution<MedianOfSortedArraysInput, Double> {

  private int l = 0;
  private int r = 0;
  private int lSize = 0;
  private int rSize = 0;
  private int totalSize = 0;
  private boolean isMergedQueueEven = false;
  private int targetIndex = 0;

  @Override
  public Double solve(MedianOfSortedArraysInput given) {
    int[] leftQueue = given.getLeft();
    int[] rightQueue = given.getRight();
    initialize(leftQueue, rightQueue);

    int currentVal = 0;
    for (int processedIndex = -1; processedIndex < targetIndex; processedIndex++) {
      currentVal = next(leftQueue, rightQueue);
    }
    if (!isMergedQueueEven) {
      return (double) currentVal;
    } else {
      double sum = currentVal + next(leftQueue, rightQueue);
      return sum / 2;
    }
  }

  private void initialize(int[] leftQueue, int[] rightQueue) {
    lSize = leftQueue.length;
    rSize = rightQueue.length;
    l = 0;
    r = 0;
    totalSize = lSize + rSize;
    isMergedQueueEven = totalSize % 2 == 0;
    targetIndex = target(totalSize, isMergedQueueEven);
  }

  private boolean isQueueExhausted(int ls, int l) {
    return l == ls;
  }

  private int target(int totalSize, boolean isEven) {
    if (isEven) {
      return (totalSize / 2) - 1;
    } else {
      return totalSize / 2;
    }
  }

  private int next(int[] leftQueue, int[] rightQueue) {
    if (isQueueExhausted(lSize, l)) {
      return rightQueue[r++];
    } else if (isQueueExhausted(rSize, r)) {
      return leftQueue[l++];
    } else {
      int leftVal = leftQueue[l];
      int rightVal = rightQueue[r];
      if (leftVal <= rightVal) {
        l++;
        return leftVal;
      } else {
        r++;
        return rightVal;
      }
    }
  }
}
