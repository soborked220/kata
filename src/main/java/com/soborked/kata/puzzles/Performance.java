package com.soborked.kata.puzzles;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Performance<I, O> implements Verifiable<I, Long>, Requirement {

  private static final int MILLION = 1000000;
  private final String name;
  private final String behavior;
  private final I input;
  private final Long output;
  private final Criteria<Long> criteria = (a, e) -> lessThan(a, e);
  private long actual;
  private final PrettyPrinter<I, Long> printer = new PrettyPrinter<>();

  @Override
  public String name() {
    return name;
  }

  @Override
  public String behavior() {
    return behavior;
  }

  @Override
  public I given() {
    return input;
  }

  @Override
  public Long expected() {
    return output;
  }

  @Override
  public Criteria<Long> criteria() {
    return criteria;
  }

  @Override
  public boolean isSatisfiedBy(Solution solution) {
    actual = averageAfterRuns(solution, MILLION);
    return criteria().isMet(actual, expected());
  }

  private long averageAfterRuns(Solution solution, int times) {
    long totalTime = 0;
    for (int run = 0; run < times; run++) {
      totalTime += run(solution);
    }
    return totalTime / times;
  }

  private long run(Solution solution) {
    long start = System.nanoTime();
    solution.solve(given());
    long end = System.nanoTime();
    return end - start;
  }

  @Override
  public String toString() {
    return printer.print(name(), behavior(), given(), expected(), actual);
  }

  private boolean lessThan(Long actual, Long expected) {
    return actual <= expected;
  }
}
