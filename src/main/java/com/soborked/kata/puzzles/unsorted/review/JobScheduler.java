package com.soborked.kata.puzzles.unsorted.review;

public interface JobScheduler {

    int schedule(int[] startTimes, int[] endTimes, int[] profits);
}
