package com.soborked.kata.puzzles.unsorted.review;

import java.util.List;

public interface CombinationFinder {

    List<List<Integer>> combinations(int[] given, int target);

}
