package com.soborked.kata.puzzles.unsorted;

public class Trie {

  private static final int ALPHABET = 26;

  private TrieNode root = new TrieNode();

  private static final class TrieNode {
    boolean isEndOfWord;
    TrieNode[] children = new TrieNode[ALPHABET];

    boolean isEmpty() {
      for(int i=0; i<ALPHABET;i++) {
        if(children[i] != null) {
          return false;
        }
      }
      return true;
    }
  }

  public void add(String input) {
    int depth = input.length();
    TrieNode current = root;
    int letter;
    for (int l = 0; l < depth; l++) {
      letter = input.charAt(l) - 'a';
      if(current.children[letter] == null) {
        current.children[letter] = new TrieNode();
      }
      current = current.children[letter];
    }
    current.isEndOfWord = true;
  }

  public boolean contains(String input) {
    int depth = input.length();
    TrieNode current = root;
    int letter;
    for(int l=0; l<depth;l++) {
      letter = input.charAt(l) - 'a';
      if(current.children[letter]==null) {
        return false;
      }
      current = current.children[letter];
    }
    return current.isEndOfWord;
  }

  public boolean startsWith(String input) {
    int depth = input.length();
    TrieNode current = root;
    for(int l=0;l<depth;l++) {
      int letter = input.charAt(l) - 'a';
      if(current.children[letter] == null) {
        return false;
      }
      current = current.children[letter];
    }
    return true;
  }

  public boolean isEmpty() {
    return root.isEmpty();
  }

  public void remove(String input) {
    if(!root.isEmpty()) {
      remove(root, 0, input);
    }
  }

  private TrieNode remove(TrieNode current, int depth, String input) {
    if(depth == input.length()) {
      current.isEndOfWord = false;
      if(current.isEmpty()) {
        current = null;
      }
      return current;
    }

    int letter = input.charAt(depth) - 'a';
    current.children[letter] = remove(current.children[letter], depth+1, input);
    if(current.isEmpty() && !current.isEndOfWord) {
      current = null;
    }
    return current;
  }

}