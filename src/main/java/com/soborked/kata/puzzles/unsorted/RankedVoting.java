package com.soborked.kata.puzzles.unsorted;

import java.util.Arrays;

/*
       Rank
   Team  0  1  2
      A
      B
      C


*/
public class RankedVoting {

  public static final int TEAM_NAME = 1;
  public static final int MAX_NUMBER_OF_TEAMS = 26;
  public static final int MOVE_DOWN = 1;
  public static final int MOVE_UP = -1;

  public String rankTeams(String[] votes) {
    int teams = votes[0].length();
    int[][] countByRankForEachTeam = new int[MAX_NUMBER_OF_TEAMS][teams + TEAM_NAME];
    initializeTeamNames(teams, countByRankForEachTeam);
    countVotes(votes, teams, countByRankForEachTeam);
    Arrays.sort(countByRankForEachTeam, (a, b) -> compareResults(teams, a, b));
    return reportResults(teams, countByRankForEachTeam);
  }

  private String reportResults(int teams, int[][] countByRankForEachTeam) {
    StringBuilder results = new StringBuilder();
    for (int rank = 0; rank < teams; rank++) {
      results.append(intAsChar(countByRankForEachTeam[rank][teams]));
    }
    return results.toString();
  }

  private int compareResults(int teams, int[] teamA, int[] teamB) {
    for (int rank = 0; rank < teams; rank++) {
      if (teamA[rank] < teamB[rank]) return MOVE_DOWN;
      if (teamA[rank] > teamB[rank]) return MOVE_UP;
    }
    return 0;
  }

  private void countVotes(String[] votes, int teams, int[][] countByRankForEachTeam) {
    for (int vote = 0; vote < votes.length; vote++) {
      String ballot = votes[vote];
      for (int rank = 0; rank < teams; rank++) {
        countByRankForEachTeam[charAsInt(ballot, rank)][rank]++;
      }
    }
  }

  private int charAsInt(String ballot, int rank) {
    // If A has an int value of 30 e.g., then A will be a distance of 0 from that.
    // Similarly, B will a distance of 1 from that.
    // This way we don't need to map or know the int values of each character
    return ballot.charAt(rank) - 'A';
  }

  private char intAsChar(int value) {
    return (char) ('A' + value);
  }

  private void initializeTeamNames(int teams, int[][] countByRankForEachTeam) {
    // No need to zero index teams here since we added an extra "rank" to slot the team name
    for (int team = 0; team < MAX_NUMBER_OF_TEAMS; team++) {
      countByRankForEachTeam[team][teams] = team;
    }
  }
}
