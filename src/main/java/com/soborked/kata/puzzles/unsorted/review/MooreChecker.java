package com.soborked.kata.puzzles.unsorted.review;

public class MooreChecker implements MajorityChecker {
    @Override
    public boolean isMajority(int[] given, int target) {
        int candidate = -1;
        int count = 0;
        for (int i : given) {
            if (count == 0) {
                candidate = i;
            }
            count += (candidate == i) ? 1 : -1;
        }
        return candidate == target && count > 0;
    }

}