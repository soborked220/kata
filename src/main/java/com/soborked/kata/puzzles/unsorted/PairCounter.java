package com.soborked.kata.puzzles.unsorted;

public interface PairCounter {

    int countPairs(int[] numbers);
}
