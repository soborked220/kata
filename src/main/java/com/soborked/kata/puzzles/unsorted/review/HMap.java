package com.soborked.kata.puzzles.unsorted.review;

public class HMap<K, V> {

    private static class Entry<K,V> {
        K key;
        V val;
        Entry<K,V> next;
        Entry(K key, V val) {
            this.key = key;
            this.val = val;
        }
    }

    private int capacity;
    private int size = 0;
    private final double scaleFactor = .75;

    private Entry<K,V>[] entries;

    public HMap() {
        this.capacity = 16;
        entries = new Entry[capacity];
    }

    public HMap(int capacity) {
        this.capacity = capacity;
        entries = new Entry[capacity];
    }

    public void put(K key, V value) {
        int index = hash(key);
        Entry<K,V> existing = entries[index];
        while(existing != null) {
            if(existing.key == key) {
                existing.val = value;
                return;
            }
            existing = existing.next;
        }
        Entry<K,V> newEntry = new Entry<>(key, value);
        newEntry.next = entries[index];
        entries[index] = newEntry;
        size++;
        if(size > scaleFactor * capacity) {
            resize();
        }
    }

    private void resize() {
        Entry<K,V>[] bigger = new Entry[2 * capacity];
        for(int i = 0; i < capacity; i++) {
            Entry<K,V> existing = entries[i];
            while(existing != null) {
                int index = hash(existing.key);
                Entry<K,V> next = existing.next;
                existing.next = bigger[index];
                bigger[index] = existing;
                existing = next;
            }
        }
        this.capacity = 2 * capacity;
        this.entries = bigger;
    }

    private int hash(K key) {
        return key.hashCode() % capacity;
    }

    public V get(K key) {
        int index = hash(key);
        Entry<K,V> existing = entries[index];
        while(existing != null) {
            if(existing.key.equals(key)) {
                return existing.val;
            }
            existing = existing.next;
        }
        return null;
    }

    public void remove(K key) {
        int index = hash(key);
        Entry<K,V> existing = entries[index];
        Entry<K,V> prev = null;
        while(existing != null) {
            if(existing.key.equals(key)) {
                if(prev == null) {
                    entries[index] = existing.next;
                } else {
                    prev.next = existing.next;
                }
                size--;
                return;
            }
            prev = existing;
            existing = existing.next;
        }
    }
}
