package com.soborked.kata.puzzles.unsorted;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BfsBinaryTreeRightSideView implements RightSideView {
  @Override
  public List<Integer> visible(TreeNode given) {
    List<Integer> visible = new ArrayList<>();
    if (given == null || (given.right == null && given.left == null)) {
      return visible;
    }
    Queue<TreeNode> queue = new LinkedList<>();
    queue.add(given);
    while (!queue.isEmpty()) {

      TreeNode furthestRightSoFar = null;
      int nodesOnLevel = queue.size();

      for (int node = 0; node < nodesOnLevel; node++) {
        TreeNode current = queue.poll();
        if (current != null) {
          furthestRightSoFar = current;
          queue.add(current.left);
          queue.add(current.right);
        }
      }
      if (furthestRightSoFar != null) {
        visible.add(furthestRightSoFar.val);
      }
    }
    return visible;
  }
}
