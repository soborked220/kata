package com.soborked.kata.puzzles.unsorted;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static java.util.Collections.reverseOrder;
import static java.util.stream.Collectors.toList;

public class EfficientWordFrequency implements WordCounter {
    @Override
    public List<String> countFrequency(String[] words, int k) {
        Map<String, Integer> counts = new TreeMap<>();
        for(String word: words) {
            counts.merge(word, 1, Integer::sum);
        }
        return counts.entrySet().stream()
                .sorted(reverseOrder(Map.Entry.comparingByValue()))
                .limit(k)
                .map(Map.Entry::getKey)
                .collect(toList());
    }
}
