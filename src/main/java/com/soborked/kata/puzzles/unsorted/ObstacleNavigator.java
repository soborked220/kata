package com.soborked.kata.puzzles.unsorted;

public interface ObstacleNavigator {

    int explore(int[][] grid);
}
