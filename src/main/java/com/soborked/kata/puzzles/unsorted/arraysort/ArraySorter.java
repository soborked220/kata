package com.soborked.kata.puzzles.unsorted.arraysort;

public interface ArraySorter {

  int[] sort(int[] given);

  default String name() {
    return this.getClass().getSimpleName();
  }
}
