package com.soborked.kata.puzzles.unsorted.review;

public interface BinaryTreeValidator {

    boolean isValid(BinaryTreeTraversalValidator.Node node);
}
