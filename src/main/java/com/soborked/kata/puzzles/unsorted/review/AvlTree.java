package com.soborked.kata.puzzles.unsorted.review;

import java.util.LinkedList;
import java.util.Queue;

public class AvlTree {

    public static class AvlNode {
        int key;
        int height;
        AvlNode left;
        AvlNode right;

        AvlNode(int key) { this.key = key;}

    }

    private AvlNode root;

    public void add(int key) {
        root = rebalancingAdd(key, root);
    }

    private AvlNode rebalancingAdd(int key, AvlNode node) {
        node = add(key, node);
        updateHeight(node);
        return rebalance(node);
    }

    private AvlNode add(int key, AvlNode node) {
        if(node == null) {
            return new AvlNode(key);
        }
        if(key < node.key) {
            node.left = rebalancingAdd(key, node.left);
        } else if (key > node.key) {
            node.right = rebalancingAdd(key, node.right);
        } else { //key == node.key
            throw new IllegalArgumentException("Key already exists!");
        }
        return node;
    }

    public AvlNode search(int key) {
        return search(key, root);
    }

    private AvlNode search(int key, AvlNode node) {
        if(doesntExist(node)) {
            return null;
        }
        if(key < node.key) {
            return search(key, node.left);
        } else if (key > node.key) {
            return search(key, node.right);
        }
        return node;
    }

    private boolean doesntExist(AvlNode node) {
        return node == null;
    }

    public void remove(int key) {
        root = remove(key, root);
    }

    private AvlNode rebalancingRemove(int key, AvlNode node) {
        node = remove(key, node);
        if(node == null) {
            return null;
        }
        updateHeight(node);
        return rebalance(node);
    }

    private AvlNode remove(int key, AvlNode node) {
        if(node == null) {
            return null;
        }
        if(node.left == null && node.right == null) {
            return null;
        }
        if(key < node.key) {
            node.left = rebalancingRemove(key, node.left);
        } else if (key > node.key) {
            node.right = rebalancingRemove(key, node.right);
        } else if (node.left == null) {
            node = node.right;
        } else if (node.right == null) {
            node = node.left;
        } else {
            removeNodeWithTwoChildren(node);
        }
        return node;
    }

    private void removeNodeWithTwoChildren(AvlNode node) {
        AvlNode successor = min(node.right);
        node.key = successor.key;
        node.right = rebalancingRemove(node.key, node.right);
    }

    private AvlNode min(AvlNode node) {
        while(node.left != null) {
            node = node.left;
        }
        return node;
    }

    public String print() {
        if(root != null) {
            Queue<AvlNode> nodes = new LinkedList<>();
            nodes.add(root);
            StringBuilder content = new StringBuilder();
            while(!nodes.isEmpty()) {
                AvlNode current = nodes.remove();
                if(current != null) {
                    content.append(current.key);
                    nodes.add(current.left);
                    nodes.add(current.right);
                }
            }
            return content.toString();
        }
        return "";
    }

    private AvlNode rebalance(AvlNode node) {
        int bf = balanceFactor(node);
        if(bf <= -2) { //left heavy
            AvlNode left = node.left;
            int lbf = balanceFactor(left);
            if(lbf > 0) {
                node.left = rotateLeft(left);
                node = rotateRight(node);
            } else {
                node = rotateRight(node);
            }
        }
        if(bf >= 2) { //right heavy
            AvlNode right = node.right;
            int rbf = balanceFactor(right);
            if(rbf < 0) {
                node.right = rotateRight(right);
                node = rotateLeft(node);
            } else {
                node = rotateLeft(node);
            }
        }
        return node;
    }

    private AvlNode rotateRight(AvlNode node) {
        AvlNode child = node.left;
        node.left = child.right;
        child.right = node;
        updateHeight(node);
        updateHeight(child);
        return child;
    }

    private AvlNode rotateLeft(AvlNode node) {
        AvlNode child = node.right;
        node.right = child.left;
        child.left = node;
        updateHeight(node);
        updateHeight(child);
        return child;
    }

    private void updateHeight(AvlNode node) {
        int left = height(node.left);
        int right = height(node.right);
        node.height = Math.max(left, right) + 1;
    }

    private int balanceFactor(AvlNode node) {
        return height(node.right) - height(node.left);
    }

    private int height(AvlNode node) {
        return node != null ? node.height : -1;
    }
}
