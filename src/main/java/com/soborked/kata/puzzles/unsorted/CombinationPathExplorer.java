package com.soborked.kata.puzzles.unsorted;

public class CombinationPathExplorer implements PathExplorer {

    //n options, r selections
    //n => rows -1 + columns -1
    //r => rows -1 or columns -1. Should come out to the same either way since this is combinations rather than permutations.
    //Breaks down if same size
    @Override
    public int explore(int rows, int columns) {
        int n = (rows -1) + (columns -1);
        int r = rows -1;
        return factorial(n) / (factorial(r) * factorial(n-r));
    }

    private int factorial(int n) {
        if(n==0) {
            return 1;
        }
        return n * factorial(n-1);
    }

}