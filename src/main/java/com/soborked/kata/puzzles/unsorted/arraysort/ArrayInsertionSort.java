package com.soborked.kata.puzzles.unsorted.arraysort;

public class ArrayInsertionSort implements ArraySorter {
    @Override
    public int[] sort(int[] given) {
        int size = given.length;
        for(int right=1; right<size; right++) {
            int val = given[right];
            int left = right - 1;
            while(left >= 0 && val < given[left]) {
                given[left+1] = given[left];
                left--;
            } //ensures left smaller than val or we have run out of room
            given[left+1] = val;
        }
        return given;
    }
}
