package com.soborked.kata.puzzles.unsorted;

public class StockChecker {
  public int maxProfit(int[] market) {
    int highestProfit = 0;
    for (int day = 0; day < market.length; day++) {
      int todaysPrice = market[day];
      int future = day + 1;
      int highestPriceInFuture = todaysPrice;
      while (future < market.length) {
        int futurePrice = market[future];
        if (futurePrice > highestPriceInFuture) {
          highestPriceInFuture = futurePrice;
        }
        future++;
      }
      int profit = highestPriceInFuture - todaysPrice;
      if (profit > highestProfit) {
        highestProfit = profit;
      }
    }

    return highestProfit;
  }
}
