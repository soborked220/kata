package com.soborked.kata.puzzles.unsorted.review;

public interface MajorityChecker {

    boolean isMajority(int[] given, int target);
}
