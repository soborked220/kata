package com.soborked.kata.puzzles.unsorted.review;

import java.util.Arrays;

public class MaxSubsequence {
    public int maxSum(int[] given) {
        int n = given.length;
        int[] solved = Arrays.copyOf(given, n);
        System.arraycopy(given, 0, solved, 0, n);
        int max = 0;
        for(int current=1; current< n; current++) {
            for(int previous=0; previous<current;previous++) {
                boolean increases = given[current] > given[previous];
                boolean isBetterToAddToRatherThanStartOver = solved[current] < solved[previous] + given[current];
                if(increases && isBetterToAddToRatherThanStartOver) {
                    solved[current] = solved[previous] + given[current];
                }
            }
            max = Math.max(max, solved[current]);
        }
        return max;
    }

}