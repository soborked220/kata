package com.soborked.kata.puzzles.unsorted;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class FastStockChecker {

  public int maxProfit(int[] market) {
    Stack<Integer> candidates = new Stack<>();
    int maxProfit = 0;
    for (int price : market) {
      if (candidates.isEmpty()) {
        candidates.push(price);
      } else {
        if (price < candidates.peek()) {
          candidates.push(price);
        } else {
          int profit = price - candidates.peek();
          maxProfit = Math.max(maxProfit, profit);
        }
      }
    }

    return maxProfit;
  }
}
