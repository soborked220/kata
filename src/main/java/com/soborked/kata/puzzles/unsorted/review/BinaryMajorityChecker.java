package com.soborked.kata.puzzles.unsorted.review;

public class BinaryMajorityChecker implements MajorityChecker {
    @Override
    public boolean isMajority(int[] given, int target) {
        int n = given.length;
        int mid = (n / 2);
        if(given[mid] != target) {
            return false;
        }
        int firstOccurrence = first(given, 0, n-1, n, target);
        int lastOccurrence = last(given, 0, n-1,n, target);
        return lastOccurrence - firstOccurrence + 1 > mid;
    }

    private int first(int given[], int start, int end, int n, int target) {
        if(start < end) {
            int mid = start + ((end - start) / 2);
            if(hitLeftBoundary(given, target, mid) && given[mid] == target) {
                return mid;
            } else if (shouldContinueDescending(target, given[mid])) {
                return first(given, start, mid-1, n, target);
            } else { //went too far, go back
                return first(given, mid+1, end, n ,target);
            }
        }
        return -1;
    }

    private boolean shouldContinueDescending(int target, int given) {
        return target <= given;
    }

    private boolean hitLeftBoundary(int[] given, int target, int current) {
        return current == 0 || target > given[current - 1];
    }

    private int last(int given[], int start, int end, int n, int target) {
        if(start < end) {
            int mid = start + ((end - start) / 2);
            if(hitRightBoundary(given, target, mid, n) && given[mid] == target) {
                return mid;
            } else if (shouldContinueClimbing(target, given[mid])) {
                return last(given, mid+1, end, n ,target);
            } else { //went too far, go back
                return last(given, start, mid-1, n, target);
            }
        }
        return -1;
    }

    private boolean shouldContinueClimbing(int target, int given) {
        return target >= given;
    }

    private boolean hitRightBoundary(int[] given, int target, int current, int n) {
        return (current == n -1) || target < given[current + 1];
    }


}
