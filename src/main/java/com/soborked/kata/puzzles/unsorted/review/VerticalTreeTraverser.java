package com.soborked.kata.puzzles.unsorted.review;

import java.util.*;

public class VerticalTreeTraverser {
    public int[][] sortByColumns(Integer[] tree) {
        Map<Integer, List<Integer>> valuesByColumn = new HashMap<>();
        buildCoordinates(tree, valuesByColumn, 0, 0, 0);
        return valuesByColumn.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .map(valuesInCol -> valuesInCol.stream().sorted().mapToInt(i -> i).toArray())
                .toArray(int[][]::new);
    }

    private void buildCoordinates(Integer[] tree, Map<Integer, List<Integer>> valuesByColumn, int node, int row, int col) {
        if(node < tree.length) {
            Integer currentVal = tree[node];
            if(currentVal != null) {
                valuesByColumn.putIfAbsent(col, new ArrayList<>());
                valuesByColumn.get(col).add(currentVal);
                int left = 2 * node + 1;
                buildCoordinates(tree, valuesByColumn, left, row + 1, col -1);
                int right = 2 * node + 2;
                buildCoordinates(tree, valuesByColumn, right, row + 1, col +1);
            }
        }
    }
}
