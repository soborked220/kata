package com.soborked.kata.puzzles.unsorted.review;

import com.soborked.kata.puzzles.unsorted.review.JobScheduler;

import java.util.Arrays;
import java.util.Comparator;

import static java.util.Comparator.comparingInt;

public class ProfitableJobScheduler implements JobScheduler {

    private static final int END = 1;
    private static final int START = 0;
    private static final int PROFIT = 2;
    @Override
    public int schedule(int[] startTimes, int[] endTimes, int[] profits) {
        int n = startTimes.length;
        int[][] jobs = new int[n][3];
        for(int i=0; i<n;i++) {
            jobs[i] = new int[]{startTimes[i], endTimes[i], profits[i]};
        }
        Arrays.sort(jobs, comparingInt(job -> job[END]));
        int[] profitAt = new int[n];
        profitAt[0] = jobs[0][PROFIT];
        for(int i = 1; i<n;i++) {
            profitAt[i] = profitAt[i-1];
            int l = 0;
            int r = i -1;
            int prev = 0;
            while(l<=r) {
                int mid = (l+r) / 2;
                if(jobs[mid][END]<= jobs[i][START]) {
                    prev = profitAt[mid];
                    l = mid +1;
                } else {
                    r = mid - 1;
                }
            }
            profitAt[i] = Math.max(profitAt[i], jobs[i][PROFIT]+prev);
        }
        return profitAt[n-1];
    }
}
