package com.soborked.kata.puzzles.unsorted.review;

import java.util.ArrayList;
import java.util.List;

public class IntegerCombinationFinder implements CombinationFinder {
    @Override
    public List<List<Integer>> combinations(int[] given, int target) {
        List<List<Integer>> combinations = new ArrayList<>();
        descend(combinations, new ArrayList<>(), given, target, 0);
        return combinations;
    }

    private void descend(List<List<Integer>> combinations, List<Integer> combo, int[] given, int remaining, int start) {
        if(remaining >= 0) {
            if(remaining == 0) {
                combinations.add(new ArrayList<>(combo));
            } else {
                for(int i = start; i<given.length; i++) {
                    int val = given[i];
                    combo.add(val);
                    descend(combinations, combo, given, remaining - val, i);
                    combo.remove(combo.size()-1);
                }
            }
        }
    }

}