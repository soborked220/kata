package com.soborked.kata.puzzles.unsorted.arraysort;

public class ArraySortByMin implements ArraySorter {

  public int[] sort(int[] nums) {
    int max = nums[0];
    int min = nums[0];
    for (int num : nums) {
      max = Math.max(max, num);
      min = Math.min(min, num);
    }
    int MaxDistanceFromMin = max - min;
    int[] countOfEachNumberAt = new int[MaxDistanceFromMin + 1];
    for (int num : nums) {
      countOfEachNumberAt[num - min]++;
    }
    int index = 0;
    for (int distanceFromMin = 0; distanceFromMin <= MaxDistanceFromMin; distanceFromMin++) {
      int currentNumber = min + distanceFromMin;
      while (countOfEachNumberAt[distanceFromMin] > 0) {
        nums[index] = currentNumber;
        index++;
        countOfEachNumberAt[distanceFromMin]--;
      }
    }
    return nums;
  }
}
