package com.soborked.kata.puzzles.unsorted;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class IntegerHeapFrequency implements IntegerCounter {
    @Override
    public int[] topKFrequent(int[] given, int k) {
        Map<Integer, Integer> counts = new HashMap<>();
        for(int num : given) {
            counts.merge(num, 1, Integer::sum);
        }
        PriorityQueue<Integer> prioritized = new PriorityQueue<>((a, b) -> counts.get(b) - counts.get(a));
        prioritized.addAll(counts.keySet());
        int[] results = new int[k];
        for(int i=0; i< k;i++) {
            results[i] = prioritized.poll();
        }
        return results;
    }
}
