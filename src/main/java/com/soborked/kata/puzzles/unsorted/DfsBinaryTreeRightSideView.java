package com.soborked.kata.puzzles.unsorted;

import java.util.ArrayList;
import java.util.List;

public class DfsBinaryTreeRightSideView implements RightSideView {
  public List<Integer> visible(TreeNode given) {
    List<Integer> visible = new ArrayList<>();
    if (given == null || (given.right == null && given.left == null)) {
      return visible;
    }
    descend(given, 0, visible);
    return visible;
  }

  private void descend(TreeNode current, int depth, List<Integer> visible) {
    if (current != null) {
      if (firstTimeAt(depth, visible)) {
        visible.add(current.val);
      }
      descend(current.right, depth + 1, visible);
      descend(current.left, depth + 1, visible);
    }
  }

  private boolean firstTimeAt(int depth, List<Integer> visible) {
    return depth == visible.size();
  }
}
