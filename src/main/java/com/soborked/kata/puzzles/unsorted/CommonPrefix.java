package com.soborked.kata.puzzles.unsorted;

public class CommonPrefix {

    public String longestPrefix(String[] given) {
        int min = Integer.MAX_VALUE;
        for(String str : given) {
            min = Math.min(str.length(), min);
        }
        int c = longestCommon(given, min);
        return given[0].substring(0,c);
    }

    private int longestCommon(String[] given, int min) {
        int c = 0;
        for(; c < min; c++) {
            char current = given[0].charAt(c);
            for (String str : given) {
                if (str.charAt(c) != current) {
                    return c;
                }
            }
        }
        return c;
    }
}
