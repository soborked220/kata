package com.soborked.kata.puzzles.unsorted;

public interface HardObstacleNavigator {

    int explore(int[][] grid);
}
