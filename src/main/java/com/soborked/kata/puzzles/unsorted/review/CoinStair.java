package com.soborked.kata.puzzles.unsorted.review;

public class CoinStair {
    public int climb(int given) {
        int steps = 0;
        int coins = given;
        while(coins > 0) {
            steps++;
            coins-=steps;
        }
        return coins == 0 ? steps : steps -1;
    }
}
