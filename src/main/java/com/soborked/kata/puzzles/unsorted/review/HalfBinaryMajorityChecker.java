package com.soborked.kata.puzzles.unsorted.review;

public class HalfBinaryMajorityChecker implements MajorityChecker {

    //Requires array to be sorted, which allows following assumptions
    //1. the midpoint will be the target if the target is a majority
    //2. the min distance between indexes will be n/2 if the target is a majority
    @Override
    public boolean isMajority(int[] given, int target) {
        int n = given.length;
        int mid = n / 2;
        if(given[mid] != target) {
            return false;
        }
        int firstOccurrence = findFirst(given, 0, n-1, target);
        int minEndExpectedIfMajority = firstOccurrence + mid;
        return given[minEndExpectedIfMajority] == target;
    }

    private int findFirst(int[] given, int start, int end, int target) {
        if(start < end) {
            int mid = start + ((end - start)/2);
            int current = given[mid];
            if(current < target) { //gone too far, go back
                return findFirst(given, mid +1, end, target);
            } else if(current == target) {
                if((mid == 0 || current > given[mid-1])) {
                    return mid;
                } else { //keep going left
                    return findFirst(given, start, mid-1, target);
                }
            } else {
                //shouldn't ever happen because we fail fast if mid of given isn't equal to target
            }
        }
        return -1;
    }
}
