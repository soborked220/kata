package com.soborked.kata.puzzles.unsorted.review;

import java.util.HashMap;
import java.util.Map;

public class LeastCache implements LeastRecentlyUsedCache {

    private final Map<String, TwoWayNode> findByKey = new HashMap<>();

    private final int capacity;
    private final TwoWayNode head = new TwoWayNode();
    private final TwoWayNode tail = new TwoWayNode();

    private static class TwoWayNode {
        TwoWayNode left;
        TwoWayNode right;
        String key;
        int value;
    }

    public LeastCache(int capacity) {
        this.capacity = capacity;
        head.right = tail;
        tail.left = head;
    }

    @Override
    public void put(String key, int value) {
        if(findByKey.containsKey(key)) {
            TwoWayNode updated = findByKey.get(key);
            updated.value = value;
            moveToFront(updated);
        } else {
            TwoWayNode latest = new TwoWayNode();
            latest.key = key;
            latest.value = value;
            putAtFront(latest);
            findByKey.put(latest.key, latest);
        }
        if(findByKey.size() > capacity) {
            TwoWayNode oldest = tail.left;
            remove(oldest);
            findByKey.remove(oldest.key);
        }
    }

    private void moveToFront(TwoWayNode used) {
        remove(used);
        putAtFront(used);
    }

    private void putAtFront(TwoWayNode latest) {
        latest.left = head;
        latest.right = head.right;
        head.right.left = latest;
        head.right = latest;
    }

    private void remove(TwoWayNode oldest) {
        oldest.right.left = oldest.left;
        oldest.left.right = oldest.right;
    }

    @Override
    public Integer get(String key) {
        if(findByKey.containsKey(key)) {
            TwoWayNode found = findByKey.get(key);
            moveToFront(found);
            return found.value;
        }
        return null;
    }

}
