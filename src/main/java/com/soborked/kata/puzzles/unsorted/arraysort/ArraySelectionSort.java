package com.soborked.kata.puzzles.unsorted.arraysort;

public class ArraySelectionSort implements ArraySorter {
    @Override
    public int[] sort(int[] given) {
        int size = given.length;
        for(int left = 0; left < size - 1; left++) {
            int min = left;
            int right = left + 1;
            for(; right < size; right++) {
                if(given[right] < given[min]) {
                    min = right;
                }
            }
            swap(given, left, min);
        }
        return given;
    }

    private void swap(int[] given, int left, int right) {
        int val = given[left];
        given[left] = given[right];
        given[right] = val;
    }
}
