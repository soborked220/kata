package com.soborked.kata.puzzles.unsorted;

import java.util.LinkedList;
import java.util.Queue;

public class IslandBreadthExplorer {

  private static final int TRUE = 1;
  private static final char LAND = '1';

  public int islandsIn(char[][] given) {
    int depth = given.length;
    int breadth = given[0].length;
    int[][] visited = new int[depth][breadth];
    int numberOfIslands = 0;
    for (int row = 0; row < depth; row++) {
      for (int col = 0; col < breadth; col++) {
        if (visited[row][col] != TRUE && given[row][col] == LAND) {
          numberOfIslands++;
          explore(row, col, visited, given);
        }
      }
    }
    return numberOfIslands;
  }

  private void explore(int row, int col, int[][] visited, char[][] given) {
    visited[row][col] = TRUE;
    Queue<int[]> landmass = new LinkedList<>();
    landmass.add(new int[] {row, col});

    while (!landmass.isEmpty()) {
      int[] location = landmass.peek();
      int r = location[0];
      int c = location[1];
      landmass.remove();
      exploreNeighbor(r, c + 1, visited, given, landmass);
      exploreNeighbor(r, c - 1, visited, given, landmass);
      exploreNeighbor(r + 1, c, visited, given, landmass);
      exploreNeighbor(r - 1, c, visited, given, landmass);
    }
  }

  private void exploreNeighbor(
      int neighborRow, int neighborCol, int[][] visited, char[][] given, Queue<int[]> landmass) {
    int depth = given.length;
    int breadth = given[0].length;
    if (inBounds(neighborRow, depth)
        && inBounds(neighborCol, breadth)
        && visited[neighborRow][neighborCol] != TRUE
        && given[neighborRow][neighborCol] == LAND) {
      visited[neighborRow][neighborCol] = TRUE;
      landmass.add(new int[] {neighborRow, neighborCol});
    }
  }

  private boolean inBounds(int val, int max) {
    return val >= 0 && val < max;
  }
}
