package com.soborked.kata.puzzles.unsorted;

public interface PileMaximizer {

    int maximize(int[] nums, int moves);
}
