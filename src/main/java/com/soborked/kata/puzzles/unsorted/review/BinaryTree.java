package com.soborked.kata.puzzles.unsorted.review;

public class BinaryTree {

    public static class Node {
        private Node(int key) {
            this.key = key;
        }
        int key;
        Node left;
        Node right;
    }

    private Node root;

    public Node root() {
        return root;
    }

    public Node search(int key) {
        return search(key, root);
    }

    private Node search(int key, Node node) {
        if(node == null) {
            return null;
        }
        if(key == node.key) {
            return node;
        } else if(key > node.key) {
           return search(key, node.right);
        } else {
           return search(key, node.left);
        }
    }

    public void add(int key) {
        root = add(key, root);
    }

    private Node add(int key, Node node) {
        if(node == null) {
            node = new Node(key);
        } else if (key < node.key) {
            node.left = add(key, node.left);
        } else if (key > node.key) {
            node.right = add(key, node.right);
        } else {
            throw new IllegalArgumentException("Key already exists");
        }
        return node;
    }

    public void remove(int key) {
        root = remove(key, root);
    }

    private Node remove(int key, Node node) {
        if(node == null) {
            return null;
        }
        if(node.left == null && node.right == null) {
            return null;
        }
        if(key < node.key) {
            node.left = remove(key, node.left);
        } else if (key > node.key) {
            node.right = remove(key, node.right);
        } else if (node.left == null) {
            node = node.right;
        } else if (node.right == null) {
            node = node.left;
        } else {
            removeNodeWithTwoChildren(node);
        }
        return node;
    }

    private void removeNodeWithTwoChildren(Node node) {
        Node successor = min(node.right);
        node.key = successor.key;
        node.right = remove(node.key, node.right);
    }

    private Node min(Node node) {
        while(node.left != null) {
            node = node.left;
        }
        return node;
    }

    public String print() {
        return print(root);
    }

    private String print(Node node) {
        if(node != null) {
            return print(node.left) + node.key + print(node.right);
        } else {
            return "";
        }
    }
}
