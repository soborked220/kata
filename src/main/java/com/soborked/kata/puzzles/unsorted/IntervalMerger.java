package com.soborked.kata.puzzles.unsorted;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class IntervalMerger {

  public int[][] merge(int[][] intervals) {
    List<int[]> answer = new ArrayList<>();
    Arrays.sort(intervals, Comparator.comparingInt(ary -> ary[0]));
    int[] current = intervals[0];
    int i = 1;
    while(i < intervals.length) {
      int[] next = intervals[i];
      if(current[1] > next[0]) {
        int start = current[0];
        int end = Math.max(current[1],next[1]);
        current = new int[] { start, end};
      } else {
        answer.add(current);
        current = next;
      }
      i++;
    }
    answer.add(current);
    return answer.toArray(int[][]::new);
  }
}
