package com.soborked.kata.puzzles.unsorted;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class IntegerFrequency implements IntegerCounter {
  public int[] topKFrequent(int[] given, int k) {
    Map<Integer, Integer> counts = new HashMap<>();
    for (int num : given) {
      counts.merge(num, 1, Integer::sum);
    }
    return counts.entrySet().stream()
        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
        .limit(k)
        .mapToInt(Map.Entry::getKey)
        .toArray();
  }

}