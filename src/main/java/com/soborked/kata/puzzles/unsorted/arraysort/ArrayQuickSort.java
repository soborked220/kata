package com.soborked.kata.puzzles.unsorted.arraysort;

public class ArrayQuickSort implements ArraySorter {

  @Override
  public int[] sort(int[] given) {
    divide(given, 0, given.length - 1);
    return given;
  }

  private void divide(int[] given, int start, int end) {
    if (start < end) {
      int pivot = sortAndPivot(given, start, end);
      divide(given, start, pivot - 1);
      divide(given, pivot + 1, end);
    }
  }

  private int sortAndPivot(int[] given, int start, int end) {
    int pivot = given[end];
    int g = start;
    for (int i = start; i < end; i++) {
      if (given[i] <= pivot) {
        int temp = given[i];
        given[i] = given[g];
        given[g] = temp;
        g++;
      }
    }
    given[end] = given[g];
    given[g] = pivot;
    return g;
  }
}
