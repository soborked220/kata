package com.soborked.kata.puzzles.unsorted;

import java.util.List;

public interface WordCounter {

    List<String> countFrequency(String[] words, int k);

}
