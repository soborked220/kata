package com.soborked.kata.puzzles.unsorted;

/**
 * Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return
 * the number of islands.
 *
 * <p>An island is surrounded by water and is formed by connecting adjacent lands horizontally or
 * vertically. You may assume all four edges of the grid are all surrounded by water.
 *
 * <p>Example 1:
 *
 * <p>Input: grid = [ ["1","1","1","1","0"], ["1","1","0","1","0"], ["1","1","0","0","0"],
 * ["0","0","0","0","0"] ] Output: 1
 *
 * <p>Example 2:
 *
 * <p>Input: grid = [ ["1","1","0","0","0"], ["1","1","0","0","0"], ["0","0","1","0","0"],
 * ["0","0","0","1","1"] ] Output: 3
 *
 * <p>Constraints:
 *
 * <p>m == grid.length n == grid[i].length 1 <= m, n <= 300 grid[i][j] is '0' or '1'.
 */
public class IslandDepthExplorer {

  private static final char LAND = '1';
  private static final char ALREADY_COUNTED = 'C';

  public int islandsIn(char[][] given) {
    int numberOfIslands = 0;
    int depth = given.length;
    int breadth = given[0].length;
    for (int row = 0; row < depth; row++) {
      for (int col = 0; col < breadth; col++) {
        if (given[row][col] == LAND) {
          numberOfIslands++;
          explore(row, col, given);
        }
      }
    }
    return numberOfIslands;
  }

  private void explore(int row, int col, char[][] given) {
    if (inBounds(row, given.length) && inBounds(col, given[0].length) && given[row][col] == LAND) {
      given[row][col] = ALREADY_COUNTED;
      explore(row, col + 1, given);
      explore(row, col - 1, given);
      explore(row + 1, col, given);
      explore(row - 1, col, given);
    }
  }

  private boolean inBounds(int val, int max) {
    return val >= 0 && val < max;
  }
}
