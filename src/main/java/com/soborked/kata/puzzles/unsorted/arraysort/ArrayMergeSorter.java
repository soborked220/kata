package com.soborked.kata.puzzles.unsorted.arraysort;

import java.util.Arrays;

public class ArrayMergeSorter implements ArraySorter {
  public int[] sort(int[] given) {
    sort(given, 0, given.length - 1);
    return given;
  }

  public void sort(int[] numbers, int start, int end) {
    if (start < end) {
      int mid = start + ((end - start) / 2);
      sort(numbers, start, mid);
      sort(numbers, mid + 1, end);
      merge(numbers, start, mid, end);
    }
  }

  private void merge(int[] numbers, int start, int mid, int end) {
    int[] front = Arrays.copyOfRange(numbers, start, mid + 1);
    int[] back = Arrays.copyOfRange(numbers, mid + 1, end + 1);
    int i = 0;
    int j = 0;
    int k = start;
    while (i < front.length && j < back.length) {
      if (front[i] < back[j]) {
        numbers[k] = front[i];
        i++;
      } else {
        numbers[k] = back[j];
        j++;
      }
      k++;
    }
    addRemaining(numbers, front, i, k);
    addRemaining(numbers, back, j, k);
  }

  private void addRemaining(int[] numbers, int[] section, int i, int k) {
    while (i < section.length) {
      numbers[k] = section[i];
      i++;
      k++;
    }
  }
}
