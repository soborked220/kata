package com.soborked.kata.puzzles.unsorted;

public class LightStockChecker {
  public int maxProfit(int[] prices) {
    int maxProfit = 0;
    int lowest = prices[0];
    for (int price : prices) {
      int profit = price - lowest;
      if (profit > maxProfit) {
        maxProfit = profit;
      } else if (price < lowest) {
        lowest = price;
      }
    }
    return maxProfit;
  }
}
