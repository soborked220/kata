package com.soborked.kata.puzzles.unsorted.arraysort;

public class ArrayHeapSort implements ArraySorter {
    @Override
    public int[] sort(int[] given) {
        int size = given.length;
        int lastLeafNode = (size/2) -1;
        for(int i=lastLeafNode; i>=0; i--) {
            heap(given, size, i);
        }
        for(int rightMostLeaf=size-1; rightMostLeaf>=0; rightMostLeaf--){
            int swap = given[rightMostLeaf];
            given[rightMostLeaf] = given[0];
            given[0] = swap;
            heap(given, rightMostLeaf, 0);
        }
        return given;
    }

    private void heap(int[] given, int size, int parent) {
        int left = (parent * 2) + 1;
        int right = (parent * 2) + 2;
        int largest = parent;
        if(left < size && given[left] > given[largest]) {
            largest = left;
        }
        if(right < size && given[right] > given[largest]) {
            largest = right;
        }
        if(largest != parent) {
            int swap = given[parent];
            given[parent] = given[largest];
            given[largest] = swap;
            heap(given, size, largest);
        }
    }
}
