package com.soborked.kata.puzzles.unsorted;

public class RecursiveExplorer implements PathExplorer {
    @Override
    public int explore(int rows, int columns) {
        int[][] uniquePathsBySquare = new int[rows][columns];
        return explore(uniquePathsBySquare, 0,0);
    }

    private int explore(int[][] visited, int row, int column) {
        if(row >= visited.length || column >= visited[0].length) {
            return 0;
        }
        if(row == visited.length -1 && column == visited[0].length-1) {
            return 1;
        }
        if(visited[row][column] != 0) {
            return visited[row][column];
        }
        visited[row][column] = explore(visited, row+1, column) + explore(visited, row, column+1);
        return visited[row][column];
    }
}
