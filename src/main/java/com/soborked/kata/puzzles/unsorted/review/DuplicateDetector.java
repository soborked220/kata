package com.soborked.kata.puzzles.unsorted.review;

import java.util.HashSet;
import java.util.Set;

public class DuplicateDetector {



    public boolean hasDuplicate(int[] given) {
        Set<Integer> seen = new HashSet<>();
        for(int num:given) {
            if(seen.contains(num)) {
                return true;
            } else {
                seen.add(num);
            }
        }
        return false;
    }
}
