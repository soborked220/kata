package com.soborked.kata.puzzles.unsorted.review;

public class BinaryTreeRecursiveValidator implements BinaryTreeValidator {


    @Override
    public boolean isValid(BinaryTreeTraversalValidator.Node node) {
        return isValid(node, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private boolean isValid(BinaryTreeTraversalValidator.Node node, int min, int max) {
        if(node == null) {
            return true;
        }
        boolean left = isValid(node.getLeft(), min, node.getVal() -1);
        boolean current = node.getVal() >= min && node.getVal() <= max;
        boolean right = isValid(node.getRight(), node.getVal()+1, max);
        return current && left && right;
    }
}
