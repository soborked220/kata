package com.soborked.kata.puzzles.unsorted.review;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PhoneToLetterConverter {
    public String[] phoneToLetterCombos(String given) {
        String[] lettersByKey = new String[] {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        List<String> combos = new ArrayList<>();
        Queue<String> currentFloorInTree = new LinkedList<>();
        currentFloorInTree.add("");
        char[] input = given.toCharArray();
        int maxDepth = input.length;
        while(!currentFloorInTree.isEmpty()) {
            String current = currentFloorInTree.remove();
            if(current.length() == maxDepth) {
                combos.add(current);
            } else {
                int depth = current.length();
                addChildren(lettersByKey, currentFloorInTree, current, input[depth]);
            }
        }
        return combos.toArray(new String[0]);
    }

    private void addChildren(String[] lettersByKey, Queue<String> currentFloorInTree, String current, char input) {
        int key = Integer.parseInt(String.valueOf(input));
        char[] letters = lettersByKey[key].toCharArray();
        for(char letter : letters) {
            currentFloorInTree.add(current + letter);
        }
    }

}
