package com.soborked.kata.puzzles.unsorted;

public class HardObstacleWalker implements HardObstacleNavigator {

    private static final int OBSTACLE = -1;
    private static final int START = 1;
    private static final int FINISH = 2;
    private static final int WALKED = -2;
    private static final int FREE = 0;

    @Override
    public int explore(int[][] grid) {
        int maxR = grid.length;
        int maxC = grid[0].length;
        int freeSquares = 0;
        int startR = 0;
        int startC = 0;
        for(int row=0; row<maxR;row++) {
            for(int col=0; col<maxC; col++) {
                int val = grid[row][col];
                if(val == START) {
                    startR= row;
                    startC = col;
                }
                if(val == FREE || val==START) {
                    freeSquares++;
                }
            }
        }
        return explore(grid, freeSquares, startR, startC, maxR, maxC);
    }

    private int explore(int[][] grid,int remaining, int startR, int startC, int maxR, int maxC) {
        if(startR < 0 || startR >= maxR || startC < 0 || startC >= maxC) {
            return 0;
        }
        if(grid[startR][startC] == OBSTACLE) {
            return 0;
        }
        if(grid[startR][startC] == WALKED) {
            return 0;
        }
        if(grid[startR][startC] == FINISH) {
            if(remaining == 0) {
                return 1;
            } else {
                return 0;
            }
        }
        grid[startR][startC] = WALKED;
        remaining--;
        int up = explore(grid,remaining, startR-1, startC, maxR, maxC);
        int down = explore(grid,remaining, startR+1, startC, maxR, maxC);
        int left = explore(grid,remaining, startR, startC-1, maxR, maxC);
        int right = explore(grid,remaining, startR, startC+1, maxR, maxC);
        int paths = up + down + left + right;
        grid[startR][startC] = FREE;
        return paths;
    }

}