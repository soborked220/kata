package com.soborked.kata.puzzles.unsorted;

public interface IntegerCounter {

    int[] topKFrequent(int[] given, int k);
}
