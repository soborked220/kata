package com.soborked.kata.puzzles.unsorted;

import java.util.LinkedList;
import java.util.Queue;

public class BfsQueueNextNode implements NodeConnector {

  public Node connect(Node given) {
    Queue<Node> queue = new LinkedList<>();
    queue.add(given);

    while (!queue.isEmpty()) {
      Node prev = null;
      int nodesOnThisLevel = queue.size();
      for (int n = 0; n < nodesOnThisLevel; n++) {
        Node current = queue.poll();
        if (current != null) {
          current.next = prev;
          prev = current;
          queue.add(current.right);
          queue.add(current.left);
        }
      }
    }
    return given;
  }
}
