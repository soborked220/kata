package com.soborked.kata.puzzles.unsorted;

public class KCopiesCharacter {

  private static final int A = 0;
  private static final int B = 1;
  private static final int C = 2;
  private static final int INDEX_OFFSET = 1;
  private static final int NOT_POSSIBLE = -1;

  public int min(String s, int k) {
    int[] counts = new int[3];
    char[] input = s.toCharArray();
    int length = input.length;
    int current = 0;
    for (; current < length; current++) {
      counts[input[current] - 'a']++;
      if (counts[A] >= k && counts[B] >= k && counts[C] >= k) {
        break;
      }
    }
    if (current == length) {
      return NOT_POSSIBLE;
    }

    int minutes = current + INDEX_OFFSET;
    int min = minutes;
    int end = length - INDEX_OFFSET;
    while (current >= 0) {
      if (counts[input[current] - 'a'] == k) {
        while (input[current] != input[end]) {
          counts[input[end] - 'a']++;
          end--;
          minutes++;
        }
        end--;
      } else {
        counts[input[current] - 'a']--;
        minutes--;
        min = Math.min(minutes, min);
      }
      current--;
    }
    return min;
  }
}
