package com.soborked.kata.puzzles.unsorted;

public interface NodeConnector {

  Node connect(Node given);
}
