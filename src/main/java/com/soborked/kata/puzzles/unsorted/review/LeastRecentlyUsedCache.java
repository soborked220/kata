package com.soborked.kata.puzzles.unsorted.review;

public interface LeastRecentlyUsedCache {

    void put(String key, int value);
    Integer get(String key);

}