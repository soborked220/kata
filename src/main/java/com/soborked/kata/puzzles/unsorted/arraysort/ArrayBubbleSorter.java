package com.soborked.kata.puzzles.unsorted.arraysort;

public class ArrayBubbleSorter implements ArraySorter {
  @Override
  public int[] sort(int[] given) {
    int safeEnd = given.length - 1;
    for (int i = 0; i < safeEnd; i++) {
      boolean swapped = false;
      for (int j = 0; j < safeEnd - i; j++) {
        if (given[j] > given[j + 1]) {
          int temp = given[j + 1];
          given[j + 1] = given[j];
          given[j] = temp;
          swapped = true;
        }
      }
      if (!swapped) {
        break;
      }
    }
    return given;
  }
}
