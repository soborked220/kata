package com.soborked.kata.puzzles.unsorted.review;

import com.soborked.kata.puzzles.unsorted.PairCounter;

import java.util.HashMap;
import java.util.Map;

public class MaxPairCounter implements PairCounter {
    @Override
    public int countPairs(int[] numbers) {
        Map<Integer,Integer> counts = new HashMap<>();
        for(int num : numbers) {
            counts.merge(num, 1, Integer::sum);
        }
        return counts.values().stream().mapToInt(frequency -> frequency/2).sum();
    }
}
