package com.soborked.kata.puzzles.unsorted.review;

import lombok.Data;

public class BinaryTreeTraversalValidator implements BinaryTreeValidator {

    private Node previous = null;

    public boolean isValid(Node node) {
        return isValidTraversal(node);
    }

    private boolean isValidTraversal(Node current) {
        if(current == null) {
            return true;
        }
        boolean left = isValidTraversal(current.left);
        if(previous != null && current.val < previous.val) {
            return false;
        }
        previous = current;
        boolean right = isValidTraversal(current.right);
        return left && right;
    }

    @Data
    public static class Node {
        private int val;
        private Node left;
        private Node right;
    }
}
