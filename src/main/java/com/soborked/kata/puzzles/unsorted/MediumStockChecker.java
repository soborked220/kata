package com.soborked.kata.puzzles.unsorted;

public class MediumStockChecker {

  // loop through once, and record each rise in price and the total profit for that day
  // 7,1,5,3,6,4
  // day 0 -> day 1 => -6
  // day 1 -> day 2 => 4
  // day 2 -> day 3 => -2
  // day 3 -> day 4 => 3
  // day 4 -> day 5 => -2
  // total profit is 7

  // 7,1,5,6,6,4
  // day 0 -> day 1 => -6
  // day 1 -> day 2 => 4
  // day 2 -> day 3 => 1
  // day 3 -> day 4 => 0
  // day 4 -> day 5 => -2
  // total profit is 5

  // 7,1,5,6,1,4
  // day 0 -> day 1 => -6
  // day 1 -> day 2 => 4
  // day 2 -> day 3 => 1
  // day 3 -> day 4 => -5
  // day 4 -> day 5 => 3
  // total profit is 8

  // 7,6,4,3,1
  // day 0 -> day 1 => -1
  // day 1 -> day 2 => -2
  // day 2 -> day 3 => -1
  // day 3 -> day 4 => -2
  // total profit is 0

  public int maxProfit(int[] given) {
    int profit = 0;
    for (int day = 0; day < given.length - 1; day++) {
      int todaysPrice = given[day];
      int tomorrowsPrice = given[day + 1];
      if (tomorrowsPrice > todaysPrice) {
        profit += tomorrowsPrice - todaysPrice;
      }
      ;
    }
    return profit;
  }
}
