package com.soborked.kata.puzzles.unsorted;

public class EdgePathExplorer implements PathExplorer {

    //For each step, I have two options: down or right.
    //For any given square, the number of unique paths to it is the sum of unique paths to square above and square left.
    //Each square on an edge only has one path to it: a straight line (either right or down)
    @Override
    public int explore(int rows, int columns) {
        int uniquePathsBySquare[][] = new int[rows][columns];
        for(int r =0; r< rows; r++) {
            uniquePathsBySquare[r][0] = 1;
        }
        for(int c=0; c<columns; c++) {
            uniquePathsBySquare[0][c] = 1;
        }
        for(int r=1; r<rows; r++) {
            for(int c=1; c<columns;c++) {
                uniquePathsBySquare[r][c] = uniquePathsBySquare[r-1][c] + uniquePathsBySquare[r][c-1];
            }
        }
        return uniquePathsBySquare[rows-1][columns-1];
    }
}
