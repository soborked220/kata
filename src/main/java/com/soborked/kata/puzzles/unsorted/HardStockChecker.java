package com.soborked.kata.puzzles.unsorted;

// Each day I have the following choices:
// Buy for first time. Sell for first time. Buy for second time. Sell for second time. Do nothing.
// My goal is to maximize my balance regardless of my choice
public class HardStockChecker {

  // 3,3,5,0,0,3,1,4
  // day 0 -> day 1 => 0
  // day 1 -> day 2 => 2
  // day 2 -> day 3 => -5
  // day 3 -> day 4 => 0
  // day 4 -> day 5 => 3
  // day 5 -> day 6 => -2
  // day 6 -> day 7 => 3

  // 3,3,6,0,1,3,1,3
  // day 0 -> day 1 => 0
  // day 1 -> day 2 => 3
  // day 2 -> day 3 => -6
  // day 3 -> day 4 => 1
  // day 4 -> day 5 => 2
  // day 5 -> day 6 => -2
  // day 6 -> day 7 => 2

  // 1,2,3,4,5
  // day 0 -> day 1 => 1
  // day 1 -> day 2 => 1
  // day 2 -> day 3 => 1
  // day 3 -> day 4 => 1

  // 1,2,3,0,5
  // day 0 -> day 1 => 1
  // day 1 -> day 2 => 1
  // day 2 -> day 3 => -3
  // day 3 -> day 4 => 5

  private static final int STARTING_BALANCE = 0;

  public int maxProfit(int[] prices) {
    int balanceAtTimeOfFirstPurchase = Integer.MIN_VALUE;
    int balanceAtTimeOfSecondPurchase = Integer.MIN_VALUE;
    int balanceAtTimeOfFirstSale = 0;
    int balanceAtTimeOfFinalSale = 0;

    for (int todaysPrice : prices) {
      int buyFirst = STARTING_BALANCE - todaysPrice;
      int sellFirst = balanceAtTimeOfFirstPurchase + todaysPrice;
      int buySecond = balanceAtTimeOfFirstSale - todaysPrice;
      int sellSecond = balanceAtTimeOfSecondPurchase + todaysPrice;
      balanceAtTimeOfFirstPurchase = Math.max(balanceAtTimeOfFirstPurchase, buyFirst);
      balanceAtTimeOfFirstSale = Math.max(balanceAtTimeOfFirstSale, sellFirst);
      balanceAtTimeOfSecondPurchase = Math.max(balanceAtTimeOfSecondPurchase, buySecond);
      balanceAtTimeOfFinalSale = Math.max(balanceAtTimeOfFinalSale, sellSecond);
    }
    return balanceAtTimeOfFinalSale;
  }
}
