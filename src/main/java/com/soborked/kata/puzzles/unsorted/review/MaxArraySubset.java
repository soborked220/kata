package com.soborked.kata.puzzles.unsorted.review;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MaxArraySubset {
    public int maxDiff(int[] given) {
        Map<Integer, Integer> countOfNumber = new HashMap<>();
        Map<Integer, Integer> countOfNegNumber = new HashMap<>();
        for(int num : given) {
            if(num < 0) {
                countOfNegNumber.merge(num, 1, Integer::sum);
            } else {
                countOfNumber.merge(num, 1, Integer::sum);
            }
        }
        if(countOfNegNumber.size() == 0 && sumNonDuplicates(countOfNumber) == 0) {
            return 0;
        }
        if(countOfNumber.size() == 0 && sumNonDuplicates(countOfNegNumber) == 0) {
            return 0;
        }
        if(countOfNegNumber.size() == 0) {
            int min = countOfNumber.keySet().stream().min(Integer::compareTo).orElse(0);
            int posSum = sumNonDuplicates(countOfNumber) - min;
            return posSum - min;
        }
        if(countOfNumber.size() == 0) {
            int max = countOfNegNumber.keySet().stream().max(Integer::compareTo).orElse(0);
            int negSum = sumNonDuplicates(countOfNegNumber) - max;
            return max - negSum;
        }
        int negSum = sumNonDuplicates(countOfNegNumber);
        int posSum = sumNonDuplicates(countOfNumber);
        return posSum - negSum;
    }

    private int sumNonDuplicates(Map<Integer, Integer> map) {
        return map.entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .mapToInt(Map.Entry::getKey)
                .sum();
    }
}
