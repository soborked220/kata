package com.soborked.kata.puzzles.unsorted;

import java.util.List;

public interface RightSideView {

  List<Integer> visible(TreeNode given);
}
