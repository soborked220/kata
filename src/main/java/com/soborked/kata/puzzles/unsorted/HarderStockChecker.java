package com.soborked.kata.puzzles.unsorted;

public class HarderStockChecker {

  private static final int STARTING_BALANCE = 0;

  public int maxProfit(int transactions, int[] prices) {
    int[][] balancesByTransaction =
        new int[transactions][]; // Each will have balance at purchase and at sale
    for (int transaction = 0; transaction < transactions; transaction++) {
      balancesByTransaction[transaction] = new int[] {Integer.MIN_VALUE, 0};
    }

    for (int todaysPrice : prices) {
      int firstBuy = balancesByTransaction[0][0];
      int firstSell = balancesByTransaction[0][1];
      balancesByTransaction[0][0] = Math.max(firstBuy, STARTING_BALANCE - todaysPrice);
      balancesByTransaction[0][1] = Math.max(firstSell, balancesByTransaction[0][0] + todaysPrice);

      for (int transaction = 1; transaction < transactions; transaction++) {
        int balanceAtPrevTransactionSell = balancesByTransaction[transaction - 1][1];
        int balanceAtThisTransactionBuy = balancesByTransaction[transaction][0];
        int balanceAtThisTransactionSell = balancesByTransaction[transaction][1];
        balancesByTransaction[transaction][0] =
            Math.max(balanceAtThisTransactionBuy, balanceAtPrevTransactionSell - todaysPrice);
        balancesByTransaction[transaction][1] =
            Math.max(
                balanceAtThisTransactionSell, balancesByTransaction[transaction][0] + todaysPrice);
      }
    }
    return balancesByTransaction[transactions - 1][1];
  }
}
