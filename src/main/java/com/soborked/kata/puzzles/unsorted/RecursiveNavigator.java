package com.soborked.kata.puzzles.unsorted;

public class RecursiveNavigator implements ObstacleNavigator {

    //For each square
    //The number of paths is equal to the number of paths from the two neighbors (right and down)
    @Override
    public int explore(int[][] grid) {
        int r = grid.length;
        int c = grid[0].length;
        int[][] paths = new int[r][c];
        return explore(0,0,r,c,grid,paths);
    }

    private int explore(int row, int col, int maxR, int maxC, int[][] grid, int[][] paths) {
        if(outOfBounds(row, col, maxR, maxC)) {
            return 0;
        }
        if(blockedByObstacle(col, grid[row])) {
            return 0;
        }
        if(arrivedAtDest(row, col, maxR, maxC)) {
            return 1;
        }
        if(paths[row][col] != 0) {
            return paths[row][col];
        }
        int down = explore(row+1, col, maxR, maxC, grid, paths);
        int right = explore(row, col+1, maxR, maxC, grid, paths);
        paths[row][col] = down + right;
        return paths[row][col];
    }

    private boolean arrivedAtDest(int row, int col, int maxR, int maxC) {
        return row == maxR - 1 && col == maxC - 1;
    }

    private boolean blockedByObstacle(int col, int[] grid) {
        return grid[col] == 1;
    }

    private boolean outOfBounds(int row, int col, int maxR, int maxC) {
        return row >= maxR || col >= maxC;
    }
}