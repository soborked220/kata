package com.soborked.kata.puzzles.unsorted.review;

import java.util.HashMap;
import java.util.Map;

public class MapMajorityChecker implements MajorityChecker {

    @Override
    public boolean isMajority(int[] given, int target) {
        Map<Integer, Integer> counts = new HashMap<>();
        for(int num : given) {
            counts.merge(num, 1, Integer::sum);
        }
        return counts.get(target) > (given.length / 2);
    }
}
