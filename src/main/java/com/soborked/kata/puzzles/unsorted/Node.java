package com.soborked.kata.puzzles.unsorted;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Node {

  int val;
  Node left;
  Node right;
  Node next;

  public Node(int val) {
    this.val = val;
  }

  public Node(int val, Node left, Node right) {
    this.val = val;
    this.left = left;
    this.right = right;
  }

  public Node(int val, Node left, Node right, Node next) {
    this.val = val;
    this.left = left;
    this.right = right;
    this.next = next;
  }
}
