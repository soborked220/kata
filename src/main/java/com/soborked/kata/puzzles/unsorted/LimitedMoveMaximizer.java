package com.soborked.kata.puzzles.unsorted;

public class LimitedMoveMaximizer implements PileMaximizer {
    //if max is moves +1 down the pile, then just pop all from the pile
    //if max is moves down the pile, then take second max (note second max may be first condition above)
    //if max is < moves -1 down the pile, then pull until moves-1 and use last move to pop max onto pile
    @Override
    public int maximize(int[] nums, int moves) {
        if(moves == 0) {
            return nums[0];
        }
        if(moves % 2 == 1 && nums.length == 1) {
            return - 1;
        }
        int first = -1;
        int firstI = 0;
        int second = -1;
        for(int i=0; i<moves+1 && i<nums.length;i++) {
            if(nums[i] > first) {
                second = first;
                first = nums[i];
                firstI = i;
            } else if(nums[i] > second && nums[i] < first) {
                second = nums[i];
            }
        }
        if(maxIsTopOfPileAfterPullingEveryMove(moves, firstI)) {
            return first;
        }
        if(maxIsPulledFromTopOnLastMove(moves, firstI)) {
            return second;
        }

        return first;
    }

    private boolean maxIsPulledFromTopOnLastMove(int moves, int firstI) {
        return firstI == moves - 1;
    }

    private boolean maxIsTopOfPileAfterPullingEveryMove(int moves, int firstI) {
        return firstI == moves;
    }
}