package com.soborked.kata.puzzles.unsorted;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.reverseOrder;

public class SimpleWordFrequency implements WordCounter {
  public List<String> countFrequency(String[] words, int k) {
    Map<String, Integer> counts = new HashMap<>();
    for(int i = 0; i < words.length; i++) {
      String word = words[i];
      counts.putIfAbsent(word, 0);
      counts.put(word, counts.get(word) + 1);
    }
    return counts.entrySet().stream()
        .sorted((a,b)-> {
          int r = b.getValue().compareTo(a.getValue());
          if(r != 0) {
            return r;
          }
          return a.getKey().compareTo(b.getKey());
        })
        .limit(k)
        .map(Map.Entry::getKey)
        .collect(Collectors.toList());
  }
}
