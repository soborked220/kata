package com.soborked.kata.puzzles.unsorted.review;

public class PermutationGuesser {
    public int[] next(int[] given) {
        int last = given.length - 1;
        int leftEdge = firstDecreaseFromRight(given);
        if(leftEdge >= 0) {
            int newEdge = firstGreaterThanEdgeFromRight(given, leftEdge);
            swap(given, leftEdge, newEdge);
        }
        reverse(given, leftEdge+1, last);
        return given;
    }

    private int firstDecreaseFromRight(int[] given) {
        int edge = given.length - 2;
        while(edge >= 0 && given[edge] > given[edge+1]) {
            edge--;
        }
        return edge;
    }

    private int firstGreaterThanEdgeFromRight(int[] given, int leftEdge) {
        int newEdge = given.length - 1;
        while(newEdge>=0 && given[newEdge] <= given[leftEdge]) {
            newEdge--;
        }
        return newEdge;
    }

    private void swap(int[] given, int i, int j) {
        int temp = given[i];
        given[i] = given[j];
        given[j] = temp;
    }

    private void reverse(int[] given, int start, int end) {
        while(start < end) {
            swap(given, start++, end--);
        }
    }


}