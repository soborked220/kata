package com.soborked.kata.puzzles.unsorted;

public class LadderClimber {
  public int climb(int[] ladder, int reach) {
    int height = 0;
    int rungsAdded = 0;
    for (int i = 0; i < ladder.length; i++) {
      int heightOfNextRung = ladder[i];
      int distToNextRung = heightOfNextRung - height;
      int factor = distToNextRung / reach;
      int remaining = distToNextRung % reach;
      if (remaining == 0) {
        rungsAdded = rungsAdded + (factor - 1);
        height = heightOfNextRung;
      } else {
        rungsAdded = rungsAdded + factor;
        height = heightOfNextRung;
      }
    }
    return rungsAdded;
  }
}
