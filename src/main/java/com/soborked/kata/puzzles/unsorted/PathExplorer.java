package com.soborked.kata.puzzles.unsorted;

public interface PathExplorer {

    int explore(int rows, int columns);
}
