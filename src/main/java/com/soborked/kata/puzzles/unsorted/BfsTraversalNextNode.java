package com.soborked.kata.puzzles.unsorted;

public class BfsTraversalNextNode implements NodeConnector {
  @Override
  public Node connect(Node current) {
    Node answer = current;
    Node leftMostOfNextFloor;
    while (current != null) {
      leftMostOfNextFloor = current.left;
      while (current != null && current.left != null) {
        current.left.next = current.right;
        if (notAtEndOfThisLevel(current)) {
          current.right.next = current.next.left;
        }
        current = current.next; // move to sibling
      }
      current = leftMostOfNextFloor; // move to next floor
    }
    return answer;
  }

  private boolean notAtEndOfThisLevel(Node current) {
    return current.next != null;
  }
}
