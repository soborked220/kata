package com.soborked.kata.puzzles.unsorted;

import java.util.ArrayList;
import java.util.List;

public class Combination {

    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        combos(1, n, k, new ArrayList<>(), result);
        return result;
    }

    private void combos(int start, int n, int k, ArrayList<Integer> combo, List<List<Integer>> result) {
        if(k == 0) {
            result.add(new ArrayList<>(combo));
        } else {
            int lastNum = n - k + 1;
            for(int currentNum=start; currentNum<=lastNum;currentNum++) {
                combo.add(currentNum);
                combos(currentNum+1, n, k-1, combo, result);
                combo.remove(combo.size()-1);
            }
        }
    }


}
