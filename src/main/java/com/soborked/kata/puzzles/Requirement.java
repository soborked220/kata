package com.soborked.kata.puzzles;

public interface Requirement {

  String name();

  String behavior();
}
