package com.soborked.kata.puzzles.search.surroundedregion;

/**
 * Courtesy of https://leetcode.com/problems/surrounded-regions/
 *
 * <p>Given an m x n matrix board containing 'X' and 'O', capture all regions that are
 * 4-directionally surrounded by 'X'.
 *
 * <p>A region is captured by flipping all 'O's into 'X's in that surrounded region.
 *
 * <p>Example 1:
 *
 * <p>Input: board = [ ["X","X","X","X"], ["X","O","O","X"], ["X","X","O","X"], ["X","O","X","X"] ]
 * Output: [ ["X","X","X","X"], ["X","X","X","X"], ["X","X","X","X"], ["X","O","X","X"] ]
 *
 * <p>Explanation: Surrounded regions should not be on the border, which means that any 'O' on the
 * border of the board are not flipped to 'X'. Any 'O' that is not on the border and it is not
 * connected to an 'O' on the border will be flipped to 'X'. Two cells are connected if they are
 * adjacent cells connected horizontally or vertically.
 *
 * <p>Constraints m == board.length n == board[i].length 1 <= m, n <= 200 board[i][j] is 'X' or 'O'.
 */
public interface SurroundedRegion {

  /**
   * Constraints m == board.length n == board[i].length 1 <= m, n <= 200 board[i][j] is 'X' or 'O'.
   *
   * @param board of X's and O's
   * @return solved board
   */
  char[][] solve(char[][] board);
}
