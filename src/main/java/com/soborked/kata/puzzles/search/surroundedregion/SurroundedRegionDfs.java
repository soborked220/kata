package com.soborked.kata.puzzles.search.surroundedregion;

public class SurroundedRegionDfs implements SurroundedRegion {

  @Override
  public char[][] solve(char[][] board) {
    boolean[][] flagged = new boolean[board.length][board[0].length];
    flagOConnectedToBorder(board, flagged);
    convertUnflaggedO(board, flagged);
    return board;
  }

  private void flagOConnectedToBorder(char[][] board, boolean[][] flagged) {
    for (int row = 0; row < board.length; row++) {
      for (int column = 0; column < board[row].length; column++) {
        if (isBorderO(board, row, column)) {
          findConnections(board, flagged, row, column);
        }
      }
    }
  }

  private boolean isBorderO(char[][] board, int row, int column) {
    return isBorder(board, row, column) && isO(board, row, column);
  }

  private boolean isBorder(char[][] board, int row, int column) {
    int rows = board.length;
    int columns = board[0].length;
    return row == 0 || row == rows - 1 || column == 0 || column == columns - 1;
  }

  private void findConnections(char[][] board, boolean[][] flagged, int row, int column) {
    if (isPartOfBoard(board, row, column)) {
      if (!flagged[row][column]) {
        boolean isO = isO(board, row, column);
        flagged[row][column] = isO;

        if (isO) {
          findConnections(board, flagged, row - 1, column); // up
          findConnections(board, flagged, row, column + 1); // right
          findConnections(board, flagged, row + 1, column); // down
          findConnections(board, flagged, row, column - 1); // left
        }
      }
    }
  }

  private boolean isPartOfBoard(char[][] board, int row, int column) {
    int rows = board.length;
    int columns = board[0].length;
    return row >= 0 && row <= rows - 1 && column >= 0 && column <= columns - 1;
  }

  private boolean isO(char[][] board, int row, int column) {
    return board[row][column] == 'O';
  }

  private void convertUnflaggedO(char[][] board, boolean[][] connectedO) {
    for (int row = 0; row < board.length; row++) {
      for (int column = 0; column < board[row].length; column++) {
        if (!connectedO[row][column]) {
          board[row][column] = 'X';
        }
      }
    }
  }
}
