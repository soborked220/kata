package com.soborked.kata.puzzles;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UseCase<I, O> implements Verifiable<I, O>, Requirement {

  private final String name;
  private final String behavior;
  private final I input;
  private final O output;
  private final Criteria<O> criteria;
  private O actual;
  private final PrettyPrinter<I, O> printer = new PrettyPrinter<>();

  @Override
  public String name() {
    return name;
  }

  @Override
  public String behavior() {
    return behavior;
  }

  @Override
  public I given() {
    return input;
  }

  @Override
  public O expected() {
    return output;
  }

  @Override
  public Criteria<O> criteria() {
    return criteria;
  }

  @Override
  public boolean isSatisfiedBy(Solution solution) {
    actual = (O) solution.solve(given());
    return criteria().isMet(actual, expected());
  }

  @Override
  public String toString() {
    return printer.print(name(), behavior(), given(), expected(), actual);
  }
}
