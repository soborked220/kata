package com.soborked.kata.puzzles;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RunThrough<O> {

  private O output;
  private long time;

  public RunThrough(O output, long start, long end) {
    this.output = output;
    this.time = end - start;
  }
}
