package com.soborked.kata.puzzles.treetraversal.maxwater;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/*
You are given an integer array height of length n. There are n vertical lines drawn such that the two endpoints of the ith line are (i, 0) and (i, height[i]).

Find two lines that together with the x-axis form a container, such that the container contains the most water.

Return the maximum amount of water a container can store.

Notice that you may not slant the container.



Example 1:

Input: height = [1,8,6,2,5,4,8,3,7]
Output: 49
Explanation: The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In this case, the max area of water (blue section) the container can contain is 49.

Example 2:

Input: height = [1,1]
Output: 1



Constraints:

    n == height.length
    2 <= n <= 105
    0 <= height[i] <= 104


 */
public class MaxWaterPuzzle implements Puzzle<List<Integer>, Integer> {
  @Override
  public Stream<UseCase<List<Integer>, Integer>> examples() {
    return Stream.of(
        new UseCase<>("Simple", "Two lines", Arrays.asList(1, 1), 1, this::comparison),
        new UseCase<>(
            "Example",
            "Two lines",
            Arrays.asList(1, 8, 6, 2, 5, 4, 8, 3, 7),
            49,
            this::comparison));
  }

  private boolean comparison(Integer a, Integer e) {
    return a.equals(e);
  }

  @Override
  public Stream<Constraint<List<Integer>, Integer>> constraints() {
    return Stream.empty();
  }

  @Override
  public Stream<Performance<List<Integer>, Integer>> performance() {
    return Stream.empty();
  }
}
