package com.soborked.kata.puzzles.treetraversal.minsubarray;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/*
Given an array of positive integers nums and a positive integer target, return the minimal length of a contiguous subarray [numsl, numsl+1, ..., numsr-1, numsr] of which the sum is greater than or equal to target. If there is no such subarray, return 0 instead.



Example 1:

Input: target = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: The subarray [4,3] has the minimal length under the problem constraint.

Example 2:

Input: target = 4, nums = [1,4,4]
Output: 1

Example 3:

Input: target = 11, nums = [1,1,1,1,1,1,1,1]
Output: 0



Constraints:

    1 <= target <= 109
    1 <= nums.length <= 105
    1 <= nums[i] <= 105

 */
public class MinSubArrayPuzzle implements Puzzle<MinSubArrayInput, Integer> {
  @Override
  public Stream<UseCase<MinSubArrayInput, Integer>> examples() {
    return Stream.of(
        new UseCase<>(
            "One",
            "Return subarray size if target sum is met",
            new MinSubArrayInput(new int[] {1}, 1),
            1,
            this::comparison),
        new UseCase<>(
            "Zero",
            "Return 0 if target sum is not met",
            new MinSubArrayInput(new int[] {1}, 2),
            0,
            this::comparison),
        new UseCase<>(
            "Seven",
            "Return smallest subarray size if multiple arrays meet target",
            new MinSubArrayInput(new int[] {2, 3, 1, 2, 4, 3}, 7),
            2,
            this::comparison),
        new UseCase<>(
            "Four",
            "Return smallest subarray size if multiple arrays meet target",
            new MinSubArrayInput(new int[] {2, 2, 3, 4}, 4),
            1,
            this::comparison));
  }

  @Override
  public Stream<Constraint<MinSubArrayInput, Integer>> constraints() {
    return Stream.empty();
  }

  @Override
  public Stream<Performance<MinSubArrayInput, Integer>> performance() {
    return Stream.empty();
  }

  private boolean comparison(Integer a, Integer e) {
    return a.equals(e);
  }
}
