package com.soborked.kata.puzzles.treetraversal.gasstation;

import com.soborked.kata.puzzles.Solution;

public class GasStationManyPassJumpAheadSolution implements Solution<GasStationInput, Integer> {
  private static final int NEVER_FAILED = -2;

  @Override
  public Integer solve(GasStationInput given) {
    int[] gasAtStation = given.getGasAtStation();
    int[] costToGetToNext = given.getCostToGetToNext();
    int numberOfStations = gasAtStation.length;
    for (int candidate = 0; candidate < numberOfStations; ) {
      int failedAt = failsCircuitAt(candidate, gasAtStation, costToGetToNext);
      if (failedAt == NEVER_FAILED) {
        return candidate;
      } else {
        if (failedAt > candidate) {
          candidate = failedAt + 1;
        } else {
          candidate++;
        }
      }
    }
    return -1;
  }

  private int failsCircuitAt(int candidate, int[] gasAtStation, int[] costToGetToNext) {
    int stations = gasAtStation.length;
    int tank = 0;
    for (int current = candidate; current < stations + candidate + 1; current++) {
      int index = index(current, stations);
      int costToNext = costToGetToNext[index];
      tank = tank - costToNext + gasAtStation[index];
      if (tank < 0) {
        return current - 1;
      }
    }
    return -2;
  }

  private int index(int current, int total) {
    return current % total;
  }
}
