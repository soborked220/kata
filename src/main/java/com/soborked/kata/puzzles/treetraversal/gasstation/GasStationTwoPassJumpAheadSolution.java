package com.soborked.kata.puzzles.treetraversal.gasstation;

import com.soborked.kata.puzzles.Solution;

public class GasStationTwoPassJumpAheadSolution implements Solution<GasStationInput, Integer> {

  @Override
  public Integer solve(GasStationInput given) {
    int[] gasAtStation = given.getGasAtStation();
    int[] costToGetToNext = given.getCostToGetToNext();
    if (routeIsntViable(gasAtStation, costToGetToNext)) {
      return -1;
    }

    int stations = gasAtStation.length;
    int gasInTank = 0;
    int start = 0;
    for (int i = 0; i < stations; i++) {
      gasInTank += gasAtStation[i] - costToGetToNext[i];
      if (notAbleToReachNextStationGivenThisStart(gasInTank)) {
        start = moveStartToNextStationBecauseWeKnowAccumulateEnoughSurplus(i);
        gasInTank = 0;
      }
    }

    return start;
  }

  private boolean notAbleToReachNextStationGivenThisStart(int gasInTank) {
    return gasInTank < 0;
  }

  private int moveStartToNextStationBecauseWeKnowAccumulateEnoughSurplus(int current) {
    return current + 1;
  }

  private boolean routeIsntViable(int[] gasAtStation, int[] costToGetToNext) {
    int stations = gasAtStation.length;
    int sum = 0;
    for (int i = 0; i < stations; i++) {
      sum += gasAtStation[i] - costToGetToNext[i];
    }
    return sum < 0;
  }
}
