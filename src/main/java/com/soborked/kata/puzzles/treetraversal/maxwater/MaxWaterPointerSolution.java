package com.soborked.kata.puzzles.treetraversal.maxwater;

import com.soborked.kata.puzzles.Solution;

import java.util.*;

public class MaxWaterPointerSolution implements Solution<List<Integer>, Integer> {
  @Override
  public Integer solve(List<Integer> given) {
    int left = 0;
    int right = given.size() - 1;
    int max = 0;
    while (haveNotCheckedAllCandidates(left, right)) {
      int lh = given.get(left);
      int rh = given.get(right);
      int lowestHeight = Math.min(lh, rh);
      int width = right - left;
      int candidateArea = lowestHeight * width;
      max = Math.max(max, candidateArea);
      if (lh < rh) {
        left++;
      } else {
        right--;
      }
    }
    return max;
  }

  private boolean haveNotCheckedAllCandidates(int left, int right) {
    return left < right;
  }

  private boolean leftLowerThanRight(int lh, int rh) {
    return lh < rh;
  }
}
