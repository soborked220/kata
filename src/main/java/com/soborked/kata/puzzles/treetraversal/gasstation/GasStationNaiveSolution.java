package com.soborked.kata.puzzles.treetraversal.gasstation;

import com.soborked.kata.puzzles.Solution;

/**
 * This has horrible performance because we naively check every station starting from left to right.
 */
public class GasStationNaiveSolution implements Solution<GasStationInput, Integer> {

  @Override
  public Integer solve(GasStationInput given) {
    int[] gasAtStation = given.getGasAtStation();
    int[] costToGetToNext = given.getCostToGetToNext();
    int numberOfStations = gasAtStation.length;
    for (int candidate = 0; candidate < numberOfStations; candidate++) {
      if (canComplete(candidate, gasAtStation, costToGetToNext)) {
        return candidate;
      }
    }
    return -1;
  }

  private boolean canComplete(int start, int[] gasAtStation, int[] costToGetToNext) {
    int tank = gasAtStation[start];
    for (int next = start + 1; next < gasAtStation.length; next++) {
      int current = next - 1;
      int costToNext = costToGetToNext[current];
      if (tank < costToNext) {
        return false;
      }
      tank = tank - costToNext + gasAtStation[next];
    }
    for (int next = 0; next < start + 1; next++) {
      int current = next == 0 ? gasAtStation.length - 1 : next - 1;
      int costToNext = costToGetToNext[current];
      if (tank < costToNext) {
        return false;
      }
      tank = tank - costToNext + gasAtStation[next];
    }
    return true;
  }
}
