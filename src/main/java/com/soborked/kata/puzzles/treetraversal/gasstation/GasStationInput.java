package com.soborked.kata.puzzles.treetraversal.gasstation;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public class GasStationInput {

  private final int[] gasAtStation;
  private final int[] costToGetToNext;

  public String toString() {
    return Arrays.toString(gasAtStation) + "," + Arrays.toString(costToGetToNext);
  }
}
