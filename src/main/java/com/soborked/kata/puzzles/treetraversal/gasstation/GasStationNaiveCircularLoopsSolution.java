package com.soborked.kata.puzzles.treetraversal.gasstation;

import com.soborked.kata.puzzles.Solution;

public class GasStationNaiveCircularLoopsSolution implements Solution<GasStationInput, Integer> {

  @Override
  public Integer solve(GasStationInput given) {
    int[] gasAtStation = given.getGasAtStation();
    int[] costToGetToNext = given.getCostToGetToNext();
    int numberOfStations = gasAtStation.length;
    for (int candidate = 0; candidate < numberOfStations; candidate++) {
      if (canComplete(candidate, gasAtStation, costToGetToNext)) {
        return candidate;
      }
    }
    return -1;
  }

  private boolean canComplete(int candidate, int[] gasAtStation, int[] costToGetToNext) {
    int stations = gasAtStation.length;
    int tank = 0;
    for (int current = candidate; current < stations + candidate + 1; current++) {
      int index = index(current, stations);
      int costToNext = costToGetToNext[index];
      tank = tank - costToNext + gasAtStation[index];
      if (tank < 0) {
        return false;
      }
    }
    return true;
  }

  private int index(int current, int total) {
    return current % total;
  }
}
