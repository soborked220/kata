package com.soborked.kata.puzzles.treetraversal.maxwater;

import com.soborked.kata.puzzles.Solution;

import java.util.List;

public class MaxWaterBruteForceSolution implements Solution<List<Integer>, Integer> {
  @Override
  public Integer solve(List<Integer> given) {
    int maxArea = 0;
    for (int i = 0; i < given.size(); i++) {
      for (int j = i; j < given.size(); j++) {
        int lh = given.get(i);
        int rh = given.get(j);
        int denominator = Math.min(lh, rh);
        int area = denominator * (j - i);
        if (area > maxArea) {
          maxArea = area;
        }
      }
    }
    return maxArea;
  }
}
