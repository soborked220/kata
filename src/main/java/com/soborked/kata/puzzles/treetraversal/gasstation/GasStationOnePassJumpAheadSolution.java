package com.soborked.kata.puzzles.treetraversal.gasstation;

import com.soborked.kata.puzzles.Solution;

public class GasStationOnePassJumpAheadSolution implements Solution<GasStationInput, Integer> {
  @Override
  public Integer solve(GasStationInput given) {
    int[] gasAtStation = given.getGasAtStation();
    int[] costToGetToNext = given.getCostToGetToNext();
    int stations = gasAtStation.length;
    int surplusGas = 0;
    int currentGasInTank = 0;
    int start = 0;
    for (int i = 0; i < stations; i++) {
      int diff = gasAtStation[i] - costToGetToNext[i];
      currentGasInTank += diff;
      surplusGas += diff;
      if (notAbleToReachNextStationGivenThisStart(currentGasInTank)) {
        start = moveStartToNextStationBecauseWeKnowAccumulateEnoughSurplus(i);
        currentGasInTank = 0;
      }
    }
    return routeIsViable(surplusGas) ? start : -1;
  }

  private boolean notAbleToReachNextStationGivenThisStart(int gasInTank) {
    return gasInTank < 0;
  }

  private int moveStartToNextStationBecauseWeKnowAccumulateEnoughSurplus(int current) {
    return current + 1;
  }

  private boolean routeIsViable(int surplusGas) {
    return surplusGas >= 0;
  }
}
