package com.soborked.kata.puzzles.treetraversal.minsubarray;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
public class MinSubArrayInput {

  private final int[] numbers;
  private final int target;
}
