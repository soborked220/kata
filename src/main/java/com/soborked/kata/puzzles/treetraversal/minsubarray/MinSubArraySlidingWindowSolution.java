package com.soborked.kata.puzzles.treetraversal.minsubarray;

import com.soborked.kata.puzzles.Solution;

public class MinSubArraySlidingWindowSolution implements Solution<MinSubArrayInput, Integer> {

  private static final int INDEX_OFFSET = 1;
  private static final int MAX_INPUT = 110;

  @Override
  public Integer solve(MinSubArrayInput given) {
    int target = given.getTarget();
    int[] numbers = given.getNumbers();
    int left = 0;
    int right = 0;
    int sum = 0;
    int minSize = 110;
    for (; right < numbers.length; right++) {
      sum += numbers[right];
      while (left <= right && sum >= target) {
        minSize = Math.min((right - left + INDEX_OFFSET), minSize);
        sum -= numbers[left];
        left++;
      }
    }
    return minSize == MAX_INPUT ? 0 : minSize;
  }
}
