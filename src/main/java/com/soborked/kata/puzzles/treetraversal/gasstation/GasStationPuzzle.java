package com.soborked.kata.puzzles.treetraversal.gasstation;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.stream.Stream;

/*
   There are n gas stations along a circular route, where the amount of gas at the ith station is gas[i].

   You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from the ith station to its
   next (i + 1)th station.
   You begin the journey with an empty tank at one of the gas stations.

   Given two integer arrays gas and cost, return the starting gas station's index if you can travel around the circuit
   once in the clockwise direction, otherwise return -1. If there exists a solution, it is guaranteed to be unique

   Example 1:

   Input: gas = [1,2,3,4,5], cost = [3,4,5,1,2]
   Output: 3
   Explanation:
   Start at station 3 (index 3) and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
   Travel to station 4. Your tank = 4 - 1 + 5 = 8
   Travel to station 0. Your tank = 8 - 2 + 1 = 7
   Travel to station 1. Your tank = 7 - 3 + 2 = 6
   Travel to station 2. Your tank = 6 - 4 + 3 = 5
   Travel to station 3. The cost is 5. Your gas is just enough to travel back to station 3.
   Therefore, return 3 as the starting index.

   Example 2:

   Input: gas = [2,3,4], cost = [3,4,3]
   Output: -1
   Explanation:
   You can't start at station 0 or 1, as there is not enough gas to travel to the next station.
   Let's start at station 2 and fill up with 4 unit of gas. Your tank = 0 + 4 = 4
   Travel to station 0. Your tank = 4 - 3 + 2 = 3
   Travel to station 1. Your tank = 3 - 3 + 3 = 3
   You cannot travel back to station 2, as it requires 4 unit of gas but you only have 3.
   Therefore, you can't travel around the circuit once no matter where you start.

   Constraints:

       gas.length == n
       cost.length == n
       1 <= n <= 105
       0 <= gas[i], cost[i] <= 104

*/
public class GasStationPuzzle implements Puzzle<GasStationInput, Integer> {
  @Override
  public Stream<UseCase<GasStationInput, Integer>> examples() {
    return Stream.of(
        new UseCase<>(
            "Viable",
            "Return start index position if route is viable.",
            new GasStationInput(new int[] {1, 2, 3, 4, 5}, new int[] {3, 4, 5, 1, 2}),
            3,
            this::comparison),
        new UseCase<>(
            "Not Viable",
            "Return -1 if route is not viable.",
            new GasStationInput(new int[] {2, 3, 4}, new int[] {3, 4, 3}),
            -1,
            this::comparison),
        new UseCase<>(
            "Performance",
            "Return route position even for very long routes.",
            new GasStationInput(gasForMaxCase(), costForMaxCase()),
            99999,
            this::comparison));
  }

  private boolean comparison(Integer a, Integer e) {
    return a.equals(e);
  }

  @Override
  public Stream<Constraint<GasStationInput, Integer>> constraints() {
    return Stream.empty();
  }

  @Override
  public Stream<Performance<GasStationInput, Integer>> performance() {
    return Stream.empty();
  }

  private static final int HUNDRED_K = 100000;

  /*
     gas => [0,0,0...,0,0,2]
     cost => [0,0,0...,0,1,0]
  */
  private int[] gasForMaxCase() {
    int[] values = new int[HUNDRED_K];
    for (int i = 0; i < HUNDRED_K; i++) {
      if (i == HUNDRED_K - 1) {
        values[i] = 2;
      } else {
        values[i] = 0;
      }
    }
    return values;
  }

  private int[] costForMaxCase() {
    int[] values = new int[HUNDRED_K];
    for (int i = 0; i < HUNDRED_K; i++) {
      if (i == HUNDRED_K - 2) {
        values[i] = 1;
      } else {
        values[i] = 0;
      }
    }
    return values;
  }
}
