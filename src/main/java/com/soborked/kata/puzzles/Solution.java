package com.soborked.kata.puzzles;

public interface Solution<I, O> {

  O solve(I given);
}
