package com.soborked.kata.puzzles.sorting.nodeswap;

import com.soborked.kata.puzzles.Constraint;
import com.soborked.kata.puzzles.Performance;
import com.soborked.kata.puzzles.Puzzle;
import com.soborked.kata.puzzles.UseCase;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

/**
 * Given a linked list, swap every two adjacent nodes and return its head. You must solve the
 * problem without modifying the values in the list's nodes (i.e., only nodes themselves may be
 * changed.)
 *
 * <p>Example 1:
 *
 * <p>Input: head = [1,2,3,4] Output: [2,1,4,3]
 *
 * <p>Example 2:
 *
 * <p>Input: head = [] Output: []
 *
 * <p>Example 3:
 *
 * <p>Input: head = [1] Output: [1]
 */
public class NodeSwapPuzzle implements Puzzle<ListNode, ListNode> {
  @Override
  public Stream<UseCase<ListNode, ListNode>> examples() {
    return Stream.of(
        new UseCase<>(
            "Zeroes",
            "Zero is a valid sum result",
            new ListNode(new LinkedList<>(asList(0))),
            new ListNode(new LinkedList<>(asList(0))),
            this::comparison));
  }

  @Override
  public Stream<Constraint<ListNode, ListNode>> constraints() {
    return null;
  }

  @Override
  public Stream<Performance<ListNode, ListNode>> performance() {
    return null;
  }

  private boolean comparison(ListNode actual, ListNode expected) {
    List<Integer> a = actual.unpack();
    List<Integer> e = expected.unpack();
    return a.equals(e);
  }
}
