package com.soborked.kata.examples.ratelimiters.runner;

import com.soborked.kata.examples.ratelimiters.runner.Attempt;
import com.soborked.kata.examples.ratelimiters.runner.Uniform;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniformLoadTest {

    private static final long PROCESSING_TIME = 20L;

    @Test
    public void should_elapseTheFullDuration_whenUniformLoaded() {
        long duration = 1000;
        Uniform uniform = new Uniform(1, duration);
        long start = System.currentTimeMillis();
        uniform.make(() -> true, 1);
        long stopped = System.currentTimeMillis();
        long elapsed = stopped - start;
        assertTrue(1000 < elapsed && elapsed < duration + PROCESSING_TIME, "Elapsed: " + elapsed);
    }

    @Test
    public void should_loadExecutionsPeriodically_whenUniformLoaded() {
        long duration = 1000;
        int capacity = 5;
        Uniform uniform = new Uniform(capacity, duration);
        long start = System.currentTimeMillis();
        List<Attempt> attempts = uniform.make(() -> true, 5);
        List<Long> elapsed = attempts.stream()
                .map(Attempt::getTime)
                .map(attempted -> attempted - start)
                .collect(toList());
        assertTrue(spacedAppropriately(elapsed, (duration/capacity)), "Elapsed: " + elapsed);
    }

    private boolean spacedAppropriately(List<Long> elapsed, long spacing) {
        long timeSpentProcessing = 0;
        for(int i = elapsed.size() - 1; i > 0; i--) {
            long latest = elapsed.get(i);
            long earlier = elapsed.get(i-1);
            timeSpentProcessing += (latest - earlier - spacing);
        }
        return timeSpentProcessing < (2 * PROCESSING_TIME);
    }

    @Test
    public void should_stopAttempts_whenNoneRemaining() {
        Uniform uniform = new Uniform(5, 1000);
        List<Attempt> attempts = uniform.make(() -> true, 1);
        assertEquals(1, attempts.size());
    }
}
