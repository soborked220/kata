package com.soborked.kata.examples.ratelimiters.runner;

import com.soborked.kata.examples.ratelimiters.runner.Attempt;
import com.soborked.kata.examples.ratelimiters.runner.Back;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BackLoadTest {

    private static final long PROCESSING_TIME = 20L;

    @Test
    public void should_elapseTheFullDuration_whenBackLoaded() {
        long duration = 1000;
        Back back = new Back(1, duration);
        long start = System.currentTimeMillis();
        back.make(() -> true, 1);
        long stopped = System.currentTimeMillis();
        long elapsed = stopped - start;
        assertTrue(1000 < elapsed && elapsed < duration + PROCESSING_TIME, "Elapsed: " + elapsed);
    }

    @Test
    public void should_loadAllExecutionsAtEnd_whenBackLoaded() {
        long duration = 1000;
        Back back = new Back(5, duration);
        long start = System.currentTimeMillis();
        List<Attempt> attempts = back.make(() -> true, 5);
        long firstLoaded = attempts.stream().map(Attempt::getTime).min(Long::compare).orElse(duration);
        long elapsed = firstLoaded - start;
        assertTrue(elapsed > duration, "Elapsed: " + elapsed);
    }

    @Test
    public void should_stopAttempts_whenNoneRemaining() {
        Back back = new Back(5, 1000);
        List<Attempt> attempts = back.make(() -> true, 1);
        assertEquals(1, attempts.size());
    }
}
