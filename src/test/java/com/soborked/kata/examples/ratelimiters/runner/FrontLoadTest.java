package com.soborked.kata.examples.ratelimiters.runner;

import com.soborked.kata.examples.ratelimiters.runner.Attempt;
import com.soborked.kata.examples.ratelimiters.runner.Front;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FrontLoadTest {

    private static final long PROCESSING_TIME = 20;

    @Test
    public void should_elapseTheFullDuration_whenFrontLoaded() {
        long duration = 1000;
        Front front = new Front(1, duration);
        long start = System.currentTimeMillis();
        front.make(() -> true, 1);
        long stopped = System.currentTimeMillis();
        long elapsed = stopped - start;
        assertTrue(1000 < elapsed && elapsed < duration + PROCESSING_TIME, "Elapsed: " + elapsed);
    }

    @Test
    public void should_loadAllExecutionsAtStart_whenFrontLoaded() {
        long duration = 1000;
        Front front = new Front(5, duration);
        long start = System.currentTimeMillis();
        List<Attempt> attempts = front.make(() -> true, 5);
        long lastLoaded = attempts.stream().map(Attempt::getTime).max(Long::compare).orElse(0L);
        assertTrue(lastLoaded < start + PROCESSING_TIME);
    }

    @Test
    public void should_stopAttempts_whenNoneRemaining() {
        Front front = new Front(5, 1000);
        List<Attempt> attempts = front.make(() -> true, 1);
        assertEquals(1, attempts.size());
    }

}