package com.soborked.kata.examples.ratelimiters.runner;

import com.soborked.kata.examples.ratelimiters.runner.Attempt;
import com.soborked.kata.examples.ratelimiters.runner.Frame;
import com.soborked.kata.examples.ratelimiters.runner.Timeline;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

public class TimelineTest {

    @Test
    public void should_returnZeroIndexFrames_whenGettingFrames() {
        long start = System.currentTimeMillis();
        List<Attempt> attempts = attempts(start);
        long stop = start + 3000;

        Timeline line = new Timeline(start, stop, 1000, attempts);

        List<Frame> frames = line.getFrames();
        assertEquals(frames.size(), 3);
        assertEquals(2, frames.get(0).getAttempts().size());
        assertEquals(2, frames.get(1).getAttempts().size());
        assertEquals(2, frames.get(2).getAttempts().size());
    }

    @Test
    public void should_returnOffsetFrames_whenGettingOffsetFrames() {
        long start = System.currentTimeMillis();
        List<Attempt> attempts = attempts(start);
        long stop = start + 3000;

        Timeline line = new Timeline(start, stop, 1000, attempts);

        List<Frame> frames = line.getOffsetFrames();
        assertEquals(2, frames.size());
        assertEquals(3, frames.get(0).getAttempts().size());
        assertEquals(2, frames.get(1).getAttempts().size());
    }

    @Test
    public void should_disagreeOnSize_whenFrameListsCompareAndAttemptsOccurredWithinLastHalfFrame() {
        long start = System.currentTimeMillis();
        List<Attempt> attempts = attempts(start);
        long stop = start + 3000;

        Timeline line = new Timeline(start, stop, 1000, attempts);
        List<Frame> zeroIndexed = line.getFrames();
        List<Frame> offset = line.getOffsetFrames();
        assertNotEquals(zeroIndexed.size(), offset.size());
        assertTrue(zeroIndexed.size() > offset.size());
    }

    private List<Attempt> attempts(long start) {
        return asList(
                new Attempt(true, start + 506),
                new Attempt(true, start + 809),
                new Attempt(true, start + 1216),
                new Attempt(true, start + 1520),
                new Attempt(true, start + 2135),
                new Attempt(true, start + 2640)
        );
    }

}