package com.soborked.kata.examples.ratelimiters;

import com.soborked.kata.examples.ratelimiters.runner.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.soborked.kata.examples.ratelimiters.runner.BatchSummary.BATCH_VIEW;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class SlidingWindowLogTest {

    private static final String AVG = "Average Success Per Second";
    private static final String MAX = "Frame with Highest Success Per Second";

    @ParameterizedTest
    @MethodSource("noThrottling")
    public void should_notThrottle_whenLimitNotNecessary(Scenario given) {
        RateLimiter limiter = new LooseSlidingWindowLimiter(given.rps());
        BatchSummary summary = given.traffic().passesThrough(limiter);
        System.out.println(summary.report());

        double avgsps = summary.averageSPSFrom(BATCH_VIEW);
        double frameWithHighestSps = summary.highestSPSFrom(BATCH_VIEW).successes();
        assertThat(AVG, avgsps, is(lessThanOrEqualTo(given.allowedAvgRPS())));
        assertThat(MAX, frameWithHighestSps, is(lessThanOrEqualTo(given.allowedMaxRPS())));
    }

    private static Stream<Arguments> noThrottling() {
        return Stream.of(
                Arguments.of(
                        new Scenario("Low traffic", 5, 5.0, 5.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 10, new Uniform(10, 3000))
                                )
                        )
                ),
                Arguments.of(
                        new Scenario("Two Clients", 5, 5.0, 5.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 6, new Uniform(6, 3000)),
                                        new LoadSequence("Uniform", 6, new Uniform(6, 3000))
                                )
                        )
                )
        );
    }

    @ParameterizedTest
    @MethodSource("uniform")
    public void should_throttleToEnforceLimit_whenFacingUniformlyDistributedTraffic(Scenario given) {
        RateLimiter limiter = new LooseSlidingWindowLimiter(given.rps());
        BatchSummary summary = given.traffic().passesThrough(limiter);
        System.out.println(summary.report());

        double avgsps = summary.averageSPSFrom(BATCH_VIEW);
        double frameWithHighestSps = summary.highestSPSFrom(BATCH_VIEW).successes();
        assertThat(AVG, avgsps, is(lessThanOrEqualTo(given.allowedAvgRPS())));
        assertThat(MAX, frameWithHighestSps, is(lessThanOrEqualTo(given.allowedMaxRPS())));
    }

    private static Stream<Arguments> uniform() {
        return Stream.of(
                Arguments.of(
                        new Scenario("Edge of limit", 5, 5.0, 5.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 15, new Uniform(15, 3000))
                                )
                        )
                ),
                Arguments.of(
                        new Scenario("Over limit", 5, 5.0, 5.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 30, new Uniform(30, 3000))
                                )
                        )
                ),
                Arguments.of(
                        new Scenario("Two Clients", 5, 5.0, 5.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 15, new Uniform(15, 3000)),
                                        new LoadSequence("Uniform", 15, new Uniform(15, 3000))
                                )
                        )
                ),
                Arguments.of(
                        new Scenario("At scale", 100, 107.0, 100.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 400, new Uniform(400, 3000))
                                )
                        )
                )
        );
    }

    @ParameterizedTest
    @MethodSource("bursts")
    public void should_throttleToEnforceLimit_whenFacingBurstsOfTraffic(Scenario given) {
        RateLimiter limiter = new LooseSlidingWindowLimiter(given.rps());
        BatchSummary summary = given.traffic().passesThrough(limiter);
        System.out.println(summary.report());

        double avgsps = summary.averageSPSFrom(BATCH_VIEW);
        double frameWithHighestSps = summary.highestSPSFrom(BATCH_VIEW).successes();
        assertThat(AVG, avgsps, is(lessThanOrEqualTo(given.allowedAvgRPS())));
        assertThat(MAX, frameWithHighestSps, is(lessThanOrEqualTo(given.allowedMaxRPS())));
    }

    private static Stream<Arguments> bursts() {
        return Stream.of(
                Arguments.of(
                        new Scenario("Edge of limit", 5, 5.0, 5.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 10,
                                                new Middle(5, 1000),
                                                new Back(2, 1000),
                                                new Front(3, 1000)
                                        )
                                )
                        )
                ),
                Arguments.of(
                        new Scenario("Over limit", 5, 5.0, 5.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 20,
                                                new Middle(10, 1000),
                                                new Back(5, 1000),
                                                new Front(5, 1000)
                                        )
                                )
                        )
                ),
                Arguments.of(
                        new Scenario("Two Synced Clients", 5, 5.0, 5.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 4, new Middle(4, 1000)),
                                        new LoadSequence("Uniform", 6, new Middle(6, 1000))
                                )
                        )
                ),
                Arguments.of(
                        new Scenario("At scale", 100, 100, 100.0,
                                new RequestLoader(
                                        new LoadSequence("Uniform", 400,
                                                new Middle(134, 1000),
                                                new Back(133, 1000),
                                                new Front(133, 1000)
                                        )
                                )
                        )
                )
        );
    }


}