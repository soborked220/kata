package com.soborked.kata.examples.ratelimiters.runner;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MiddleLoadTest {

    private static final long PROCESSING_TIME = 20L;

    @Test
    public void should_elapseTheFullDuration_whenMiddleLoaded() {
        long duration = 1000;
        Middle middle = new Middle(1, duration);
        long start = System.currentTimeMillis();
        middle.make(() -> true, 1);
        long stopped = System.currentTimeMillis();
        long elapsed = stopped - start;
        assertThat(elapsed, is(greaterThan(1000L)));
        assertThat(elapsed, is(lessThan(duration + PROCESSING_TIME)));
    }

    @Test
    public void should_loadAllExecutionsInMiddle_whenMiddleLoaded() {
        long duration = 1000;
        Middle middle = new Middle(5, duration);
        long start = System.currentTimeMillis();
        List<Attempt> attempts = middle.make(() -> true, 5);
        long firstLoaded = attempts.stream().map(Attempt::getTime).min(Long::compare).orElse(duration);
        long lastLoaded = attempts.stream().map(Attempt::getTime).max(Long::compare).orElse(0L);
        long timeToFirstLoad = firstLoaded - start;
        long timeToLastLoad = lastLoaded - start;
        assertThat(timeToFirstLoad, is(greaterThan(duration/2)));
        assertThat(timeToLastLoad, is(lessThan(duration/2 + PROCESSING_TIME)));
    }

    @Test
    public void should_stopAttempts_whenNoneRemaining() {
        Middle middle = new Middle(5, 1000);
        List<Attempt> attempts = middle.make(() -> true, 1);
        assertThat(attempts.size(), is(equalTo(1)));
    }
}
