package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Given numbers in ascending order, return all combinations of the numbers that add up to the target.
 * You can use a number multiple times.
 * The input number array contains no duplicates
 */
public class CombinationSumTest {

    private CombinationFinder cut = new IntegerCombinationFinder();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnCombinations_whichSumUpToTarget(int[] given, int target, List<List<Integer>> expected) {
      List<List<Integer>> actual = cut.combinations(given, target);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[] {2,3,6,7}, 7,
                      Arrays.asList(Arrays.asList(2,2,3), Arrays.asList(7))
               )

      );
    }

}
