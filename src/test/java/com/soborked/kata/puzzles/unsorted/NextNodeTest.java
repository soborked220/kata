package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.*;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * You are given a perfect binary tree where all leaves are on the same level, and every parent has two children. The binary tree has the following definition:
 *
 * struct Node {
 *   int val;
 *   Node *left;
 *   Node *right;
 *   Node *next;
 * }
 *
 * Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.
 *
 * Initially, all next pointers are set to NULL.
 *
 *
 *
 * Example 1:
 *
 * Input: root = [1,2,3,4,5,6,7]
 * Output: [1,#,2,3,#,4,5,6,7,#]
 * Explanation: Given the above perfect binary tree (Figure A), your function should populate each next pointer to point to its next right node, just like in Figure B. The serialized output is in level order as connected by the next pointers, with '#' signifying the end of each level.
 *
 * Example 2:
 *
 * Input: root = []
 * Output: []
 *
 *
 *
 * Constraints:
 *
 *     The number of nodes in the tree is in the range [0, 212 - 1].
 *     -1000 <= Node.val <= 1000
 *
 *
 *
 * Follow-up:
 *
 *     You may only use constant extra space.
 *     The recursive approach is fine. You may assume implicit stack space does not count as extra space for this problem.
 */
public class NextNodeTest {

    private List<NodeConnector> cut = Arrays.asList(
            new BfsQueueNextNode(),
            new BfsTraversalNextNode(),
            new NextNodePractice()
    );
    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnRootWithNextPointersPopulated_whenGivenPerfectTree(Node given, Node expected) {
        cut.forEach(connector -> {
            Node actual = connector.connect(given);
            MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
        });
    }

    private static final Node SEVEN = new Node(7);
    private static final Node SIX = new Node(6, null, null, SEVEN);
    private static final Node FIVE = new Node(5, null, null, SIX);
    private static final Node FOUR = new Node(4, null, null, FIVE);
    private static final Node THREE = new Node(3, SIX, SEVEN);
    private static final Node TWO = new Node(2, FOUR, FIVE, THREE);
    private static final Node ONE = new Node(1, TWO, THREE);

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(
                      new Node(1,
                              new Node(2,
                                      new Node(4),
                                      new Node(5)
                              ),
                              new Node(3,
                                      new Node(6),
                                      new Node(7)
                              )
                      ),
                      ONE
              )
      );
    }
    
}
