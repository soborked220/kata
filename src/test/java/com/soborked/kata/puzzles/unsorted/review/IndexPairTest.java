package com.soborked.kata.puzzles.unsorted.review;

import com.soborked.kata.puzzles.unsorted.PairCounter;
import com.soborked.kata.puzzles.unsorted.review.MaxPairCounter;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * Return the total number of index pairs such that given[i] = given[j] and i != j
 */
public class IndexPairTest {

    private PairCounter cut = new MaxPairCounter();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnCountOfPairs_whenGivenArrayOfInts(int[] given, int expected) {
      int actual = cut.countPairs(given);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[]{2,3,1,2,3,1,4}, 3),
              Arguments.of(new int[]{2,2}, 1),
              Arguments.of(new int[]{2,1}, 0),
              Arguments.of(new int[]{2,2,1,1}, 2),
              Arguments.of(new int[]{1,2,2,1}, 2)
      );
    }
}
