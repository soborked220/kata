package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Given a sorted array of size n and a target number, determine if the target
 * appears in the array enough times to constitute the majority. Majority is defined as greater than n / 2 occurrences.
 */
public class MajorityElementTest {

    private List<MajorityChecker> cut = Arrays.asList(
            new MapMajorityChecker(),
            new BinaryMajorityChecker(),
            new HalfBinaryMajorityChecker(),
            new MooreChecker()
    );

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnTrue_whenNumberIsMajorityOfGivenArray(int[] given, int target, boolean expected) {
      cut.forEach(checker -> {
          boolean actual = checker.isMajority(given, target);
        MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
      });
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[]{1,2,3}, 2, false),
              Arguments.of(new int[]{1,2,2,3}, 2, false),
              Arguments.of(new int[]{1,2,3,4,5}, 2, false),
              Arguments.of(new int[]{1,2,3,4,5}, 4, false),
              Arguments.of(new int[]{1,1,1,2,3}, 2, false),
              Arguments.of(new int[]{1,2,3,3,3}, 2, false),
              Arguments.of(new int[]{1,2,2,2,4}, 2, true),
              Arguments.of(new int[]{2,2,2,4}, 2, true),
              /*Arguments.of(new int[]{2,1,1,1,2,2,2}, 2, true),
              Arguments.of(new int[]{2,1,1,1,2,2,2,3}, 2, false),*/
              Arguments.of(new int[]{1,2,2,2,2,2,2,4}, 2, true)
      );
    }
}
