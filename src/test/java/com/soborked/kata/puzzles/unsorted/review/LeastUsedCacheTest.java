package com.soborked.kata.puzzles.unsorted.review;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.is;



public class LeastUsedCacheTest {

    private LeastRecentlyUsedCache cut;



    @Test
    public void should_returnValue_whenKeyPreviouslyAdded() {
        cut = new LeastCache(2);
        cut.put("a", 1);
        assertThat(cut.get("a"), is(equalTo(1)));
    }

    @Test
    public void should_returnUpdatedValue_whenKeysValueIsChanged() {
        cut = new LeastCache(2);
        cut.put("a", 1);
        cut.put("a", 2);
        assertThat(cut.get("a"), is(equalTo(2)));
    }

    @Test
    public void should_removeOldestAdded_whenNewlyAddedEntryExceedsLimit() {
        cut = new LeastCache(2);
        cut.put("a", 1);
        cut.put("b", 2);
        cut.put("c", 3);
        assertThat(cut.get("a"), is(equalTo(null)));
        assertThat(cut.get("b"), is(equalTo(2)));
        assertThat(cut.get("c"), is(equalTo(3)));
    }

    @Test
    public void should_countUpdatesAsAnAdd_whenPrioritizingRemoval() {
        cut = new LeastCache(2);
        cut.put("a", 1);
        cut.put("b", 2);
        cut.put("a", 0);
        cut.put("c", 3);
        assertThat(cut.get("a"), is(equalTo(0)));
        assertThat(cut.get("c"), is(equalTo(3)));
        assertThat(cut.get("b"), is(equalTo(null)));
    }

    @Test
    public void should_countReadsAsAnAdd_whenPrioritizingRemoval() {
        cut = new LeastCache(2);
        cut.put("a", 1);
        cut.put("b", 2);
        cut.get("a");
        cut.put("c", 3);
        assertThat(cut.get("a"), is(equalTo(1)));
        assertThat(cut.get("c"), is(equalTo(3)));
        assertThat(cut.get("b"), is(equalTo(null)));
    }

    @Test
    public void should_ignoreTotalCountOfReads_whenPrioritizingRemoval() {
        cut = new LeastCache(2);
        cut.put("a", 1);
        cut.put("b", 2);
        cut.get("a");
        cut.get("a");
        cut.get("a");
        cut.get("b");
        cut.put("c", 3);
        assertThat(cut.get("a"), is(equalTo(null)));
        assertThat(cut.get("c"), is(equalTo(3)));
        assertThat(cut.get("b"), is(equalTo(2)));
    }

}