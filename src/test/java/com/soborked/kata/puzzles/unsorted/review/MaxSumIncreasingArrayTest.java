package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * Given an array of positive numbers, maximize the sum of an increasing subsequence.
 * Note that a subsequence can be created by deleting any number of elements in the given array from any position.
 * But the order of the elements in the given array cannot change.
 * Given: [1,0,2,0,3]
 * A valid subsequence is: [1,2,3]
 * Given: [1,0,3,0,2]
 * A valid subsequence is: [1,3], [1,3,2] is invalid because it does not increase when moving from 3 to 2
 */
public class MaxSumIncreasingArrayTest {

    MaxSubsequence cut = new MaxSubsequence();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnMaxSum_whenGivenArrayContainingIncreasingSubsets(int[] given, int expected) {
      int actual = cut.maxSum(given);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[]{2,5,8,3,4,6}, 15),
              Arguments.of(new int[]{2,5,3,8,4,6}, 15)
      );
    }
}
