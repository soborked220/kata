package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.EfficientWordFrequency;
import com.soborked.kata.puzzles.unsorted.SimpleWordFrequency;
import com.soborked.kata.puzzles.unsorted.WordCounter;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

/**
 * Given an array of strings words and an integer k, return the k most frequent strings.
 *
 * <p>Return the answer sorted by the frequency from highest to lowest. Sort the words with the same
 * frequency by their lexicographical order.
 *
 * <p>Example 1:
 *
 * <p>Input: words = ["i","love","leetcode","i","love","coding"], k = 2 Output: ["i","love"]
 * Explanation: "i" and "love" are the two most frequent words. Note that "i" comes before "love"
 * due to a lower alphabetical order.
 *
 * <p>Example 2:
 *
 * <p>Input: words = ["the","day","is","sunny","the","the","the","sunny","is","is"], k = 4 Output:
 * ["the","is","sunny","day"] Explanation: "the", "is", "sunny" and "day" are the four most frequent
 * words, with the number of occurrence being 4, 3, 2 and 1 respectively.
 *
 * <p>Constraints:
 *
 * <p>1 <= words.length <= 500 1 <= words[i].length <= 10 words[i] consists of lowercase English
 * letters. k is in the range [1, The number of unique words[i]]
 *
 * <p>Follow-up: Could you solve it in O(n log(k)) time and O(n) extra space?
 */
public class WordFrequencyTest {

    private List<WordCounter> cut = Arrays.asList(
            new SimpleWordFrequency(),
            new EfficientWordFrequency()
    );


    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnKMostFrequentWords_whenGivenListOfWords(String[] words, int k, List<String> expected) {
      cut.forEach(counter -> {
          List<String> actual = counter.countFrequency(words, k);
          MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
      });
    }

    private static Stream<Arguments> examples() {
    return Stream.of(
        Arguments.of(new String[] {"i", "love", "leetcode", "i", "love", "coding"}, 2,
                asList("i", "love")),
        Arguments.of(new String[] {"the","day","is","sunny","the","the","the","sunny","is","is"}, 4,
                asList("the","is","sunny","day"))
    );
    }
}
