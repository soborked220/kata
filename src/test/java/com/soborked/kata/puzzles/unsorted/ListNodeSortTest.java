package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.ListNode;

import com.soborked.kata.puzzles.unsorted.ListNodeSort;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class ListNodeSortTest {

    private ListNodeSort cut = new ListNodeSort();

    @ParameterizedTest
    @MethodSource("scenarios")
    public void should_returnSortedList_whenGivenUnsortedList(ListNode given, List<Integer> expected) {
        ListNode actual = cut.sortList(given);
        assertThat(actual.unpack(), is(equalTo(expected)));
    }

    private static Stream<Arguments> scenarios() {
      return Stream.of(
              Arguments.of(new ListNode(
                      new LinkedList<>(asList(4,2,1,3))), asList(1,2,3,4)),
              Arguments.of(new ListNode(
                      new LinkedList<>(asList(-1,5,3,4,0))), asList(-1,0,3,4,5))
      );
    }
}
