package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class AvlTreeTest {

  private AvlTree cut = new AvlTree();

  @ParameterizedTest
  @MethodSource("searchExamples")
  public void should_returnNode_whenPreviouslyAdded(int[] given, int expected) {
    for( int key : given) {
      cut.add(key);
    }
    assertThat(cut.search(expected).key, is(equalTo(expected)));
  }

  private static Stream<Arguments> searchExamples() {
    return Stream.of(
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 1),
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 2),
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 3),
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 4),
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 5),
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 6),
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 7),
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 8),
            Arguments.of(new int[] {6,3,2,1,4,5,8,7,9}, 9)
    );
  }

  @Test
  public void should_removeNode_whenTargetIsLeaf() {
    int[] given = new int[] {3,2,1};
    for( int key : given) {
      cut.add(key);
    }
    int target = 1;
    assertThat(cut.search(target).key, is(equalTo(target)));
    assertThat(cut.print(), is(equalTo("213")));
    cut.remove(target);
    assertThat(cut.search(target), is(equalTo(null)));
    assertThat(cut.print(), is(equalTo("23")));
  }

  @Test
  public void should_removeNode_whenTargetHasLeftChild() {
    int[] given = new int[] {4,2,8,1,3,7};
    for( int key : given) {
      cut.add(key);
    }
    int target = 8;
    assertThat(cut.search(target).key, is(equalTo(target)));
    assertThat(cut.print(), is(equalTo("428137")));
    cut.remove(target);
    assertThat(cut.search(target), is(equalTo(null)));
    assertThat(cut.print(), is(equalTo("42713")));
  }

  @Test
  public void should_removeNode_whenTargetHasRightChild() {
    int[] given = new int[] {4,2,8,1,3,9};
    for( int key : given) {
      cut.add(key);
    }
    int target = 8;
    assertThat(cut.search(target).key, is(equalTo(target)));
    assertThat(cut.print(), is(equalTo("428139")));
    cut.remove(target);
    assertThat(cut.search(target), is(equalTo(null)));
    assertThat(cut.print(), is(equalTo("42913")));
  }

  @Test
  public void should_removeNode_whenTargetHasTwoChildren() {
    int[] given = new int[] {4,2,8,1,3,9};
    for( int key : given) {
      cut.add(key);
    }
    int target = 2;
    assertThat(cut.search(target).key, is(equalTo(target)));
    assertThat(cut.print(), is(equalTo("428139")));
    cut.remove(target);
    assertThat(cut.search(target), is(equalTo(null)));
    assertThat(cut.print(), is(equalTo("43819")));
  }

  @Test
  public void should_printByRows_printingTreeContents() {
    int[] given = new int[] {6,3,2,1,4,5,8,7,9};
    for( int key : given) {
      cut.add(key);
    }
    assertThat(cut.print(), is(equalTo("327158469")));
  }

  @Test
  public void should_rebalance_whenAddedNodeCausesLeftHeavyGrandParent() {
    int[] given = new int[] {3,2,1};
    for( int key : given) {
      cut.add(key);
    }
    assertThat(cut.print(), is(equalTo("213")));
  }

  @Test
  public void should_rebalance_whenAddedNodeCausesLeftHeavyGrandParentButParentIsRightHeavy() {
    int[] given = new int[] {3,1,2};
    for( int key : given) {
      cut.add(key);
    }
    assertThat(cut.print(), is(equalTo("213")));
  }

  @Test
  public void should_rebalance_whenAddedNodeCausesRightHeavyGrandParent() {
    int[] given = new int[] {1,2,3};
    for( int key : given) {
      cut.add(key);
    }
    assertThat(cut.print(), is(equalTo("213")));
  }

  @Test
  public void should_rebalance_whenAddedNodeCausesRightHeavyGrandParentButParentIsLeftHeavy() {
    int[] given = new int[] {1,3,2};
    for( int key : given) {
      cut.add(key);
    }
    assertThat(cut.print(), is(equalTo("213")));
  }
}