package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.IslandDepthExplorer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class IslandTest {

   //private IslandBreadthExplorer cut = new IslandBreadthExplorer();

   private IslandDepthExplorer cut = new IslandDepthExplorer();

   @ParameterizedTest
   @MethodSource("grids")
   public void should_countIslands_whenGivenGrid(char[][] given, int expected) {
       int actual = cut.islandsIn(given);
       Assertions.assertEquals(expected, actual);
   }

   private static Stream<Arguments> grids() {
     return Stream.of(
             Arguments.of(new char[][] {
                     {'1','1','1','1','0'},
                     {'1','1','0','1','0'},
                     {'1','1','0','0','0'},
                     {'0','0','0','0','0'}
             }, 1),
             Arguments.of(new char[][] {
                     {'1','1','0','0','0'},
                     {'1','1','0','0','0'},
                     {'0','0','1','0','0'},
                     {'0','0','0','1','1'}
             }, 3)
     );
   }

}