package com.soborked.kata.puzzles.unsorted.review;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HMapTest {

    @Test
    public void should_returnItem_whenPreviouslyAdded() {
        HMap<String, Integer> map = new HMap<>();
        map.put("a", 1);
        assertThat(map.get("a"), is(equalTo(1)));
    }

    @Test
    public void should_returnNull_whenItemDoesNotExist() {
        HMap<String, Integer> map = new HMap<>(1);
        assertThat(map.get("a"), is(equalTo(null)));
    }

    @Test
    public void should_returnLatestValue_whenItemIsUpdated() {
        HMap<String, Integer> map = new HMap<>(1);
        map.put("a", 1);
        map.put("a", 2);
        assertThat(map.get("a"), is(equalTo(2)));
    }

    @Test
    public void should_returnItems_whenCollisionExists() {
        HMap<Foo, Integer> map = new HMap<>(1);
        Foo a = new Foo();
        Foo b = new Foo();
        map.put(a, 1);
        map.put(b, 2);
        assertThat(map.get(a), is(equalTo(1)));
        assertThat(map.get(b), is(equalTo(2)));
    }

    @Test
    public void should_neverRunOutOfRoom_whenAddingItems() {
        HMap<String, Integer> map = new HMap<>(4);
        map.put("a", 1);
        map.put("b", 1);
        map.put("c", 1);
        map.put("d", 1);
        map.put("e", 1);
        assertThat(map.get("a"), is(equalTo(1)));
        assertThat(map.get("e"), is(equalTo(1)));
    }

    @Test
    public void should_removeNode_whenTargeted() {
        HMap<String, Integer> map = new HMap<>();
        map.put("a", 1);
        assertThat(map.get("a"), is(equalTo(1)));
        map.remove("a");
        assertThat(map.get("a"), is(equalTo(null)));
    }

    @Test
    public void should_removeNodeWithCollision_whenTargeted() {
        HMap<Foo, Integer> map = new HMap<>(1);
        Foo a = new Foo();
        Foo b = new Foo();
        Foo c = new Foo();
        map.put(a, 1);
        map.put(b, 2);
        map.put(c, 3);
        assertThat(map.get(b), is(equalTo(2)));
        map.remove(b);
        assertThat(map.get(a), is(equalTo(1)));
        assertThat(map.get(b), is(equalTo(null)));
        assertThat(map.get(c), is(equalTo(3)));
    }

    private static class Foo {

        @Override
        public int hashCode() {
            return 1;
        }

    }

}