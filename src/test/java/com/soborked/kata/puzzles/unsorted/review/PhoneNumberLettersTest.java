package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * Given a string of numbers, where each number n: 2 <= n <= 9.
 * Return all combinations of letter representations for the input string, given:
 * 2 => ABC
 * 3 => DEF
 * 4 => GHI
 * 5 => JKL
 * 6 => MNO
 * 7 => PQRS
 * 8 => TUV
 * 9 => WXYZ
 */
public class PhoneNumberLettersTest {

    private PhoneToLetterConverter cut = new PhoneToLetterConverter();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnAllCombinations_whenGivenPhoneNumber(String given, String[] expected) {
      String[] actual = cut.phoneToLetterCombos(given);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of("23", new String[] {"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"})
      );
    }
}
