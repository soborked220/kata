package com.soborked.kata.puzzles.unsorted.review;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class BinaryTreeTest {

  private BinaryTree cut = new BinaryTree();

  @Test
  public void should_insertAtRoot_whenAddingFirstNode() {
    cut.add(3);
    assertThat(cut.root().key, is(equalTo(3)));
  }
  @Test
  public void should_listNodesInTraversalOrder_whenPrinted() {
    cut.add(3);
    cut.add(2);
    cut.add(1);
    assertThat(cut.print(), is(equalTo("123")));
  }

  @ParameterizedTest
  @MethodSource("searchExamples")
  public void should_returnNode_whenSearchingForPreviouslyAdded(int[] given, int expected) {
    for(int key: given) {
      cut.add(key);
    }
    assertThat(cut.search(expected).key, is(equalTo(expected)));
  }

  private static Stream<Arguments> searchExamples() {
    return Stream.of(
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 1),
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 2),
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 3),
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 4),
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 5),
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 6),
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 7),
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 8),
            Arguments.of(new int[]{6,9,7,8,3,2,1,4,5}, 9)
    );
  }

  @Test
  public void should_removeNode_whenDeletingLeaf() {
    cut.add(6);
    cut.add(3);
    cut.add(2);
    cut.add(1);
    cut.add(4);
    cut.add(5);
    cut.add(8);
    cut.add(7);
    cut.add(9);
    assertThat(cut.print(), is(equalTo("123456789")));
    cut.remove(1);
    assertThat(cut.print(), is(equalTo("23456789")));
  }

  @Test
  public void should_removeNode_whenDeletingNodeWithSingleLeftChild() {
    cut.add(6);
    cut.add(3);
    cut.add(2);
    cut.add(1);
    cut.add(4);
    cut.add(5);
    cut.add(8);
    cut.add(7);
    cut.add(9);
    assertThat(cut.print(), is(equalTo("123456789")));
    cut.remove(2);
    assertThat(cut.print(), is(equalTo("13456789")));
  }

  @Test
  public void should_removeNode_whenDeletingNodeWithSingleRightChild() {
    cut.add(6);
    cut.add(3);
    cut.add(2);
    cut.add(1);
    cut.add(4);
    cut.add(5);
    cut.add(8);
    cut.add(7);
    cut.add(9);
    assertThat(cut.print(), is(equalTo("123456789")));
    cut.remove(4);
    assertThat(cut.print(), is(equalTo("12356789")));
  }

  @Test
  public void should_removeNode_whenDeletingNodeWithTwoChildren() {
    cut.add(6);
    cut.add(3);
    cut.add(2);
    cut.add(1);
    cut.add(4);
    cut.add(5);
    cut.add(8);
    cut.add(7);
    cut.add(9);
    assertThat(cut.print(), is(equalTo("123456789")));
    cut.remove(8);
    assertThat(cut.print(), is(equalTo("12345679")));
  }

}