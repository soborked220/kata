package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.ObstacleNavigator;
import com.soborked.kata.puzzles.unsorted.RecursiveNavigator;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class ObstacleGridTest {

  private List<ObstacleNavigator> cut = Arrays.asList(
          new RecursiveNavigator()
  );

  @ParameterizedTest
  @MethodSource("examples")
  public void should_returnCountOfViablePaths_whenGivenGrid(int[][] given, int expected) {
    cut.forEach(navigator -> {
      int actual = navigator.explore(given);
        MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    });
  }

  private static Stream<Arguments> examples() {
    return Stream.of(
            Arguments.of(new int[][]{
                    {0,0,0},
                    {0,1,0},
                    {0,0,0}
            }, 2),
            Arguments.of(new int[][]{
                    {0,1},
                    {0,0}
            }, 1)
    );
  }
}
