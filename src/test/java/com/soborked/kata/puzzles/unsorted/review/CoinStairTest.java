package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class CoinStairTest {

    private CoinStair cut = new CoinStair();
    
    @ParameterizedTest
    @MethodSource("examples")
    public void should_heightOfStairCase_whenGivenNumberOfCoins(int given, int expected) {
      int actual = cut.climb(given);
      assertThat(actual, is(Matchers.equalTo(expected)));
    }
    
    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(5, 2),
              Arguments.of(8, 3),
              Arguments.of(6, 3)
      );
    }

}