package com.soborked.kata.puzzles.unsorted;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.equalTo;

/**
 * Given two integers n and k, return all possible combinations of k numbers chosen from the range
 * [1, n].
 *
 * <p>You may return the answer in any order.
 *
 * <p>Example 1:
 *
 * <p>Input: n = 4, k = 2 Output: [[1,2],[1,3],[1,4],[2,3],[2,4],[3,4]] Explanation: There are 4
 * choose 2 = 6 total combinations. Note that combinations are unordered, i.e., [1,2] and [2,1] are
 * considered to be the same combination.
 *
 * <p>Example 2:
 *
 * <p>Input: n = 1, k = 1 Output: [[1]] Explanation: There is 1 choose 1 = 1 total combination.
 *
 * <p>Constraints:
 *
 * <p>1 <= n <= 20 1 <= k <= n
 */
public class CombinationTest {

  private Combination cut;

  @Test
  public void should_returnAllCombos_whenGivenRangeOfNumbers() {
    cut = new Combination();
    assertThat(
        cut.combine(4, 2), is(equalTo(Arrays.asList(
                    Arrays.asList(1, 2),
                    Arrays.asList(1, 3),
                    Arrays.asList(1, 4),
                    Arrays.asList(2, 3),
                    Arrays.asList(2, 4),
                    Arrays.asList(3, 4))
            )));
  }
}
