package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class VerticalTraversalTest {

    private VerticalTreeTraverser cut = new VerticalTreeTraverser();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnValuesInEachVerticalColumn_whenGivenBinaryTree(Integer[] given, int[][] expected) {
      int[][] actual = cut.sortByColumns(given);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new Integer[]{3,9,20,null, null, 15,7}, new int[][]{
                      new int[]{9},
                      new int[]{3,15},
                      new int[]{20},
                      new int[]{7}
              }),
              Arguments.of(new Integer[]{1,2,3,4,5,6,7}, new int[][]{
                      new int[]{4},
                      new int[]{2},
                      new int[]{1,5,6},
                      new int[]{3},
                      new int[]{7}
              }),
              Arguments.of(new Integer[]{1,2,3,4,6,5,7}, new int[][]{
                      new int[]{4},
                      new int[]{2},
                      new int[]{1,5,6},
                      new int[]{3},
                      new int[]{7}
              })
      );
    }
}
