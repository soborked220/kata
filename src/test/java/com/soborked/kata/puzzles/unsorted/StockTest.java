package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.LightStockChecker;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * You are given an array prices where prices[i] is the price of a given stock on the ith day.
 *
 * You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.
 *
 * Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.
 *
 *
 *
 * Example 1:
 *
 * Input: prices = [7,1,5,3,6,4]
 * Output: 5
 * Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
 * Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.
 *
 * Example 2:
 *
 * Input: prices = [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transactions are done and the max profit = 0.
 *
 *
 *
 * Constraints:
 *
 *     1 <= prices.length <= 105
 *     0 <= prices[i] <= 104
 */
public class StockTest {

    //private StockChecker cut = new StockChecker();
    //private FastStockChecker cut = new FastStockChecker();

    private LightStockChecker cut = new LightStockChecker();

    @ParameterizedTest
    @MethodSource("priceTimeline")
    public void should_returnMaxProfitPossible_whenAnalyzingFuturePrices(int[] given, int expected) {
        int actual = cut.maxProfit(given);
        MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> priceTimeline() {
      return Stream.of(
              Arguments.of(new int[]{2,1}, 0),
              Arguments.of(new int[]{7,1,5,3,6,4}, 5),
              Arguments.of(new int[]{7,6,4,3,1}, 0)
      );
    }
}
