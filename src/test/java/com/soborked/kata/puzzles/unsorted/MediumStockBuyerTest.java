package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.MediumStockChecker;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * You are given an integer array prices where prices[i] is the price of a given stock on the ith day.
 *
 * On each day, you may decide to buy and/or sell the stock. You can only hold at most one share of the stock at any time. However, you can buy it then immediately sell it on the same day.
 *
 * Find and return the maximum profit you can achieve.
 *
 *
 *
 * Example 1:
 *
 * Input: prices = [7,1,5,3,6,4]
 * Output: 7
 * Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
 * Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
 * Total profit is 4 + 3 = 7.
 *
 * Example 2:
 *
 * Input: prices = [1,2,3,4,5]
 * Output: 4
 * Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
 * Total profit is 4.
 *
 * Example 3:
 *
 * Input: prices = [7,6,4,3,1]
 * Output: 0
 * Explanation: There is no way to make a positive profit, so we never buy the stock to achieve the maximum profit of 0.
 *
 *
 *
 * Constraints:
 *
 *     1 <= prices.length <= 3 * 104
 *     0 <= prices[i] <= 104
 */
public class MediumStockBuyerTest {

    private MediumStockChecker cut = new MediumStockChecker();

    @ParameterizedTest
    @MethodSource("priceTimeline")
    public void should_returnMaxProfitPossible_whenAnalyzingFuturePrices(int[] given, int expected) {
        int actual = cut.maxProfit(given);
        MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> priceTimeline() {
        return Stream.of(
                Arguments.of(new int[]{2,1}, 0),
                Arguments.of(new int[]{7,1,5,3,6,4}, 7),
                Arguments.of(new int[]{7,1,5,6,6,4}, 5),
                Arguments.of(new int[]{7,1,5,6,1,4}, 8),
                Arguments.of(new int[]{7,6,4,3,1}, 0)
        );
    }
}
