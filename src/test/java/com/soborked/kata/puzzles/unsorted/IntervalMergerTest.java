package com.soborked.kata.puzzles.unsorted;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class IntervalMergerTest {
   private final IntervalMerger cut = new IntervalMerger();

   @ParameterizedTest
   @MethodSource("distinct")
   public void should_notMerge_whenNoneOverlap(int[][] given, int[][] expected) {
       int[][] actual = cut.merge(given);
       assertThat(actual, is(equalTo(expected)));
   }

   private static Stream<Arguments> distinct() {
     return Stream.of(
             Arguments.of(new int[][]{{0,1}, {2,3}}, new int[][]{{0,1}, {2,3}}),
             Arguments.of(new int[][]{{2,3}, {0,1}}, new int[][]{{0,1}, {2,3}}),
             Arguments.of(new int[][]{{0,1}}, new int[][]{{0,1}})
     );
   }

   @ParameterizedTest
   @MethodSource("overlapping")
   public void should_returnMerged_whenAllOverlap(int[][] given, int[][] expected) {
       int[][] actual = cut.merge(given);
       assertThat(actual, is(equalTo(expected)));
   }

   private static Stream<Arguments> overlapping() {
     return Stream.of(
         Arguments.of(new int[][]{{0,2}, {1,3}}, new int[][]{{0,3}})
     );
   }

}