package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class PermutationGuesserTest {

    private PermutationGuesser cut = new PermutationGuesser();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnNextPermutation_whenGivenArrayOfIntegers(int[] given, int[] expected) {
      int[] actual = cut.next(given);
      assertThat(actual, is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              //1,2,3 -> 1,3,2 -> 2,1,3 -> 2,3,1 -> 3,1,2 -> 3,2,1
              Arguments.of(new int[]{1,2,3}, new int[] {1,3,2}),
              Arguments.of(new int[]{1,3,2}, new int[] {2,1,3}),
              Arguments.of(new int[]{2,1,3}, new int[] {2,3,1}),
              Arguments.of(new int[]{2,3,1}, new int[] {3,1,2}),
              Arguments.of(new int[]{3,1,2}, new int[] {3,2,1}),
              Arguments.of(new int[]{3,2,1}, new int[] {1,2,3}),
              Arguments.of(new int[]{1,5,8,4,7,6,5,3,1}, new int[] {1,5,8,5,1,3,4,6,7})
      );
    }

}