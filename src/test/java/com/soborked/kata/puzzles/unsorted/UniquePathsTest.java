package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.EdgePathExplorer;
import com.soborked.kata.puzzles.unsorted.PathExplorer;
import com.soborked.kata.puzzles.unsorted.RecursiveExplorer;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * There is a robot on an m x n grid. The robot is initially located at the top-left corner (i.e.,
 * grid[0][0]). The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]). The
 * robot can only move either down or right at any point in time.
 *
 * <p>Given the two integers m and n, return the number of possible unique paths that the robot can
 * take to reach the bottom-right corner.
 *
 * <p>The test cases are generated so that the answer will be less than or equal to 2 * 109.
 *
 * <p>Example 1:
 *
 * <p>Input: m = 3, n = 7 Output: 28
 *
 * <p>Example 2:
 *
 * <p>Input: m = 3, n = 2 Output: 3 Explanation: From the top-left corner, there are a total of 3
 * ways to reach the bottom-right corner: 1. Right -> Down -> Down 2. Down -> Down -> Right 3. Down
 * -> Right -> Down
 *
 * <p>Constraints:
 *
 * <p>1 <= m, n <= 100
 */
public class UniquePathsTest {

    private List<PathExplorer> cut = Arrays.asList(
            //new CombinationPathExplorer()
            new EdgePathExplorer(),
            new RecursiveExplorer()
    );

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnCountOfUniquePaths_whenGivenGrid(int rows, int columns, int expected) {
      cut.forEach(explorer -> {
        int actual = explorer.explore(rows, columns);
        MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
      });
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(3,7,28),
              Arguments.of(3,2,3),
              Arguments.of(3,3,6),
              Arguments.of(4,4,20),
              Arguments.of(5,5,70),
              Arguments.of(6,6,252),
              Arguments.of(7,7,924),
              Arguments.of(8,8,3432),
              Arguments.of(9,9,12870),
              Arguments.of(10,2,10),
              Arguments.of(3,10,55),
              Arguments.of(10,10,48620)

      );
    }
}
