package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * Given an array of numbers, some of which may be negative, divide the array into precisely two arrays,
 * and return the max possible difference between the sums of all the values in the two arrays.
 *
 * You can divide the arrays unevenly: e.g. if given an array.length = 6, you may create an array of length 1 and length 5
 * You cannot reuse the same value: the length of the two subsets must equal the length of the input
 *
 * Some values may repeat in the given array, but never more than once.
 * The two subset arrays cannot contain a duplicate value: e.g. if two 5s are present in the given, they must be in separate subsets
 *
 * The given is not guaranteed to contain a negative number.
 * Numbers must be between -1000 <= n <= 1000
 */
public class MaxDiffArraySubsetTest {

    private MaxArraySubset cut = new MaxArraySubset();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnMaxDiff_whenGivenArray(int[] given, int expected) {
      int actual = cut.maxDiff(given);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[]{2,5,-3,-1,5,7}, 13),
              Arguments.of(new int[]{1,5,3,-1,-5,6}, 21),
              Arguments.of(new int[]{1,1}, 0),
              Arguments.of(new int[]{-1,-1}, 0),
              Arguments.of(new int[]{1,2,3}, 4),
              Arguments.of(new int[]{-1,-2,-3}, 4)
      );
    }
}
