package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.*;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.nullValue;

/**
 * Given the root of a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.
 *
 *
 *
 * Example 1:
 *
 * Input: root = [1,2,3,null,5,null,4]
 * Output: [1,3,4]
 *
 * Example 2:
 *
 * Input: root = [1,null,3]
 * Output: [1,3]
 *
 * Example 3:
 *
 * Input: root = []
 * Output: []
 *
 *
 *
 * Constraints:
 *
 *     The number of nodes in the tree is in the range [0, 100].
 *     -100 <= Node.val <= 100
 */
public class BinaryTreeRightSideViewTest {

    private List<RightSideView> cut = Arrays.asList(
            new DfsBinaryTreeRightSideView(),
            new BfsBinaryTreeRightSideView(),
            new RightSideViewPractice()
    );

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnOnlyRightSideVisibleNodes_whenGivenBinaryTree(TreeNode given, List<Integer> expected) {
        cut.forEach(view -> {
            List<Integer> actual = view.visible(given);
            MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
        });
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new TreeNode(1,
                      new TreeNode(2, null,
                              new TreeNode(5)),
                      new TreeNode(3, null,
                              new TreeNode(4))),
                      asList(1,3,4)),
              Arguments.of(new TreeNode(1,
                      null,
                      new TreeNode(3)),
                      asList(1,3)),
              Arguments.of(new TreeNode(), Collections.emptyList()),
              Arguments.of(null, Collections.emptyList())
      );
    }
}
