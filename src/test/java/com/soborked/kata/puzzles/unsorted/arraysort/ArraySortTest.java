package com.soborked.kata.puzzles.unsorted.arraysort;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Given an array of integers nums, sort the array in ascending order and return it.
 *
 * You must solve the problem without using any built-in functions in O(nlog(n)) time complexity and with the smallest space complexity possible.
 *
 *
 *
 * Example 1:
 *
 * Input: nums = [5,2,3,1]
 * Output: [1,2,3,5]
 * Explanation: After sorting the array, the positions of some numbers are not changed (for example, 2 and 3), while the positions of other numbers are changed (for example, 1 and 5).
 *
 * Example 2:
 *
 * Input: nums = [5,1,1,2,0,0]
 * Output: [0,0,1,1,2,5]
 * Explanation: Note that the values of nums are not necessairly unique.
 *
 *
 *
 * Constraints:
 *
 *     1 <= nums.length <= 5 * 104
 *     -5 * 104 <= nums[i] <= 5 * 104
 */
public class ArraySortTest {

    private List<ArraySorter> cut = Arrays.asList(
            new ArraySortByMin(),
            new ArrayMergeSorter(),
            new ArrayBubbleSorter(),
            new ArrayQuickSort(),
            new ArrayHeapSort(),
            new ArrayInsertionSort(),
            new ArraySelectionSort(),
            new ArrayQ(),
            new ArrayM(),
            new ArrayB(),
            new ArrayH(),
            new ArrayS(),
            new ArrayI()
    );

    @ParameterizedTest
    @MethodSource("scenarios")
    public void should_returnArraySortedInAscendingOrder_whenGivenArray(int[] given, int[] expected) {
    cut.forEach(
        sorter -> {
          int[] actual = sorter.sort(Arrays.copyOf(given, given.length));
          assertEquals(Arrays.toString(expected), Arrays.toString(actual), format("%s failed", sorter.name()));
        });
    }

    private static Stream<Arguments> scenarios() {
      return Stream.of(
              Arguments.of(new int[]{1,2,3}, new int[]{1,2,3}),
              Arguments.of(new int[]{5,2,3,1}, new int[]{1,2,3,5}),
              Arguments.of(new int[]{5,1,1,2,0,0}, new int[]{0,0,1,1,2,5}),
              Arguments.of(new int[]{8,7,6,1,0,9,2}, new int[]{0,1,2,6,7,8,9}),
              Arguments.of(new int[]{1,8,1,7,6,1,0,9,2}, new int[]{0,1,1,1,2,6,7,8,9})
      );
    }

}
