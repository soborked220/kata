package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.HarderStockChecker;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * You are given an integer array prices where prices[i] is the price of a given stock on the ith day, and an integer k.
 *
 * Find the maximum profit you can achieve. You may complete at most k transactions: i.e. you may buy at most k times and sell at most k times.
 *
 * Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock before you buy again).
 *
 *
 *
 * Example 1:
 *
 * Input: k = 2, prices = [2,4,1]
 * Output: 2
 * Explanation: Buy on day 1 (price = 2) and sell on day 2 (price = 4), profit = 4-2 = 2.
 *
 * Example 2:
 *
 * Input: k = 2, prices = [3,2,6,5,0,3]
 * Output: 7
 * Explanation: Buy on day 2 (price = 2) and sell on day 3 (price = 6), profit = 6-2 = 4. Then buy on day 5 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
 *
 *
 *
 * Constraints:
 *
 *     1 <= k <= 100
 *     1 <= prices.length <= 1000
 *     0 <= prices[i] <= 1000
 */
public class HarderStockBuyerTest {


    private HarderStockChecker cut = new HarderStockChecker();

    @ParameterizedTest
    @MethodSource("priceTimeline")
    public void should_returnMaxProfitPossible_whenAnalyzingFuturePrices(int k, int[] prices,  int expected) {
        int actual = cut.maxProfit(k, prices);
        MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> priceTimeline() {
        return Stream.of(
                Arguments.of(2, new int[]{2,1}, 0),
                Arguments.of(2, new int[]{3,3,5,0,0,3,1,4}, 6),
                Arguments.of(2, new int[]{3,3,6,0,1,3,1,3}, 6),
                Arguments.of(2, new int[]{1,2,3,4,5}, 4),
                Arguments.of(2, new int[]{1,2,3,0,5}, 7),
                Arguments.of(2, new int[]{7,6,4,3,1}, 0),
                Arguments.of(2, new int[]{1,2,4,2,5,7,2,4,9,0}, 13)
        );
    }

}
