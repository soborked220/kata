package com.soborked.kata.puzzles.unsorted;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

/**
 * Write a function to find the longest common prefix string amongst an array of strings.
 *
 * <p>If there is no common prefix, return an empty string "".
 *
 * <p>Example 1:
 *
 * <p>Input: strs = ["flower","flow","flight"] Output: "fl"
 *
 * <p>Example 2:
 *
 * <p>Input: strs = ["dog","racecar","car"] Output: "" Explanation: There is no common prefix among
 * the input strings.
 *
 * <p>Constraints:
 *
 * <p>1 <= strs.length <= 200 0 <= strs[i].length <= 200 strs[i] consists of only lowercase English
 * letters.
 */
public class CommonPrefixTest {

    private CommonPrefix cut = new CommonPrefix();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnLongestPrefix_whenGivenListOfWords(String[] given, String expected) {
      String actual = cut.longestPrefix(given);
      assertThat(actual, is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new String[] {"flower", "flow", "flight"}, "fl"),
              Arguments.of(new String[] {"dog", "racecar", "car"}, ""),
              Arguments.of(new String[] {"dog", "dogs"}, "dog")
      );
    }

}