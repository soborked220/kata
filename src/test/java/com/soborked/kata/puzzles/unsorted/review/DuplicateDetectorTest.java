package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class DuplicateDetectorTest {

    private DuplicateDetector cut = new DuplicateDetector();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnTrue_whenDuplicateEntryExists(int[] given, boolean expected) {
      boolean actual = cut.hasDuplicate(given);
      assertThat(actual, is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[] {1,2,3,1}, true),
              Arguments.of(new int[] {1,2,3,4}, false),
              Arguments.of(new int[] {1,1,1,3,3,4,3,2,4,2}, true)
      );
    }

}