package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.HardStockChecker;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * You are given an array prices where prices[i] is the price of a given stock on the ith day.
 *
 * Find the maximum profit you can achieve. You may complete at most two transactions.
 *
 * Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock before you buy again).
 *
 *
 *
 * Example 1:
 *
 * Input: prices = [3,3,5,0,0,3,1,4]
 * Output: 6
 * Explanation: Buy on day 4 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
 * Then buy on day 7 (price = 1) and sell on day 8 (price = 4), profit = 4-1 = 3.
 *
 * Example 2:
 *
 * Input: prices = [1,2,3,4,5]
 * Output: 4
 * Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
 * Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are engaging multiple transactions at the same time. You must sell before buying again.
 *
 * Example 3:
 *
 * Input: prices = [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transaction is done, i.e. max profit = 0.
 *
 *
 *
 * Constraints:
 *
 *     1 <= prices.length <= 105
 *     0 <= prices[i] <= 105
 */
public class HardStockBuyerTest {

    private HardStockChecker cut = new HardStockChecker();

    @ParameterizedTest
    @MethodSource("priceTimeline")
    public void should_returnMaxProfitPossible_whenAnalyzingFuturePrices(int[] given, int expected) {
        int actual = cut.maxProfit(given);
        MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> priceTimeline() {
        return Stream.of(
                Arguments.of(new int[]{2,1}, 0),
                Arguments.of(new int[]{3,3,5,0,0,3,1,4}, 6),
                Arguments.of(new int[]{3,3,6,0,1,3,1,3}, 6),
                Arguments.of(new int[]{1,2,3,4,5}, 4),
                Arguments.of(new int[]{1,2,3,0,5}, 7),
                Arguments.of(new int[]{7,6,4,3,1}, 0),
                Arguments.of(new int[]{1,2,4,2,5,7,2,4,9,0}, 13)
        );
    }
}
