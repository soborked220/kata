package com.soborked.kata.puzzles.unsorted.review;

import com.soborked.kata.puzzles.unsorted.review.BinaryTreeTraversalValidator.Node;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ValidBinaryTreeTest {

    private List<BinaryTreeValidator> cut = Arrays.asList(
            new BinaryTreeTraversalValidator(),
            new BinaryTreeRecursiveValidator()
    );

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnTrue_whenGivenValidBinaryTree(Integer[] given, boolean expected) {
      Node root = assemble(given,0);
      cut.forEach(clazz -> {
        boolean actual = clazz.isValid(root);
        assertThat(actual, is(Matchers.equalTo(expected)));
      });
    }

  private Node assemble(Integer[] given, int index) {
      if(index < given.length && given[index] != null) {
        Node node = new Node();
        node.setVal(given[index]);
        node.setLeft(assemble(given, 2*index + 1));
        node.setRight(assemble(given, 2*index+2));
        return node;
      }
      return null;
  }

  private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new Integer[] {4,2,5,1,3,null,6}, true),
              Arguments.of(new Integer[] {4,2,5,1,3,6}, false),
              Arguments.of(new Integer[] {12,7,16,3,9,14,19,null,null,8,14,6,null,null,null}, false)
      );
    }

}