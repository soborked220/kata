package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.Trie;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class ReTrievalTest {

  @Test
  public void should_containString_afterInsert() {
    Trie structure = new Trie();
    structure.add("baseline");
    assertThat(structure.contains("baseline"), is(equalTo(true)));
  }

  @Test
  public void should_showEmpty_whenNoneAdded() {
    Trie structure = new Trie();
    assertThat(structure.isEmpty(), is(equalTo(true)));
  }

  @Test
  public void should_notContain_whenRemoved() {
    Trie structure = new Trie();
    structure.add("baseline");
    structure.remove("baseline");
    assertThat(structure.contains("baseline"), is(equalTo(false)));
  }

  @Test
  public void should_onlyRemoveTheTarget_whenSiblingsExist() {
    Trie structure = new Trie();
    structure.add("bad");
    structure.add("bat");
    structure.remove("bad");
    assertThat(structure.contains("bad"), is(equalTo(false)));
    assertThat(structure.contains("bat"), is(equalTo(true)));
  }

  @Test
  public void should_onlyRemoveTheTarget_whenDescendantsExist() {
    Trie structure = new Trie();
    structure.add("bat");
    structure.add("bats");
    structure.remove("bat");
    assertThat(structure.contains("bat"), is(equalTo(false)));
    assertThat(structure.contains("bats"), is(equalTo(true)));
  }

  @Test
  public void should_onlyRemoveTheTarget_whenParentIsItselfAnAddedWord() {
    Trie structure = new Trie();
    structure.add("bat");
    structure.add("bats");
    structure.remove("bats");
    assertThat(structure.contains("bats"), is(equalTo(false)));
    assertThat(structure.contains("bat"), is(equalTo(true)));
  }

  @Test
  public void should_notRemoveRoot_whenAllDescendantsRemoved() {
    Trie structure = new Trie();
    structure.add("baseline");
    structure.remove("baseline");
    structure.add("bats");
    assertThat(structure.contains("baseline"), is(equalTo(false)));
    assertThat(structure.contains("bats"), is(equalTo(true)));
  }

  @Test
  public void should_returnTrue_whenAddedStringStartsWithPrefix() {
    Trie structure = new Trie();
    structure.add("baseline");
    assertThat(structure.startsWith("base"), is(equalTo(true)));
  }

  @Test
  public void should_returnTrue_whenAddedStringStartsWithItself() {
    Trie structure = new Trie();
    structure.add("baseline");
    assertThat(structure.startsWith("baseline"), is(equalTo(true)));
  }

  @Test
  public void should_notThrowException_whenEmpty() {
    Trie structure = new Trie();
    structure.remove("baseline");
  }
}