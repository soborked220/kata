package com.soborked.kata.puzzles.unsorted.review;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * No two jobs can overlap.
 */
public class MaxProfitTest {

    private JobScheduler cut = new ProfitableJobScheduler();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnMaxProfit_whenNoJobsOverlap(int[] startTimes, int[] endTimes, int[] profits, int expected) {
      int actual = cut.schedule(startTimes, endTimes, profits);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[] {1,2,3,3}, new int[]{3,4,5,6}, new int[]{50,10,40,70}, 120),
              Arguments.of(new int[] {1,2,3,4,6}, new int[]{3,5,10,6,9}, new int[]{20,20,100,70,60}, 150)
      );
    }
}
