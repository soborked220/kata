package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.IntegerCounter;
import com.soborked.kata.puzzles.unsorted.IntegerFrequency;
import com.soborked.kata.puzzles.unsorted.IntegerHeapFrequency;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class ElementFrequencyTest {

    private List<IntegerCounter> cut = Arrays.asList(
            new IntegerFrequency(),
            new IntegerHeapFrequency()
    );

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnMostFrequentIntegers_whenGivenMultiple(int[] given, int k, int[] expected) {
        cut.forEach(counter -> {
            int[] actual = counter.topKFrequent(given, k);
            MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
        });
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[]{1,1,1,2,2,3}, 2, new int[]{1,2}),
              Arguments.of(new int[]{1}, 1, new int[]{1})
      );
    }
}
