package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.LimitedMoveMaximizer;
import com.soborked.kata.puzzles.unsorted.PileMaximizer;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * You are given a 0-indexed integer array nums representing the contents of a pile, where nums[0]
 * is the topmost element of the pile.
 *
 * <p>In one move, you can perform either of the following:
 *
 * <p>If the pile is not empty, remove the topmost element of the pile. If there are one or more
 * removed elements, add any one of them back onto the pile. This element becomes the new topmost
 * element.
 *
 * <p>You are also given an integer k, which denotes the total number of moves to be made.
 *
 * <p>Return the maximum value of the topmost element of the pile possible after exactly k moves. In
 * case it is not possible to obtain a non-empty pile after k moves, return -1.
 *
 * <p>Example 1:
 *
 * <p>Input: nums = [5,2,2,4,0,6], k = 4 Output: 5 Explanation: One of the ways we can end with 5 at
 * the top of the pile after 4 moves is as follows: - Step 1: Remove the topmost element = 5. The
 * pile becomes [2,2,4,0,6]. - Step 2: Remove the topmost element = 2. The pile becomes [2,4,0,6]. -
 * Step 3: Remove the topmost element = 2. The pile becomes [4,0,6]. - Step 4: Add 5 back onto the
 * pile. The pile becomes [5,4,0,6]. Note that this is not the only way to end with 5 at the top of
 * the pile. It can be shown that 5 is the largest answer possible after 4 moves.
 *
 * <p>Example 2:
 *
 * <p>Input: nums = [2], k = 1 Output: -1 Explanation: In the first move, our only option is to pop
 * the topmost element of the pile. Since it is not possible to obtain a non-empty pile after one
 * move, we return -1.
 *
 * <p>Constraints:
 *
 * <p>1 <= nums.length <= 105 0 <= nums[i], k <= 109
 */
public class MaxPileTest {

    private PileMaximizer cut = new LimitedMoveMaximizer();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnMaxValueOnTop_whenGivenPileAndLimitedMoves(int[] given, int moves, int expected) {
      int actual = cut.maximize(given, moves);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[]{5,2,2,4,0,6}, 4, 5),
              Arguments.of(new int[]{4,2,2,4,5,6}, 4, 5),
              Arguments.of(new int[]{4,2,2,6,5,6}, 4, 5),
              Arguments.of(new int[]{4,2,6,4,5,6}, 4, 6),
              Arguments.of(new int[]{4,2,6,6,5,6}, 4, 6),
              Arguments.of(new int[]{2}, 0, 2),
              Arguments.of(new int[]{2}, 1, -1),
              Arguments.of(new int[]{2}, 2, 2),
              Arguments.of(new int[]{2}, 3, -1),
              Arguments.of(new int[]{2,1}, 1, 1),
              Arguments.of(new int[]{99,95,68,24,18}, 69, 99),
              Arguments.of(new int[]{99,95,68,24,18}, 5, 99),
              Arguments.of(new int[]{99,95,68,24,18}, 6, 99),
              Arguments.of(new int[]{99,95,68,24,18}, 7, 99),
              Arguments.of(new int[]{91,98,17,79,15,55,47,86,4,5,17,79,68,60,60,31,72,85,25,77,8,78,40,96,76,69,95,2,42,87,48,72,45,25,40,60,21,91,32,79,2,87,80,97,82,94,69,43,18,19,21,36,44,81,99}, 2, 91)
      );
    }
}
