package com.soborked.kata.puzzles.unsorted;

import com.soborked.kata.puzzles.unsorted.HardObstacleNavigator;
import com.soborked.kata.puzzles.unsorted.HardObstacleWalker;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class HardObstacleGridTest {
    
    private HardObstacleNavigator cut = new HardObstacleWalker();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnCountOfFullPaths_whenGivenObstacleGrid(int[][] given, int expected) {
      int actual = cut.explore(given);
      MatcherAssert.assertThat(actual, Matchers.is(Matchers.equalTo(expected)));
    }
    
    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[][]{
                      {1,0,0,0},
                      {0,0,0,0},
                      {0,0,2,-1}
              }, 2),
              Arguments.of(new int[][]{
                      {1,0,0,0},
                      {0,0,0,0},
                      {0,0,0,2}
              }, 4),
              Arguments.of(new int[][]{
                      {0,1},
                      {2,0}
              }, 0)
      );
    }
}
