package com.soborked.kata.puzzles.search.surroundedregion;

import com.soborked.kata.puzzles.search.surroundedregion.SurroundedRegion;
import com.soborked.kata.puzzles.search.surroundedregion.SurroundedRegionDfs;
import com.soborked.kata.puzzles.search.surroundedregion.SurroundedRegionSimplifiedDfs;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SurroundedRegionTest {

    private List<SurroundedRegion> problems;

    @BeforeEach
    public void setupAll() {
        problems = Arrays.asList(
                new SurroundedRegionDfs(),
                new SurroundedRegionSimplifiedDfs()
        );
    }

    @ParameterizedTest
    @MethodSource("examples")
    public void should_haveNoMissedRequirements_whenSolved(char[][] input, char[][] expected) {
        problems.forEach(problem -> {
            char[][] solution = problem.solve(input);
            assertThat(solution, is(notNullValue()));
            assertThat(solution, is(equalTo(expected)));
        });
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
        Arguments.of(
                new char[][] {
                        new char[] {'X'}
                },
                new char[][] {
                        new char[] {'X'}
                }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'O'}
              },
              new char[][] {
                      new char[] {'O'}
              }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'X', 'X', 'X'},
                      new char[] {'X', 'O', 'X'},
                      new char[] {'X', 'X', 'X'}
              },
              new char[][] {
                      new char[] {'X', 'X', 'X'},
                      new char[] {'X', 'X', 'X'},
                      new char[] {'X', 'X', 'X'}
              }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'O', 'X', 'X'},
                      new char[] {'X', 'O', 'X'},
                      new char[] {'X', 'X', 'X'}
              },
              new char[][] {
                      new char[] {'O', 'X', 'X'},
                      new char[] {'X', 'X', 'X'},
                      new char[] {'X', 'X', 'X'}
              }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'X', 'O', 'X'},
                      new char[] {'X', 'O', 'X'},
                      new char[] {'X', 'X', 'X'}
              },
              new char[][] {
                      new char[] {'X', 'O', 'X'},
                      new char[] {'X', 'O', 'X'},
                      new char[] {'X', 'X', 'X'}
              }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'X', 'X', 'X'},
                      new char[] {'X', 'O', 'X'},
                      new char[] {'X', 'O', 'X'}
              },
              new char[][] {
                      new char[] {'X', 'X', 'X'},
                      new char[] {'X', 'O', 'X'},
                      new char[] {'X', 'O', 'X'}
              }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'X', 'X', 'X'},
                      new char[] {'X', 'O', 'O'},
                      new char[] {'X', 'X', 'X'}
              },
              new char[][] {
                      new char[] {'X', 'X', 'X'},
                      new char[] {'X', 'O', 'O'},
                      new char[] {'X', 'X', 'X'}
              }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'X', 'X', 'X'},
                      new char[] {'O', 'O', 'X'},
                      new char[] {'X', 'X', 'X'}
              },
              new char[][] {
                      new char[] {'X', 'X', 'X'},
                      new char[] {'O', 'O', 'X'},
                      new char[] {'X', 'X', 'X'}
              }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'X', 'X', 'X', 'X'},
                      new char[] {'X', 'O', 'O', 'X'},
                      new char[] {'X', 'X', 'O', 'X'},
                      new char[] {'X', 'O', 'X', 'X'}
              },
              new char[][] {
                      new char[] {'X', 'X', 'X', 'X'},
                      new char[] {'X', 'X', 'X', 'X'},
                      new char[] {'X', 'X', 'X', 'X'},
                      new char[] {'X', 'O', 'X', 'X'}
              }
        ),
        Arguments.of(
              new char[][] {
                      new char[] {'O', 'X', 'X', 'O', 'X'},
                      new char[] {'X', 'O', 'O', 'X', 'O'},
                      new char[] {'X', 'O', 'X', 'O', 'X'},
                      new char[] {'O', 'X', 'O', 'O', 'O'},
                      new char[] {'X', 'X', 'O', 'X', 'O'}
              },
              new char[][] {
                      new char[] {'O', 'X', 'X', 'O', 'X'},
                      new char[] {'X', 'X', 'X', 'X', 'O'},
                      new char[] {'X', 'X', 'X', 'O', 'X'},
                      new char[] {'O', 'X', 'O', 'O', 'O'},
                      new char[] {'X', 'X', 'O', 'X', 'O'}
              }
        )
      );
    }

}