package com.soborked.kata.puzzles.string.parse.romanToInt;

import com.soborked.kata.puzzles.Solution;
import com.soborked.kata.puzzles.Requirement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class RomanToIntPuzzleTest {

    private List<Solution<String, Integer>> solutions;

    @BeforeEach
    public void setupEach() {
        solutions = Arrays.asList(
                new RomanToIntNaiveSolution(),
                new RomanToIntEnumSolution(),
                new RomanToIntMapSolution()
        );
    }

    @Test
    public void should_haveNoMissedRequirements_whenSolved() {
        solutions.forEach(solution -> {
            List<Requirement> missed = new RomanToIntPuzzle().solveWith(solution);
            assertThat(missed, is(equalTo(emptyList())));
        });
    }
}