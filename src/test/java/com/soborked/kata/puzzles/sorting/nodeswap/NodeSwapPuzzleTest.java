package com.soborked.kata.puzzles.sorting.nodeswap;

import com.soborked.kata.puzzles.Requirement;
import com.soborked.kata.puzzles.Solution;
import com.soborked.kata.puzzles.sorting.nodeswap.ListNode;
import com.soborked.kata.puzzles.sorting.nodeswap.NodeSwapNaiveSolution;
import com.soborked.kata.puzzles.sorting.nodeswap.NodeSwapPuzzle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class NodeSwapPuzzleTest {

    private List<Solution<ListNode, ListNode>> solutions;

    @BeforeEach
    public void setupEach() {
        solutions = Arrays.asList(
                new NodeSwapNaiveSolution()
        );
    }

    @Test
    public void should_haveNoMissedRequirements_whenSolved() {
        solutions.forEach(solution -> {
            List<Requirement> missed = new NodeSwapPuzzle().solveWith(solution);
            assertThat(missed, is(equalTo(emptyList())));
        });
    }
}