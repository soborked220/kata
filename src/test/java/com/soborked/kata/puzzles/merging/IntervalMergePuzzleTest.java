package com.soborked.kata.puzzles.merging;

import com.soborked.kata.puzzles.Requirement;
import com.soborked.kata.puzzles.Solution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class IntervalMergePuzzleTest {

    private List<Solution<List<Interval>, List<Interval>>> solutions;

    @BeforeEach
    public void setupEach() {
        solutions = Arrays.asList(
                new IntervalMergeNaiveSolution()
        );
    }

    @Test
    public void should_haveNoMissedRequirements_whenSolved() {
        solutions.forEach(solution -> {
            List<Requirement> missed = new IntervalMergePuzzle().solveWith(solution);
            assertThat(missed, is(equalTo(emptyList())));
        });
    }
}