package com.soborked.kata.puzzles.math.sums.ofThree;

import com.jayway.jsonpath.internal.function.numeric.Sum;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

/**
 * Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i !=
 * j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.
 *
 * <p>Notice that the solution set must not contain duplicate triplets.
 *
 * <p>Example 1:
 *
 * <p>Input: nums = [-1,0,1,2,-1,-4] Output: [[-1,-1,2],[-1,0,1]] Explanation: nums[0] + nums[1] +
 * nums[2] = (-1) + 0 + 1 = 0. nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0. nums[0] + nums[3] +
 * nums[4] = (-1) + 2 + (-1) = 0. The distinct triplets are [-1,0,1] and [-1,-1,2]. Notice that the
 * order of the output and the order of the triplets does not matter.
 *
 * <p>Example 2:
 *
 * <p>Input: nums = [0,1,1] Output: [] Explanation: The only possible triplet does not sum up to 0.
 *
 * <p>Example 3:
 *
 * <p>Input: nums = [0,0,0] Output: [[0,0,0]] Explanation: The only possible triplet sums up to 0.
 *
 * <p>Constraints:
 *
 * <p>3 <= nums.length <= 3000 -105 <= nums[i] <= 105
 */
public class SumOfThreeTest {

    private SumOfThree cut = new SumOfThree();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnTriplets_whenGivenArray(int[] given, int[][] expected) {
      int[][] actual = cut.triplets(given);
      assertThat(actual, is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[] {-1,0,1,2,-1,-4}, new int[][] {
                      new int[] {-1,-1,2},
                      new int[]{-1,0,1}
              })
      );
    }



}