package com.soborked.kata.puzzles.math.sums.ofTwo;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;


/**
 * Given an array of numbers, return all sets of numbers which can sum to zero.
 * Do not include duplicate pairs
 */
public class ZeroSumOfTwoTest {

    private ZeroSumOfTwo cut = new ZeroSumOfTwo();

    @ParameterizedTest
    @MethodSource("examples")
    public void should_returnPairs_whenGivenNumbers(int[] given, int[][] expected) {
      int[][] actual = cut.zerosum(given);
      assertThat(actual, is(Matchers.equalTo(expected)));
    }

    private static Stream<Arguments> examples() {
      return Stream.of(
              Arguments.of(new int[] {-1, 1}, new int[][]{
                      new int[]{-1,1}
              }),
              Arguments.of(new int[] {-1,0,1}, new int[][]{
                      new int[]{-1,1}
              }),
              Arguments.of(new int[] {-1,2,1,-2}, new int[][]{
                      new int[]{-2,2},
                      new int[]{-1,1}
              }),
              Arguments.of(new int[] {-1,2,1,-2,-1,2,1,-2}, new int[][]{
                      new int[]{-2,2},
                      new int[]{-1,1}
              })
      );
    }

}