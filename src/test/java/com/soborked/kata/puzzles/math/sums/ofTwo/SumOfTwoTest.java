package com.soborked.kata.puzzles.math.sums.ofTwo;

import com.soborked.kata.puzzles.Requirement;
import com.soborked.kata.puzzles.Solution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/*
Summary of how a java hashmap works:

1. Backed by an array
2. The index of the array is determined by the hashcode function and a bit-wise AND operation on the current max size.
3. Collisions of the hashcode function still go into the same index, but the value of the index is a Node
4. Nodes started as simple linked list (only one neighbor: next), but once collisions hit a sufficient size become Trees (multiple neighbors)
5. When doing comparing in a Get or Put, we first check hashcode, and then check equals (to actually compare the values)
6. When doing a Put, we first loop through array and compare to see if it exists, doing an update if it does.
If it does not exist, we ensure the capacity of the array has room for the new entry.
7. We need to track the current size to determine whether we need to expand the array.
8. We only ever need to expand the array in doubles
 */
public class SumOfTwoTest {

    public static final String O_N_COMPLEXITY = "The solution should be solved in O(N) complexity within the specified time in nanoseconds";
    private List<Solution<SumOfTwoInput, int[]>> solutions;

    @BeforeEach
    public void setupEach() {
        solutions = Arrays.asList(
                new SumOfTwoNaiveSolution(), //Doesn't meet the O(N) requirement
                new SumOfTwoOptimizedSolution()
        );
    }

    @Test
    public void should_haveNoMissedRequirements_whenSolved() {
        solutions.forEach(solution -> {
            List<Requirement> missed = new SumOfTwoPuzzle().solveWith(solution);
            assertThat(missed.stream()
                    .filter(req -> !req.behavior().contains(O_N_COMPLEXITY))
                    .collect(toList()), is(equalTo(emptyList())));
        });
    }

}