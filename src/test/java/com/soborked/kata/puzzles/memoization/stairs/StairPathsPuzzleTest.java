package com.soborked.kata.puzzles.memoization.stairs;

import com.soborked.kata.puzzles.Solution;
import com.soborked.kata.puzzles.Requirement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;


public class StairPathsPuzzleTest {

    private List<Solution<Integer, Integer>> solutions;

    @BeforeEach
    public void setupEach() {
        solutions = Arrays.asList(
                new StairPathsNaiveSolution(),
                new StairPathsMemoizedSolution(),
                new StairPathsClimbSolution()
        );
    }

    @Test
    public void should_haveNoMissedRequirements_whenSolved() {
        solutions.forEach(solution -> {
            List<Requirement> missed = new StairPathsPuzzle().solveWith(solution);
            assertThat(missed, is(equalTo(emptyList())));
        });
    }
}